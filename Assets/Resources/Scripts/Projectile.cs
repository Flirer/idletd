﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public Weapon parentWeapon;

	public Transform target;
	public float distanceToTarget;
	public float timeToTravel;

	public float moveSpeed;

    public SpriteRenderer sprite;

    public delegate void onProjectileLand(EventInfo info);
    public event onProjectileLand OnProjectileLand;

    public delegate void onProjectileDestroy(Projectile proj);
    public event onProjectileDestroy OnProjectileDestroy;

    public void ini(Transform tar, float movespeed, Weapon wep)
    {
        target = tar;
        moveSpeed = movespeed;
        parentWeapon = wep;

        distanceToTarget = Vector3.Distance(transform.position, target.transform.position);
        timeToTravel = distanceToTarget / moveSpeed;

        target.GetComponent<Creep>().onDying += remove;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate() {

        if (target != null)
        {
            if (Vector3.Distance(target.position, transform.position) >= 0.5f)
            {
                Vector3 dista = target.position - transform.position;
                Vector3 nextFrameStep = (dista.normalized) * 7.5f;

                if (Vector3.Distance(target.position, transform.position) > nextFrameStep.magnitude)
                {
                    transform.position += nextFrameStep;
                }
                else
                {
                    transform.position = target.position;
                    reachTarget();
                }
            }
            else
            {
                //Debug.Log("We made it to target");
                
                reachTarget();
            }

        } else
        {
            remove(new EventInfo());
        }

	}

    void reachTarget()
    {
        if (OnProjectileLand != null)
        {
            EventInfo info = new EventInfo();
            info.weapon = parentWeapon;
            info.creep = target.GetComponent<Creep>();
            info.projectile = this;
            OnProjectileLand(info);
        }
        remove(new EventInfo());
    }

     void remove(EventInfo info)
    {
        if (target != null)
        {
            target.GetComponent<Creep>().onDying -= remove;
        }
        
        if (OnProjectileDestroy != null)
        {
            OnProjectileDestroy(this);
        }
        Destroy(this.gameObject);
      
    }
}

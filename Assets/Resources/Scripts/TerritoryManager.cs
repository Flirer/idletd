﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerritoryManager : MonoBehaviour {

    public List<Territory> territories;
    public static List<Territory> Territories;

    public static TerritoryManager Instance;

    public static int currentlyHightlightedTerritory = -1;

    // Use this for initialization
    void Awake () {
        Instance = this;
        Territories = territories;

    }

    public delegate void OnTerritoryChange();
    public static event OnTerritoryChange onTerritoryChange;

    public static void highlightTerritory(int id)
    {
        if (id == -1) return;
        if (currentlyHightlightedTerritory == id)
        {
            return;
        } else if (currentlyHightlightedTerritory == -1)
        {
            currentlyHightlightedTerritory = id;
        } else if (currentlyHightlightedTerritory != id)
        {
            unlightTerritory(currentlyHightlightedTerritory);
            currentlyHightlightedTerritory = id;
        }
        foreach (int hexId in Territories[id].hexesIds)
        {
            GameManager.Instance.hexes[hexId].highlightTerritory();
        }
    }

    public static void unlightTerritory(int id)
    {
        foreach (int hexId in Territories[id].hexesIds)
        {
            GameManager.Instance.hexes[hexId].unlightTerritort();
        }
        currentlyHightlightedTerritory = -1;
    }

    public static void initializeTerritories()
    {
        for (int i = 0; i < Territories.Count;i++)
        {
            foreach (int hexId in Territories[i].hexesIds)
            {
                GameManager.Instance.hexes[hexId].territoryId = i;
            }

            if (!Inventory.Territories.Contains(i)) //This territory is not unlocked
            {
                Territories[i].initialize();
            }
        }
    }

    public static void clickOnTerritory(int id)
    {
        if (Territories[id].checkRequierments() && Inventory.Gold.amount >= Territories[id].goldCost)
        {
            Inventory.unlockTerritory(id);
            Territories[id].unlock();
            Inventory.Gold.amount -= Territories[id].goldCost;
        }
    }

    public static void unlockTerritory(int id)
    {
        Territories[id].unlock();
        if (onTerritoryChange != null) onTerritoryChange();
    }

    
}

[System.Serializable]
public class Territory
{
    public List<int> hexesIds;
    public List<TerritoryRequierment> requiermnets;
    public int goldCost;
    public List<Reward> rewards;

    public delegate void OnTerritoryUnlock();
    public event OnTerritoryUnlock onTerritoryUnlock;

    public void initialize()
    {
        foreach (int id in hexesIds)
        {
            GameManager.Instance.hexes[id].lockTerritory();
        }
        if (checkRequierments()) //All requirments met, territory is ready to be unlocked
        {
            show();
        } else //We check individualy every requirment, and connect listeners to related objects
        {
            hide();
            foreach (TerritoryRequierment req in requiermnets)
            {
                if (!req.check()) req.intialize();
                req.onRequiermentMet += reqMetHandler;
            }
        }
    }

    void reqMetHandler()
    {
        //Debug.Log("Testing reqs on territory " + TerritoryManager.Territories.IndexOf(this));
        if (checkRequierments())
        {
            //Debug.Log("All met!");
            show();
        }
    }

    public bool checkRequierments()
    {
        foreach (TerritoryRequierment req in requiermnets)
        {
            //Debug.Log(req.check() + " " + req.number);
            if (!req.check()) return false;
        }
        return true;
    }

    void show()
    {
        //Show territory that ready to be unlocked
        foreach (int id in hexesIds)
        {
            GameManager.Instance.hexes[id].territoryReqMet();
        }
    }

    void hide()
    {
        foreach (int id in hexesIds)
        {
            GameManager.Instance.hexes[id].territoryReqUnmet();
        }
    }

    public void unlock()
    {
        
        foreach (int id in hexesIds)
        {
            GameManager.Instance.hexes[id].unlockTerritory();
        }
        GameManager.rebuildPathing();
        if (onTerritoryUnlock != null) onTerritoryUnlock();
        foreach (Reward reward in rewards)
        {
            reward.grant();
        }
    }
}

[System.Serializable]
public class TerritoryRequierment
{
    public enum Type {wave, territory}

    public delegate void OnRequiermentMet();
    public event OnRequiermentMet onRequiermentMet;

    public Type type;
    public int number;

    public void intialize() //Connect to requierment related object to know, when requierment is met
    {

        if (type == Type.territory)
        {
            TerritoryManager.Territories[number].onTerritoryUnlock += requiermentMetHandler;
        }
        else if (type == Type.wave)
        {
            //TODO: Refactor Spawn Manager so it support checking beaten waves and stuff
        }
    }

    void requiermentMetHandler()
    {
        //Debug.Log("Requierment met handler on territory");
        if (onRequiermentMet != null) onRequiermentMet();
        onRequiermentMet = null;
        TerritoryManager.Territories[number].onTerritoryUnlock -= requiermentMetHandler;
    }

    public bool check()
    {
        //Debug.Log(Time.frameCount + " check territory " + number);
        //Debug.Log(Inventory.Territories.Count);
        if (type == Type.territory)
        {
            if (Inventory.Territories.Contains(number)) return true; //Player have requiered territory
            else return false;
        } else if(type == Type.wave)
        {
            if (SpawnManager.currentWave > number) return true; //Player beaten required wave
            else return false;
        }
        return false;
    }
}

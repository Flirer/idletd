﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Inventory {

    public static Resource Gold;
    public static Resource Knowledge;
    public static Resource Supply;

    public static List<int> Towers;
    public static List<int> Territories;

    public List<int> towers;
    public List<int> territories;
    public Dictionary<int, int> towerResearchParts;     //First int - towerId, second int - number of parts

    public static bool inventoryLoaded = false;

    public delegate void OnInventoryLoaded();
    public static event OnInventoryLoaded onInventoryLoaded;

    [NonSerialized]
    private static Inventory Instance;

    public int startingSupply = 2;

    public void initialize()
    {
        Instance = this;
        Towers = towers;
        Territories = territories;

        GameManager.onGameLoad += updateSupplies;

        initializeResource();
    }

    public void initializeResource()
    {
        Gold = new Resource(SaveLoad.playerData.credits,
            -1);

        Knowledge = new Resource(SaveLoad.playerData.knowledge,
            -1);

        Supply = new Resource(new refData<double>(0),
            startingSupply,
            true);

        inventoryLoaded = true;
        if (onInventoryLoaded != null)
        {
            onInventoryLoaded();
            onInventoryLoaded = null;
        }
    }

    public static void researchTower(int id)
    {
        if (!Towers.Contains(id))
        {
            if (!Instance.towerResearchParts.ContainsKey(id))
                Instance.towerResearchParts.Add(id, 0);

            Instance.towerResearchParts[id]++;

            if (TowerCollection.TowerPrefabs[id].GetComponent<Tower>().researchParts 
                == 
                Instance.towerResearchParts[id])
                Towers.Add(id);

            if (!SaveLoad.playerData.towerStatisticData.ContainsKey(id))
                SaveLoad.playerData.towerStatisticData.Add(id, new TowerStaticData());
            SaveLoad.reqSave();
        }
        
    }

    public static void unlockTerritory(int id)
    {
        //Debug.Log(id);
        if (!Territories.Contains(id))
        {
            Territories.Add(id);
            
            SaveLoad.reqSave();
        }
    }

    public static void updateSupplies()
    {
        double tempSupplies = 0;
        double tempSuppliesMax = Instance.startingSupply;

        foreach (Tower construction in GameManager.constructions)
        {
            tempSupplies += construction.SupplyCost;
            tempSuppliesMax += construction.SupplyGive;
        }

        Supply.amount = tempSupplies;
        Supply.maxAmount = tempSuppliesMax;
    }
}

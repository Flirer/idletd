﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerUpgraidBTN : MonoBehaviour {

    public GameManager gameManager;

    double price;

    public Button btn;

    public Text text;

    // Use this for initialization
    void Start () {

        GameManager.onTowerSelect += attachToTower;
        GameManager.onTowerDeselect += detachFromTower;

        attachToTower();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void attachToTower()
    {
        //Debug.Log("Attach to tower " + gameManager.selectedTower.id);
        gameManager.selectedTower.onUpgraidLevelUp += updateUpgraidPrice;
        Inventory.Gold.onChange += updateUpgraidPrice;
        //gameManager.selectedTower.OnInfoUpdate += updateUpgraidPrice;
    }

    void detachFromTower()
    {
        //Debug.Log("Dettach to tower " + gameManager.selectedTower.id);
        gameManager.selectedTower.onUpgraidLevelUp -= updateUpgraidPrice;
        Inventory.Gold.onChange -= updateUpgraidPrice;
        //gameManager.selectedTower.OnInfoUpdate -= updateUpgraidPrice;
    }

    void updateUpgraidPrice(EventInfo info)
    {
        updateUpgraidPrice();
    }

    void updateUpgraidPrice()
    {
        
        //Debug.Log("Updating selected tower price " + gameManager.selectedTower.upgraidLevel.data);
        if (gameManager.selectedTower != null)
        {
            Tower T = gameManager.selectedTower;
            int towerUpgraidLevel = T.upgraidLevel.data;
            double towerCost = T.goldCost;

            price = (towerCost * (towerUpgraidLevel + 1)) * (Math.Pow(1.5, towerUpgraidLevel));
            text.text = "Upgraid\n" + price;

            checkIfEnoughResource();
        }
    }

    void checkIfEnoughResource()
    {
        if (price < Inventory.Gold.amount)
        {
            makeActive();
        } else
        {
            makeInactive();
        }
    }

    void checkIfEnoughResource(EventInfo info)
    {
        checkIfEnoughResource();
    }

    void makeActive()
    {
        btn.interactable = true;
    }

    void makeInactive()
    {
        btn.interactable = false;
    }

    private void OnEnable()
    {
        updateUpgraidPrice();
    }

    public void Upgraid()
    {
        gameManager.selectedTower.upgraid();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneCollectionItem : MonoBehaviour {

    public Rune rune;

    public Text textName;
    public Text textStats;

    public GameObject icon;

	void Start () {

    }



    public void updateLables()
    {
        textName.text = rune.Name;
        textStats.text = "";
        foreach (RuneStat r in rune.bonusStats)
        {
            textStats.text += r.statType.ToString() + ": " + r.stat.toString() + "\n\n";
        }
    }

}

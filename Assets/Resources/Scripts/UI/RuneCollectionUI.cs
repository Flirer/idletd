﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneCollectionUI : MonoBehaviour {

    public Button buyNewRune;

    public GameObject runeCollectionItemPrefab;
    public Transform runeCollectionHolder;

    // Use this for initialization
    void Start () {
        if (buyNewRune != null)
        {
            buyNewRune.onClick.AddListener(buyNewRuneHandler);
        }

        RuneCollection.renderRune(runeCollectionHolder);
        //renderRunes();
    }

    void buyNewRuneHandler()
    {
        RuneCollection.buyNewRune();
        RuneCollection.renderRune(runeCollectionHolder);
        //renderRunes();
    }



}

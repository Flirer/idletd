﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HotBarItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    GameManager gameManager;

    public string towerName;
    public int goldCost;

    public Text toolTip;

    public GameObject towerPrefab;

    bool available = false;

    public void initialize(GameObject tower, GameManager gm)
    {
        gameManager = gm;
        towerPrefab = tower;
        
        Tower towerScript = tower.GetComponent<Tower>();
        towerName = towerScript.towerName;
        goldCost = towerScript.goldCost;
        
        GetComponent<Button>().onClick.AddListener(OnClick);
        available = true;

        transform.Find("Icon").GetComponent<Image>().sprite = towerScript.icon;

        //if (Inventory.towerResearch[towerScript.prefabId].researched)

        if (Inventory.Towers.Contains(towerScript.prefabId))
        {
            iniBuildBTN();
        } else
        {
            iniResearchBTN();
        }

        transform.Find("Icon").GetComponent<Image>().sprite = towerScript.icon;
    }

    void iniBuildBTN ()
    {
        toolTip.text = towerPrefab.GetComponent<Tower>().generateToolTip();
    }

    void iniResearchBTN ()
    {
        transform.Find("Icon").GetComponent<Image>().color = Color.cyan;
        toolTip.text = towerPrefab.GetComponent<Tower>().generateResearchToolTip();
        
    }

    public void OnClick()
    {
        if (available)
        {
            gameManager.buildBtnClick();
        }
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        gameManager.buildBtnCursorEnter(towerPrefab.GetComponent<Tower>().prefabId);
        toolTip.transform.parent.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        gameManager.buildBtnCursorExit();
        toolTip.transform.parent.gameObject.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        
    }
}

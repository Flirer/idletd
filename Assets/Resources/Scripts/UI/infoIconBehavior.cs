﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class infoIconBehavior : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public GameObject toopTipHolder;
    public Text text;
    public int count = 1;
    public Text countText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void showToolTip()
    {
        toopTipHolder.gameObject.SetActive(true);
    }

    void hideTooltip()
    {
        toopTipHolder.gameObject.SetActive(false);
    }

    public void generateTooltip(string str)
    {
        text.text = str;
        if (count > 1)
        {
            //Debug.Log("Effects: " + count);
            countText.text = count.ToString();
        } else
        {
            //Debug.Log("No effects: " + count);
            countText.text = "";
        }
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        showToolTip();
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        hideTooltip();
    }
}

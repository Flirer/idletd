﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class TowerCollectionItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public Image borders;
    public Image icon;

    public Text nameText;
    public Text supplyCost;
    public Text dpsText;
    public Text levelExpText;
    public Text levelUpgText;

    public Button button;
    Tower tower;


    public void initialize (int prefabId)
    {
        Tower tower = TowerCollection.TowerPrefabs[prefabId].GetComponent<Tower>();
        button.onClick.AddListener(iconClickHandler);
        
        nameText.text = tower.name;
        icon.sprite = tower.icon;
        supplyCost.text = tower.SupplyCost + "";
        

        levelUpgText.text = SaveLoad.playerData.towerStatisticData[tower.prefabId].upgraidLevel.data + "";

        levelExpText.text = SaveLoad.playerData.towerStatisticData[tower.prefabId].level.data + "";
        //tower.level.data + "";

        //dpsText.text = "DPS: \n" + tower.weapons[0].damage.Value * (tower.weapons[0].baseAttackSpeed / tower.weapons[0].attackSpeed.Value);


        this.tower = tower;


        icon.transform.parent.GetComponent<SlotForDragable>().onlyTakeOriginal = true;
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        GameManager.Instance.buildBtnCursorEnter(tower.prefabId);
        //toolTip.transform.parent.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.Instance.buildBtnCursorExit();
        //toolTip.transform.parent.gameObject.SetActive(false);
    }

    void iconClickHandler()
    {
        GameManager.Instance.buildBtnClick();
    }
}
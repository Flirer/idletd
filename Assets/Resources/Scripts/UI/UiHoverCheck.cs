﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UiHoverCheck : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        GameManager.CursorOverUi = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.CursorOverUi = false;
    }
}

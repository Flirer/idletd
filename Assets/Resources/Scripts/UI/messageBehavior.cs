﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class messageBehavior : MonoBehaviour {

    //Message will pop in Air and show information that we want.
    TextMesh txt;

    public int i;
    public bool active = false;

    public void setMessage(Color c, string msg)
    {
        StopAllCoroutines();
        txt.color = c;
        txt.text = msg;
        transform.localPosition = Vector3.zero;
        i = 0;
        active = true;
        StartCoroutine("floating");
    }

	// Use this for initialization
	void Awake () {
        txt = GetComponent<TextMesh>();
        txt.GetComponent<MeshRenderer>().sortingOrder = 100;
    }

    IEnumerator floating()
    {
        while (i < 100)
        {
            i++;
            transform.localPosition += new Vector3(0, 0.0001f, 0)*i;
            yield return null;
        }
        i = 0;
        active = false;
        transform.localPosition = Vector3.zero;
        txt.text = "";
    }
	
	

    
}

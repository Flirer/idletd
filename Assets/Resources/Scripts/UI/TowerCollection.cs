﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInspector;

[AdvancedInspector]
public class TowerCollection : MonoBehaviour {

    [Inspect]
    public List<GameObject> TowersPrefabs;


    public static List<GameObject> TowerPrefabs;

    

    // Use this for initialization
    public void initialization () {
        for (int i = 0; i < TowersPrefabs.Count; i++)
        {
            TowersPrefabs[i].GetComponent<Tower>().prefabId = i;
        }
        TowerPrefabs = TowersPrefabs;
    }
    

    public static void setTowerToSlot(int slot, int towerId)
    {
        SaveLoad.setTowerToSlot(slot, towerId);
    }

    public static int getTowerIdInSlot(int slot)
    {
        return SaveLoad.playerData.actionBarSlots[slot];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildModeBTN : MonoBehaviour {



    public void switchState()
    {
        if (GameManager.buildMode)
        {
            GetComponent<Image>().color = new Color(0.6f, 1, 1, 1.8f);
            transform.GetChild(0).GetComponent<Text>().text = "Game\nMode";
        } else
        {
            GetComponent<Image>().color = Color.white;
            transform.GetChild(0).GetComponent<Text>().text = "Build\nMode";
        }
    }
}

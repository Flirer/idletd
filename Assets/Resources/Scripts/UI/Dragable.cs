﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


/// <summary>
/// Base class for UI elements that need to be dragable.
/// </summary>
public class Dragable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    /// <summary>
    /// GameObject that is dragged
    /// </summary>
    public static GameObject itemBeingDragged;

    /// <summary>
    /// Position, wher we picked up item.
    /// </summary>
    protected Vector3 startPosition;


    protected Transform startParent;

    public Transform homeBase;

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;

        transform.parent.GetComponent<SlotForDragable>().pickItem();
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        Vector3 screenPoint = Input.mousePosition;
        screenPoint.z = 10.0f; //distance of the plane from the camera
        transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        itemBeingDragged = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        if (transform.parent == startParent)
        {
            transform.position = startPosition;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerRuneInsertionUI : MonoBehaviour {

    public static TowerRuneInsertionUI that;

    public Image icon;
    public Text Name;
    public Text Description;

    public GameObject towerRuneSlotPrefab;


    public CanvasRenderer _slotsForRunesHolder;
    public static CanvasRenderer slotsForRunesHolder;

    public static List<GameObject> slots;

    Tower tower;

    RuneCollectionUI runeCollectionUI;

    public static void startInitialize()
    {
        that.iniPreFire();
        
    }

    public void iniPreFire()
    {
        StartCoroutine("initialize");
    }

    //We wait one frame befor first rune render
    IEnumerator initialize()
    {
        yield return null;
        //Debug.Log("Initialize rune insertion UI");

        //slots = new List<GameObject>();

        RuneCollection.slotsForInsertionInTower = new List<GameObject>();

        //tower = Lobby.towerForRuneInsertion;
        icon.sprite = tower.icon;
        Name.text = tower.towerName;

        foreach (Transform child in slotsForRunesHolder.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        for (int i = 0; i < tower.runeSlotsCount; i++)
        {
            GameObject runeSlot = Instantiate(towerRuneSlotPrefab) as GameObject;
            runeSlot.transform.SetParent(slotsForRunesHolder.transform, false);

            //runeSlot.GetComponent<RuneCollectionSlot>().onlyTakeOriginal = true;

            runeSlot.GetComponent<RuneCollectionSlot>().towerPrefabId = tower.prefabId;
            runeSlot.GetComponent<RuneCollectionSlot>().slotId = i;
            runeSlot.name += i;

            runeSlot.GetComponent<RuneCollectionSlot>().slotEmptied += RuneCollection.releaseRune;
            runeSlot.GetComponent<RuneCollectionSlot>().slotFilled += RuneCollection.insertRune;

            //slots.Add(runeSlot);
            RuneCollection.slotsForInsertionInTower.Add(runeSlot);
        }

        RuneCollection.renderRunesForInsertion(runeCollectionUI.runeCollectionHolder);
    }

	// Use this for initialization
	void Awake () {
        that = this;
        slotsForRunesHolder = _slotsForRunesHolder;
        runeCollectionUI = GetComponent<RuneCollectionUI>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

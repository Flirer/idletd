﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Messenger : MonoBehaviour {

    [SerializeField]
    List<messageBehavior> messagesContainer;

    public void sendMessage(Color col, string text)
    {
        getAvailableMessageContainer().setMessage(col, text);
    }

    messageBehavior getAvailableMessageContainer()
    {
        foreach (messageBehavior mc in messagesContainer)
        {
            if (!mc.active) return mc;
        }

        int I = 100;
        int pos = 0;

        for (int j = 0; j < messagesContainer.Count; j++)
        {
            if (messagesContainer[j].i < I) pos =  j;
        }
        return messagesContainer[pos];
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

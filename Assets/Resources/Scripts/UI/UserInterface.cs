﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour
{

    //UI
    Canvas canvas;

    public GameObject infoIconPrefab;

    Transform selectedCreepInfo;
    Text creepName;
    Text creepHp;
    Text creepDamageTaken;

    Transform abilityPanelCreep;
    Transform effectPanelCreep;

    public Text goldAmount;
    public Text supplyAmount;

    Transform selectedTowerInfoHolder;
    Text towerToolTip;

    Transform weaponPanelTower;
    Transform abilityPanelTower;
    Transform effectPanelTower;

    public CanvasRenderer hotBarItemsHolder;
    public GameObject hotBarItemPrefab;
    public List<HotBarItem> hotBarItems;

    int weaponId = 0;
    int abilityId = 0;
    int effectId = 0;

    GameObject tower;
    Tower towerScript;

    List<Weapon> weapons;
    List<Ability> abilities;
    List<Effect> effects;

    Transform towerBuildPanel;
    public GameObject towerBuildCardPrefab;

    GameManager gameManager;

    public Text TextWaveNumber;
    public Text TextWaveHp;


    Dictionary<int, infoIconBehavior> effectIdIndexes;

    // Use this for initialization
    public void initialization()
    {
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        selectedTowerInfoHolder = canvas.transform.Find("SelectedTowerInformation");
        towerToolTip = selectedTowerInfoHolder.Find("TowerToolTip").GetComponent<Text>();

        weaponPanelTower = selectedTowerInfoHolder.Find("WeaponPanel");
        abilityPanelTower = selectedTowerInfoHolder.Find("AbilityPanel");
        effectPanelTower = selectedTowerInfoHolder.Find("EffectPanel");


        selectedCreepInfo = canvas.transform.Find("SelectedCreepInfo");
        creepName = selectedCreepInfo.Find("Name").GetComponent<Text>();
        creepHp = selectedCreepInfo.Find("Health").GetComponent<Text>();
        creepDamageTaken = selectedCreepInfo.Find("DamageTaken").GetComponent<Text>();

        abilityPanelCreep = selectedCreepInfo.Find("AbilityPanel");
        effectPanelCreep = selectedCreepInfo.Find("EffectPanel");

        towerBuildPanel = canvas.transform.Find("TowerBuildPanel");

        gameManager = GetComponent<GameManager>();

        GameManager.onTowerSelect += towerSelectHandler;
        GameManager.onTowerDeselect += towerDeselectHandler;
        GameManager.onCreepSelect += creepSelectHandler;
        GameManager.onCreepDeselect += creepDeselectHandler;
        GameManager.onHexSelect += hexSelectHandler;
        GameManager.onHexDeselect += hexDeselectHandler;

        creepName = selectedCreepInfo.transform.Find("Name").GetComponent<Text>();
        creepHp = selectedCreepInfo.transform.Find("Health").GetComponent<Text>();
        creepDamageTaken = selectedCreepInfo.transform.Find("DamageTaken").GetComponent<Text>();

        if (Inventory.inventoryLoaded)
        {
            initializeResourceLabels();
        } else
        {
            Inventory.onInventoryLoaded += initializeResourceLabels;
        }
    }

    void initializeResourceLabels()
    {
        Inventory.Gold.onChange += updateGoldAmount;
        updateGoldAmount();
        Inventory.Supply.onChange += updateSupplyAmount;
        updateSupplyAmount();
    }

    void initializeTowerCard(GameObject tower)
    {

        towerScript = tower.GetComponent<Tower>();

        weapons = towerScript.weapons;
        foreach (Weapon wep in weapons)
        {
            wep.id = weaponId;
            weaponId++;
        }

        abilities = towerScript.abilities;
        foreach (Ability abil in abilities)
        {
            abil.id = abilityId;
            abilityId++;

            abil.effectId = effectId;
            effectId++;
        }
    }

    void showActionBar()
    {
        foreach (Transform trns in hotBarItemsHolder.transform)
        {
            Destroy(trns.gameObject);
        }
        if (gameManager.selectedTower == null) //Hex selected, show basic options for building
        {

            hotBarItems = new List<HotBarItem>();
            for (int i = 0; i < 3; i++)
            {
                GameObject hbi = Instantiate(hotBarItemPrefab) as GameObject;
                hbi.transform.SetParent(hotBarItemsHolder.transform);
                hbi.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
                hbi.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
                hbi.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                hbi.GetComponent<RectTransform>().position = new Vector3(45 + 90 * i, 45, 0);
                hotBarItems.Add(hbi.GetComponent<HotBarItem>());

                int towerIdForSlot = TowerCollection.getTowerIdInSlot(i);
                //Debug.Log(towerIdForSlot);

                //if (TowerCollection.towersPathToPrefab[i] != "na")
                if (TowerCollection.TowerPrefabs[i])
                {
                    GameObject tower = TowerCollection.TowerPrefabs[i];
                    hotBarItems[i].initialize(tower, GetComponent<GameManager>());
                    initializeTowerCard(hotBarItems[i].towerPrefab);
                }
                else Debug.LogError("Could not load Tower id " + i);

            }

        } else  //Tower is selected - show options for upgraid
        {

        }
    }

    void hideActionBar()
    {
        foreach (Transform trns in hotBarItemsHolder.transform)
        {
            Destroy(trns.gameObject);
        }
    }

    void showTowerBuildMenu()
    {
        foreach (Transform trns in towerBuildPanel)
        {
            Destroy(trns.gameObject);
        }
        towerBuildPanel.gameObject.SetActive(true);
        foreach (int prefId in Inventory.Towers)
        {
            GameObject card = Instantiate(towerBuildCardPrefab) as GameObject;
            card.transform.SetParent(towerBuildPanel, false);
            card.GetComponent<TowerCollectionItem>().initialize(prefId);
        }
    }

    void hideTowerBuildMenu()
    {
        towerBuildPanel.gameObject.SetActive(false);
    }

    void updateSelectedTowerInfo(EventInfo info)
    {
        updateSelectedTowerInfo(info.tower);
    }

    void updateSelectedTowerInfo(Tower tower)
    {
        towerToolTip.text = tower.generateSelectInfoTip();

        //Weapon
        //1st - clear all pre info
        foreach (Transform child in weaponPanelTower)
        {
            Destroy(child.gameObject);
        }

        foreach (Transform child in abilityPanelTower)
        {
            Destroy(child.gameObject);
        }

        foreach (Transform child in effectPanelTower)
        {
            Destroy(child.gameObject);
        }

        foreach (Weapon wep in tower.weapons)
        {
            GameObject wepIcon = Instantiate(infoIconPrefab) as GameObject;
            wepIcon.transform.SetParent(weaponPanelTower.transform, false);
            if (wep.icon)
            {
                wepIcon.GetComponent<Image>().sprite = wep.icon;
            }
            wepIcon.GetComponent<infoIconBehavior>().generateTooltip(wep.generateToolTip());
        }

        foreach (Ability abil in tower.abilities)
        {
            GameObject abilIcon = Instantiate(infoIconPrefab) as GameObject;
            abilIcon.transform.SetParent(abilityPanelTower.transform, false);
            if (abil.icon)
            {
                abilIcon.GetComponent<Image>().sprite = abil.icon;
            }
            
            abilIcon.GetComponent<infoIconBehavior>().generateTooltip(abil.generateToolTip());
        }

        effectIdIndexes = new Dictionary<int, infoIconBehavior>();

        foreach (Effect eff in tower.effects)
        {

            if (effectIdIndexes.ContainsKey(eff.effectId))
            {
                effectIdIndexes[eff.effectId].count++;
                effectIdIndexes[eff.effectId].generateTooltip(eff.generateToolTip());
            } else
            {

                GameObject effectIcon = Instantiate(infoIconPrefab) as GameObject;
                effectIcon.transform.SetParent(effectPanelTower.transform, false);
                if (eff.icon)
                {
                    effectIcon.GetComponent<Image>().sprite = eff.icon;
                }

                effectIdIndexes.Add(eff.effectId, effectIcon.GetComponent<infoIconBehavior>());

                effectIcon.GetComponent<infoIconBehavior>().generateTooltip(eff.generateToolTip());
            }
        }
    }

    void updateSelectedCreepInfo(EventInfo info)
    {
        updateSelectedCreepInfo(info.creep);
    }

    void updateSelectedCreepInfo(Creep creep)
    {
        creepName.text = creep.name;
        creepHp.text = creep.hpCurrent + "/" + creep.hpMax.Value;
        creepDamageTaken.text = "" + creep.slowFactor.Value;
        //TODO: Implent damage taken

        foreach (Transform child in abilityPanelCreep)
        {
            Destroy(child.gameObject);
        }

        foreach (Transform child in effectPanelCreep)
        {
            Destroy(child.gameObject);
        }

        foreach (Ability abil in creep.abilities)
        {
            GameObject abilIcon = Instantiate(infoIconPrefab) as GameObject;
            abilIcon.transform.SetParent(abilityPanelCreep.transform, false);
            if (abil.icon)
            {
                abilIcon.GetComponent<Image>().sprite = abil.icon;
            }

            abilIcon.GetComponent<infoIconBehavior>().generateTooltip(abil.generateToolTip());
        }

        effectIdIndexes = new Dictionary<int, infoIconBehavior>();

        foreach (Effect eff in creep.effects)
        {

            if (effectIdIndexes.ContainsKey(eff.effectId))
            {
                effectIdIndexes[eff.effectId].count++;
                effectIdIndexes[eff.effectId].generateTooltip(eff.generateToolTip());
            }
            else
            {

                GameObject effectIcon = Instantiate(infoIconPrefab) as GameObject;
                effectIcon.transform.SetParent(effectPanelCreep.transform, false);
                if (eff.icon)
                {
                    effectIcon.GetComponent<Image>().sprite = eff.icon;
                }

                effectIdIndexes.Add(eff.effectId, effectIcon.GetComponent<infoIconBehavior>());

                effectIcon.GetComponent<infoIconBehavior>().generateTooltip(eff.generateToolTip());
            }
        }
    }



    void updateGoldAmount()
    {
        goldAmount.text = Inventory.Gold.amount + "";
    }

    void updateSupplyAmount()
    {
        supplyAmount.text = Inventory.Supply.amount + "/" + Inventory.Supply.maxAmount;
    }

    public void updateWaveInfo()
    {
        TextWaveNumber.text = "Wave: " + SpawnManager.currentWave;
        TextWaveHp.text = "HP: " + gameManager.spawnManager.hpForWave;
    }

    #region selectHandlers

    void hexSelectHandler()
    {
        if (GameManager.buildMode) showTowerBuildMenu();
    }

    void hexDeselectHandler()
    {
        hideTowerBuildMenu();
    }

    void towerSelectHandler()
    {
        updateSelectedTowerInfo(gameManager.selectedTower);
        selectedTowerInfoHolder.gameObject.SetActive(true);

        gameManager.selectedTower.onInfoUpdate += updateSelectedTowerInfo;
        gameManager.selectedTower.onUpgraidLevelUp += updateSelectedTowerInfo;
        gameManager.selectedTower.onLevelUp += updateSelectedTowerInfo;
    }

    void towerDeselectHandler()
    {
        selectedTowerInfoHolder.gameObject.SetActive(false);

        gameManager.selectedTower.onInfoUpdate -= updateSelectedTowerInfo;
        gameManager.selectedTower.onUpgraidLevelUp -= updateSelectedTowerInfo;
        gameManager.selectedTower.onLevelUp -= updateSelectedTowerInfo;
    }

    void creepSelectHandler()
    {
        updateSelectedCreepInfo(gameManager.selectedCreep);
        selectedCreepInfo.gameObject.SetActive(true);
        gameManager.selectedCreep.onInfoUpdate += updateSelectedCreepInfo;
    }

    void creepDeselectHandler()
    {
        selectedCreepInfo.gameObject.SetActive(false);

        gameManager.selectedCreep.onInfoUpdate -= updateSelectedCreepInfo;
    }

    #endregion
}
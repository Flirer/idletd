﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SlotForDragable : MonoBehaviour, IDropHandler
{

    public delegate void onSlotFill();
    public event onSlotFill slotFilled;

    public delegate void onSlotEmpty();
    public event onSlotEmpty slotEmptied;

    /// <summary>
    /// If true, mean it will save slot for only original item, that should be nested here (ex: put spell back to spell book from action bar)
    /// </summary>
    public bool onlyTakeOriginal = false;

    public static SlotForDragable triggeredSlot;
    public static GameObject lastPickedItem;
    public static GameObject lastDropedItem;

    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public virtual void OnDrop(PointerEventData eventData)
    {
        //Drop check sequence
        //First check: If slot is empty
        if (!item)
        {
            //Second check: if expect only original item
            if (onlyTakeOriginal)
            {
                //We check if slot of item is original
                if (transform != Dragable.itemBeingDragged.GetComponent<Dragable>().homeBase)
                {
                    //If its not the origianl slot, we stop function, item will return from where it was picked up.
                    return;
                }
            }

            //Or we put item to this slot.
            Dragable.itemBeingDragged.transform.SetParent(transform);
            Dragable.itemBeingDragged.transform.localPosition = Vector3.zero;

            triggeredSlot = this;
            lastDropedItem = Dragable.itemBeingDragged;
            dropItem();
        }
    }

    /// <summary>
    /// Call for event and reset slotNumber for Hot Bar if tower was picked up from hot bar.
    /// </summary>
    public virtual void pickItem()
    {
        triggeredSlot = this;
        lastPickedItem = item;
        if (slotEmptied != null)
        {
            slotEmptied();
        }
    }

    /// <summary>
    /// Call event and assign new HotBar slot for tower;
    /// </summary>
    public virtual void dropItem()
    {
        if (slotFilled != null)
        {
            slotFilled();
        }
    }

    private void OnDestroy()
    {
        slotEmptied = null;
        slotFilled = null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class talantControlUI : MonoBehaviour {


    public Text NameText;
    string Name;
    public Text DescriptionText;
    string Description;
    public int level;
    public int levelMax;

    public Talants.talantType type;
    Talant talant;

    public Button plus;
    public Button minus;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void initialized(Talant tal)
    {
        level = tal.level;
        levelMax = tal.levelMax;
        Name = tal.name;
        Description = tal.description;
        talant = tal;
        updateUI();

        plus.onClick.AddListener(inc);
        minus.onClick.AddListener(dec);

        talant.OnTalantChange += updateUI;

    }

    public void updateUI()
    {
        NameText.text = Name + "(" + talant.level +  "/" + levelMax + ")";
        DescriptionText.text = Description;
    }

    public void inc()
    {
        //lobby.talants.addTalantPoint(type);
        Debug.Log("Add talant point");
        talant.addPoint();
    }

    public void dec()
    {
        talant.removePoint();
        //lobby.talants.removeTalantPoint(type);
    }
}

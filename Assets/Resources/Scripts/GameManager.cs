﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public static bool CursorOverUi = false;

    public delegate void OnEnteringBuildMode();
    public static event OnEnteringBuildMode onEnteringBuildMode;

    public delegate void OnEnteringGameMode();
    public static event OnEnteringGameMode onEnteringGameMode;

    public delegate void OnTowerSelect();
    public static event OnTowerSelect onTowerSelect;

    public delegate void OnTowerDeselect();
    public static event OnTowerDeselect onTowerDeselect;

    public delegate void OnCreepSelect();
    public static event OnCreepSelect onCreepSelect;

    public delegate void OnCreepDeselect();
    public static event OnCreepDeselect onCreepDeselect;

    public delegate void OnHexSelect();
    public static event OnHexSelect onHexSelect;

    public delegate void OnHexDeselect();
    public static event OnHexDeselect onHexDeselect;

    public delegate void OnGameLoad();
    public static event OnGameLoad onGameLoad;


    public GameObject hexPrefab;

	public int 
            hexRows = 10,                                       //number of rows for hex map generator
            hexColumns = 10;                                    //number of columns for hex map generator

    int
        hexAmount,                                              //total number of hexes on map
        startingHexId = 309,                                      //Id for starting and ending hexes
        endingHexId = 313;

    public List<int> waypoints;

    //public Hex[] hexes;                                         //Array with all hexes indixed by id
    public Dictionary<int, Hex> hexes;

    public List<List<GameObject>> 
        hexesTable = new List<List<GameObject>>();              //Table with Hexes wich can be accesed by coordinates (x, y)

    
    public Dictionary<Hex, Hex> 
        uncheckedHexes = new Dictionary<Hex, Hex> ();     //Hexes to Check
    public Dictionary<Hex, Hex> 
        unchekedHexNextGroup = new Dictionary<Hex, Hex> ();

	public Hex currentDestinationCheck;

    public Hex startingHex;
	public Hex endingHex;

	public List<Hex> hexesWithCreeps;

    List<Hex> rangeCheckHexList = new List<Hex>();


    public SpawnManager spawnManager;


    //TOWERS
    public GameObject towerToBuild;
    public GameObject towerToMove;

    Hex towerToMoveOriginalLocation;

    bool towerBeingMoved = false;
    bool canBePlaced = false;
    Hex hexForMovingTower;


    public GameObject towerPlacement;
    int towerId = 0;                                            //Counter for towers to give them uniq id
	public bool towerPlacmentMode = false;
    public static List<Tower> constructions = new List<Tower>();

    public List<Zone> zones;
    public static Zone currentZone;
    

    //USERINTERFACE

    public UserInterface userInterface;

    public Creep selectedCreep;
    public Tower selectedTower;
    public Hex selectedHex;

    public Camera cam;

    public static bool buildMode = true;


    public void initialization()
    {
        Instance = this;
        if (SaveLoad.newGame)
            iniNewGame();
         else
            loadGame();

        if (onGameLoad != null) onGameLoad();
    }

    void iniNewGame()
    {
        zones = SaveLoad.playerData.zones;

        if (zones.Count == 0 )                                              //If no zones is presented, we create new, initial zone
            zones.Add(new Zone());

        currentZone = zones[0];

        hexAmount = hexRows * hexColumns;
        hexes = new Dictionary<int, Hex>();
        createHexField();

        foreach (int key in hexes.Keys)
        {
            hexes[key].initialize();
        }


        startingHex = hexes[startingHexId];
        endingHex = hexes[endingHexId];
        startingHex.buildable = false;
        startingHex.spriteRenderer.sprite = startingHex.spriteEnterPortal;
        endingHex.buildable = false;
        endingHex.spriteRenderer.sprite = endingHex.spriteEndPortal;

        rebuildPathing();

        spawnManager.ini(startingHex, endingHex, currentZone.LastWaveBeaten+1);
        
        Vector3 camPosition = hexes[startingHexId].transform.position;
        camPosition.z = -10;

        cam.transform.position = camPosition;

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && towerBeingMoved) endMoveTower(towerToMoveOriginalLocation);
    }

    public static void rebuildPathing()
    {
        Instance.mapNewDestinationIni(Instance.endingHex);

        foreach (int waypoint in Instance.waypoints)
        {
            Instance.mapNewDestinationIni(Instance.hexes[waypoint]);
        }
    }

    void loadGame()
    {
        iniNewGame();
        foreach(TowerSavedLocationData data in currentZone.towers)
        {
            loadTower(data);
        }
    }


    public void selectTower(EventInfo info)
    {
        selectTower(info.tower);
    }

    public void selectTower(Tower tower)
    {
        deselect();
        selectedTower = tower;

        EventInfo info = new EventInfo();
        info.tower = selectedTower;
        tower.selectHandler();
        if (onTowerSelect != null) onTowerSelect();
    }

    public void selectCreep(EventInfo info)
    {
        selectCreep(info.creep);
    }

    public void selectCreep(Creep creep)
    {
        deselect();
        selectedCreep = creep;

        if (onCreepSelect != null) onCreepSelect();
    }

    public void selectHex(EventInfo info)
    {
        if (towerBeingMoved)
        {
            if (canBePlaced)
            {
                endMoveTower(hexForMovingTower);
            }
        } else {
            selectHex(info.hex);
        }
    }

    public void selectHex(Hex hex)
    {
        deselect();                                             //Remove all previouse selection
        
        if (Inventory.Territories.Contains(hex.territoryId))    //If hex is opened
        {
            if (hex.tower != null)                              //If hex have a tower on it - select tower, else select hex
                selectTower(hex.tower);
            else
                selectedHex = hex;

            if (onHexSelect != null) onHexSelect();
        } else                                                  //Selecting territory
        {
            TerritoryManager.clickOnTerritory(hex.territoryId);
        }
    }

    void deselect()
    {

        if (selectedCreep != null)
        {
            if (onCreepDeselect != null) onCreepDeselect();

            selectedCreep = null;
        }

        if (selectedTower != null)
        {
            selectedTower.deselectHandler();
            if (onTowerDeselect != null) onTowerDeselect();
            selectedTower = null;
        }

        if (selectedHex)
        {
            if (onHexDeselect != null) onHexDeselect();

            selectedHex.markAsDeselected();
            selectedHex = null;
        }
    }

    public static void clickOnEmptySpace()
    {
        Instance.deselect();
    }

    public void deselectDestroyedCreep(EventInfo info)
    {
        if (selectedCreep == info.creep) deselect();
    }

    /*
	 * ---#--#--###--#---#--###---####-
	 * ---#--#--#-----#-#---#----#-----
	 * ---####--###----#----###---###--
	 * ---#--#--#-----#-#---#--------#-
	 * ---#--#--###--#---#--###--####--
	 * */

    void createHexField() {

		int id = 0;
		float x = 0;

		GameObject singleHex;
		Hex hexScript;

		for (int i = 0; i < hexColumns; i++) {


			if (i % 2 == 0) {
				x = 24f;
			} else {
				x = 0;
			}


			hexesTable.Add(new List<GameObject>()); 

			for (int j = 0; j < hexRows; j++) {
				 singleHex = Instantiate (
					hexPrefab, 
					new Vector3(48*j + x,  i* 36, 0), 
					Quaternion.identity
				) as GameObject;

				singleHex.name = "hex:" + i + ":" + j + ":ID - " + id;

				hexScript = singleHex.GetComponent<Hex>();
				hexScript.id = id;
				if (x > 0) {
					hexScript.shift = true;
				}
				hexScript.x = j;
				hexScript.y = i;
				hexScript.gameManager = this;
				hexes [id] = hexScript;

                hexScript.onClick += selectHex;

                id++;
				hexesTable[i].Add (singleHex);
			}

		}

        TerritoryManager.initializeTerritories();

        /*
        foreach (KeyValuePair<int, Hex> hex in hexes)
        {
            if (hex.Value.territoryId == -1)
            {
                Destroy(hex.Value.gameObject);
            }
        }

        for (var i = hexes.Count - 1; i > -1; i--)
        {
            if (hexes[i].territoryId == -1)
            {
                hexes.Remove(i);
            }
        }*/
        for (var i = hexes.Count - 1; i > -1; i--)
        {
            if (hexes[i].territoryId == -1)
            {
                Destroy(hexes[i].gameObject);
                hexes.Remove(i);
            }
        }
    }

    
	public bool mapNewDestinationIni(Hex dest) {
		//Clear if prev we had some path:
		foreach (KeyValuePair<int, Hex> hex in hexes) {
			hex.Value.destinationsChecking.Remove (dest);
			hex.Value.updateDebugInfo ();
		}

        
		dest.destinationsChecking [dest] = new destinationNextStep (dest, 0 , null);
		currentDestinationCheck = dest;

        //First we get all hexes around destination and put them into checking list
        foreach (Hex hex in dest.neighborList) {
			if (hex != null) {
				//Adding hex to check and parrent to track back
				if (hex.walkable) {
					unchekedHexNextGroup.Add (hex, dest);
				}
			}
		}

		//Second - we map new path into temp storage to check it later, and we put all neighbors of maped hex into checking list
		while (unchekedHexNextGroup.Count > 0) {

			uncheckedHexes.Clear ();
			uncheckedHexes = new Dictionary<Hex, Hex> (unchekedHexNextGroup);

			unchekedHexNextGroup.Clear ();
			List<Hex> openHexesPathfindingList = new List<Hex> ();

			foreach (KeyValuePair<Hex, Hex> hexPair in uncheckedHexes) {
				openHexesPathfindingList.Add (hexPair.Key);
			}

			foreach (Hex hex in openHexesPathfindingList) {
				mapNewDestinationToHex (hex, uncheckedHexes[hex]);
				uncheckedHexes.Remove (uncheckedHexes[hex]);
			}
		}

		//Third step - we check if all important hexes have path to destination:
		bool pathIsAvailable = true;


        if (startingHex.destinationsChecking.ContainsKey(currentDestinationCheck)) {
            foreach (Hex _1hex in hexesWithCreeps)
            {
                if (!_1hex.destinationsChecking.ContainsKey(currentDestinationCheck))
                {
                    pathIsAvailable = false;
                }
            }
            foreach (int waypoint in waypoints)
            {
                if (!hexes[waypoint].destinationsChecking.ContainsKey(currentDestinationCheck))
                {
                    pathIsAvailable = false;
                }
            }
        } else {
            pathIsAvailable = false;
		}

		//Last step - if Path is available we move destinationNext to destination;

		if (pathIsAvailable) {
			foreach (KeyValuePair<int, Hex> _hex in hexes) {
				if (_hex.Value.destinationsChecking.ContainsKey(currentDestinationCheck)) {
					//_hex.Value.destinations.Remove (currentDestinationCheck);
					_hex.Value.destinations [currentDestinationCheck] = new destinationNextStep (_hex.Value.destinationsChecking [currentDestinationCheck]);
                    _hex.Value.updateDebugInfo();
                }
			}

            return true;
		} else {
            return false;
		}
	}

	//Map hex and add its neighbor to checklist
	public void mapNewDestinationToHex(Hex hexToMap, Hex parent) {

		//We want to either add new destination (wich will be parent.step+1 length) or check if new path is better
		if (!hexToMap.destinationsChecking.ContainsKey (currentDestinationCheck)) {
            hexToMap.destinationsChecking.Add (currentDestinationCheck, new destinationNextStep (hexToMap, parent.destinationsChecking [currentDestinationCheck].Step + 1, parent));
			foreach (Hex hex in hexToMap.neighborList) {
				//Check if not NULL, walkable, it is not alredy checked and not alredy put to next check queue
				if (hex != null &&
				    hex.walkable &&
				    !hex.destinationsChecking.ContainsKey (currentDestinationCheck) &&
				    !unchekedHexNextGroup.ContainsKey (hex)) {
                    unchekedHexNextGroup.Add (hex, hexToMap);
				} /*else
                {
                    if (hex == null) Debug.Log(" - * - denied, does not exist " + hex.id);
                    if (!hex.walkable) Debug.Log(" - * - denied, not walkable " + hex.id);
                    if (hex.destinationsChecking.ContainsKey(currentDestinationCheck)) Debug.Log(" - * - denied, alredy have destination " + hex.id);
                    if (unchekedHexNextGroup.ContainsKey(hex)) Debug.Log(" - * - denied, alredy added " + hex.id);
                }*/
            }
		} else {
            //We alredy have path here, lets check if there is better way
            destinationNextStep nextDest = hexToMap.destinationsChecking[currentDestinationCheck];
			destinationNextStep parentDest = parent.destinationsChecking[currentDestinationCheck];
			if (nextDest.Step > parentDest.Step) {
                //hexToMap.destinationsChecking.Add (currentDestinationCheck, new destinationNextStep (hexToMap, parent.destinationsChecking [currentDestinationCheck].Step + 1, parent));
                hexToMap.destinationsChecking[currentDestinationCheck] = new destinationNextStep(hexToMap, parent.destinationsChecking[currentDestinationCheck].Step + 1, parent);
			}

		}
    }


    /*
	 *----##---###---###--###--###----####-
	 *---#--#--#--#--#----#----#--#--#-----
	 *---#-----###---###--###--###----###--
	 *---#--#--#-#---#----#----#---------#-
	 *----##---#--#--###--###--#-----####--
	 * */

    public void reachTheEnd(Creep crp) {

        //1 - teleport creep back
        crp.moveTo(startingHex, spawnManager.path);
        //2 -Set to spawnManager creepReached exit and we resend the same wave
        spawnManager.creepReachExit = true;
	}

    public void exitBackToLobby()
    {
        PlayerPrefs.SetInt("goldCount", PlayerPrefs.GetInt("goldCount") + SpawnManager.currentWave);
        SceneManager.LoadScene(0);
    }

	/*
	#####----#----#---#--###--###----####-
	  #----#---#--#-#-#--#----#--#--#-----
	  #----#---#--#-#-#--###--###----###--
	  #----#---#--#-#-#--#----#-#-------#-
	  #------#-----#-#---###--#--#--####--
	*/
	public void towerBuild() {

		if (checkPathingAvailableWithoutHex(selectedHex)) {


            Inventory.Gold.amount -= (towerToBuild.GetComponent<Tower>().goldCost);

            SaveLoad.playerData.towerStatisticData[towerToBuild.GetComponent<Tower>().prefabId].numberBuilded.data++;

            towerPlacmentMode = false;

            Tower tower = createTower(towerToBuild, selectedHex);

            Inventory.updateSupplies();

            currentZone.towers.Add(new TowerSavedLocationData(tower));

            Destroy (towerPlacement);

            SaveLoad.updateSaveData();
        }
	}

    void loadTower (TowerSavedLocationData data)
    {
        if (checkPathingAvailableWithoutHex(hexes[data.hex]))
        {
            towerToBuild = TowerCollection.TowerPrefabs[data.prefabId];
            Tower tower = createTower(towerToBuild, hexes[data.hex]);

            data.initialize(tower);

            Inventory.updateSupplies();
            SaveLoad.updateSaveData();
        } else
        {
            Debug.LogError("Cant build Tower ");
        }
        
    }

    public static Tower CreateTower(GameObject prefab, Hex hex)
    {
        Tower tower =  Instance.createTower(prefab, hex);

        currentZone.towers.Add(new TowerSavedLocationData(tower));

        return tower;
    }

    Tower createTower(GameObject prefab, Hex hex)
    {
        GameObject _tower = Instantiate(
                                        prefab,
                                        hex.transform.position,
                                        Quaternion.identity
                                    ) as GameObject;
        _tower.name = "Tower_" + towerId;

        Tower tow = _tower.GetComponent<Tower>();
        tow.build(hex);
        tow.id = towerId;
        towerId++;
        tow.onClick += selectTower;
        constructions.Add(tow);
        Inventory.updateSupplies();
        return tow;
    }

    public void removeSelectedTower()
    {
        selectedTower.locationHex.removeTower();
        selectedTower.removeTowerIni();
        constructions.Remove(selectedTower);
        currentZone.removeTower(selectedTower);
        deselect();
        Inventory.updateSupplies();
        SaveLoad.reqSave();
    }

    bool checkPathingAvailableWithoutHex(Hex hex)
    {
        hex.walkable = false;

        bool pathIsOk = true;
        foreach (int waypoint in waypoints)
        {
            if (!mapNewDestinationIni(hexes[waypoint])) {
                pathIsOk = false;
            }
        }

        if (pathIsOk) pathIsOk = mapNewDestinationIni(endingHex);
        hex.walkable = true;

        if (pathIsOk) return true;
        else
        {
            //Revert back all maping
            mapNewDestinationIni(endingHex);
            foreach (int waypoint in waypoints)
            {
                mapNewDestinationIni(hexes[waypoint]);
            }
            return false;
        }
    }

    public void beginMoveTower()
    {
        towerToMoveOriginalLocation = selectedTower.locationHex;
        selectedTower.locationHex.removeTower();
        towerToMove = selectedTower.gameObject;
        towerToMove.GetComponent<Tower>().deselectHandler();
        towerBeingMoved = true;
        towerToMove.GetComponent<Collider2D>().enabled = false;

        selectedTower.initializeMove();


        StartCoroutine(moveTower());
    }

    IEnumerator moveTower()
    {
        RaycastHit2D hit;
        Hex lastCheckedHex = null;

        while (towerBeingMoved)
        {
            hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit)
            {
                //Transform objectHit = hit.transform;
                Hex hex = hit.transform.GetComponent<Hex>();
                if (hex && hex.buildable)
                {
                    if (lastCheckedHex == null || lastCheckedHex != hex)
                    {
                        lastCheckedHex = hex;
                        towerToMove.transform.position = hex.transform.position;
                        hexForMovingTower = hex;
                        if (checkPathingAvailableWithoutHex(hex))
                        {
                            canBePlaced = true;
                        }
                        else
                        {
                            Vector3 pos = cam.ScreenToWorldPoint(Input.mousePosition);
                            pos.z = 0;
                            towerToMove.transform.position = pos;
                            canBePlaced = false;
                        }
                    }
                    
                } else
                {
                    //Debug.Log("Cant build here");
                    canBePlaced = false;
                    Vector3 pos = cam.ScreenToWorldPoint(Input.mousePosition);
                    pos.z = 0;
                    towerToMove.transform.position = pos;
                }
            }
            towerToMove.GetComponent<Tower>().whileMove();
            yield return null;
        }
    }

    void endMoveTower(Hex hex)
    {
        towerToMove.GetComponent<Tower>().locationHex = hex;
        hex.beingBuildUpon(towerToMove.GetComponent<Tower>());
        towerBeingMoved = false;
        towerToMove.transform.position = hex.transform.position;
        towerToMove.GetComponent<Collider2D>().enabled = true;
        towerToMove.GetComponent<Tower>().endMove();
    }


    public void towerButtonClick() {
        Tower tower = towerToBuild.GetComponent<Tower>();
        //if (Inventory.towerResearch[tower.prefabId].researched)
        if (Inventory.Towers.Contains(tower.prefabId))
        {
            if (tower.buildMax > 0 &&
                SaveLoad.playerData.towerStatisticData[tower.prefabId].numberBuilded.data >= tower.buildMax)
            {
                Debug.Log("Maximum amount builded alredy!");
                return;
            }

            if (Inventory.Gold.amount < tower.goldCost)
            {
                Debug.Log("Not enough gold!");
                return;
            }
            if ((Inventory.Supply.maxAmount - Inventory.Supply.amount) 
                <
                tower.SupplyCost)
            {
                Debug.Log("Not enough supply!");
                return;
            }
            if (!selectedHex.buildable)
            {
                Debug.Log("Cant build on this hex");
                return;
            } 

            if (selectedHex.creepsInHex.Count > 0)
            {
                Debug.Log("Creeps inside hex!");
                return;
            }

            cleareRangeCheck();
            towerBuild();
            deselect();
        } else
        {
            if (Inventory.Knowledge.amount < towerToBuild.GetComponent<Tower>().researchCost)
            {
                Debug.Log("Not enough Knowledge!");
                return;
            }

            Inventory.researchTower(towerToBuild.GetComponent<Tower>().prefabId);
            Inventory.Knowledge.amount -= (towerToBuild.GetComponent<Tower>().researchCost);
        }
    }

    public void buildBtnCursorEnter(int id)
    {
        towerToBuild = TowerCollection.TowerPrefabs[id];
        
        if (Inventory.Towers.Contains(towerToBuild.GetComponent<Tower>().prefabId))
        {
            if (!towerPlacmentMode)
            {
                towerPlacmentMode = true;
                towerPlacement = Instantiate(
                    towerToBuild.GetComponent<Tower>().placementDecoy,
                    selectedHex.transform.position,
                    Quaternion.identity
                ) as GameObject;
            }
            else
            {
                Destroy(towerPlacement);
                towerPlacement = Instantiate(
                    towerToBuild.GetComponent<Tower>().placementDecoy,
                    selectedHex.transform.position,
                    Quaternion.identity
                ) as GameObject;
            }

            towerPlacement.transform.position = selectedHex.transform.position;

            if (selectedHex.buildable && selectedHex.creepsInHex.Count == 0)
            {
                towerPlacement.GetComponent<SpriteRenderer>().color = Color.green;
            }
            else
            {
                towerPlacement.GetComponent<SpriteRenderer>().color = Color.red;
            }
            if (selectedHex.walkable)
            {
                selectedHex.walkable = false;
                if (!mapNewDestinationIni(endingHex)) towerPlacement.GetComponent<SpriteRenderer>().color = Color.red;
                selectedHex.walkable = true;
            }
            
            int towerRange = towerToBuild.GetComponent<Tower>().getRange();
            if (towerRange > 0)
                rangeCheck(selectedHex, towerRange);
        } 
        
    }

    public void buildBtnCursorExit()
    {
        cleareRangeCheck();
        if (towerPlacement)
        {
            Destroy(towerPlacement);
        }
    }

    public void buildBtnClick()
    {
        towerButtonClick();
    }



    public void towerBuildCancel() {
		towerPlacmentMode = false;
		Destroy (towerPlacement);
	}

    public void rangeCheck(Hex hex, int range)
    {
        cleareRangeCheck();
        rangeCheckHexList = hex.getNeighborsInRange(range);
        //Debug.Log(rangeCheckHexList.Count);
        foreach (Hex _hex in rangeCheckHexList)
        {
            _hex.showRangeCheckMarker();
        }
    }

    public void cleareRangeCheck()
    {
        //Debug.Log(rangeCheckHexList.Count);
        foreach (Hex hex in rangeCheckHexList)
        {
            hex.cleareRangeCheck();
        }
        rangeCheckHexList.Clear();
    }

    

    //TIME

    public void pauseGame()
    {
        Time.timeScale = 0;
    }

    public void gameSpeed1x()
    {
        Time.timeScale = 1;
    }

    public void gameSpeed2x()
    {
        Time.timeScale = 2;
    }

    public void gameSpeed4x()
    {
        Time.timeScale = 4;
    }

    public void switchBuildMode()
    {
        if (buildMode)
        {
            buildMode = false;
            if (onEnteringGameMode != null)
            {
                onEnteringGameMode();
            }
        } else
        {
            buildMode = true;
            if (onEnteringBuildMode != null)
            {
                onEnteringBuildMode();
            }
        }
    }
}
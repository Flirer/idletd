﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class TowerModel : MonoBehaviour {

    public delegate void Animation_buildComplete();
    public Animation_buildComplete buildCompleteEvent;

    public delegate void Animation_removeFinish();
    public Animation_removeFinish onRemoveAnimationFinish;

    [Inspect]
    public List<TowerSubModel> subModels;

    [Inspect]
    public bool buildAnimation;

    Animator animator;

    //This function triggered by Tower to lunch animation
    public void buildAnimationStart()
    {
        animator.SetTrigger("Build");
    }

    public void removeTowerAnimationLunch()
    {
        animator.SetTrigger("Destory");
    }

    public void removeAniationFinishHandler()
    {
        if (onRemoveAnimationFinish != null) onRemoveAnimationFinish();
    }

    //This event is triggered FROM animation
    public void buildCompleteAnimationEvent()
    {
        if (buildCompleteEvent != null)
        {
            buildCompleteEvent();
        }
    }

    public void Awake()
    {
        animator = GetComponent<Animator>();
    }

}

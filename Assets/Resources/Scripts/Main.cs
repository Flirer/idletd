﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Main script will initialize all systems
//Lol i forget about it and now it kinda...not doing any thing. Yet.
//But now aftyer looking at it, i think we should move here at some point and make all initialization happen here, in a very controlled fasion.

public class Main : MonoBehaviour {
    static Main that;

    void Start () {
        that = this;

        if (SaveLoad.firstLoadComplete)
        {
            initialize();
        }
        else
        {
            SaveLoad.OnPlayerLoaded += initialize;
        }
    }

    void initialize()
    {
        

        GetComponent<GameManager>().initialization();
        GetComponent<UserInterface>().initialization();

        
    }

}

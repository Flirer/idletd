﻿using System.Collections.Generic;

/// <summary>
/// This class may, or may not hold information, depending on event, that happend with tower. Each event will have description to explain what information it can supply.
/// </summary>
public class EventInfo {

    /// <summary>
    /// This tower host event
    /// </summary>
    public Tower tower = null;

    /// <summary>
    /// This tower triggered event
    /// </summary>
    public Tower triggerTower = null;

    /// <summary>
    /// In case many towers need to be supplied in event
    /// </summary>
    public List<Tower> triggerTowers = null;

    /// <summary>
    /// If creep is related to event
    /// </summary>
    public Creep creep = null;

    /// <summary>
    /// In case multiply creeps related to event
    /// </summary>
    public List<Creep> creeps = null;

    /// <summary>
    /// If weapon is related to event
    /// </summary>
    public Weapon weapon = null;

    /// <summary>
    /// If multiply weaons related to event
    /// </summary>
    public List<Weapon> weapons = null;

    /// <summary>
    /// If weapon is related to event
    /// </summary>
    public Projectile projectile = null;

    /// <summary>
    /// If multiply weaons related to event
    /// </summary>
    public List<Projectile> projectiles = null;

    /// <summary>
    /// Hex related to event
    /// </summary>
    public Hex hex = null;

    /// <summary>
    /// Related number
    /// </summary>
    public double amount;

    /// <summary>
    /// Related numbers iof more then one - carefull about this one
    /// </summary>
    public List<double> amounts;

    /// <summary>
    /// Related effect
    /// </summary>
    public Effect effect;

    /// <summary>
    /// Related effects
    /// </summary>
    public List<Effect> effects;

    /// <summary>
    /// Related ability
    /// </summary>
    public Ability ability;

    /// <summary>
    /// Related ability
    /// </summary>
    public List<Ability> abilities;

    /// <summary>
    /// Related ability
    /// </summary>
    public Stat stat;

    /// <summary>
    /// Related ability
    /// </summary>
    public List<Stat> stats;


}

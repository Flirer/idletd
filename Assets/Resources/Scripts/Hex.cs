﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[AdvancedInspector]
public class Hex : MapObject {

    [Inspect, Method(MethodDisplay.Invoke)]
    private void displayDestinations()
    {
        foreach (KeyValuePair< Hex, destinationNextStep> pair in destinations)
        {
            if (pair.Value.NextStep != null)
            GUILayout.Label("Destination to: " + 
                pair.Key.id + " -> " + 
                pair.Value.NextStep.id + " step: " + 
                pair.Value.Step);
        }
    }

    //public int id;
    public bool shift = false;                                          //Hex belong to shifted row
	public int x = 0;                                                   //Position in hexes coordinates
	public int y = 0;                                                   

    public Tower tower;                                                 //Tower on top of hex

    [Inspect]
    public int territoryId = -1;

    
    public Dictionary<string, Hex> 
        neighbors = new Dictionary<string, Hex> ();                     //Neighbors of hex, sorted on directions

	public List<Hex> neighborList = new List<Hex>();                    //Neighbors of hex, as simple list

    [Inspect]
	public Dictionary<Hex, destinationNextStep> 
        destinations = new Dictionary<Hex, destinationNextStep> ();     //List of all important destinations on map

	public Dictionary<Hex, destinationNextStep> 
        destinationsChecking = new Dictionary<Hex, destinationNextStep> (); //Temp list of all important destionation, for pah checking

	public bool walkable {                                              //If true - creeps will use this hex for path
        get { return _walkable; }
        set {
            //Debug.Log("Hex ID " + id + " walkable state has been chzanged to " + value);
            _walkable = value;
            //if (_walkable) debugTextMesh.color = Color.blue;
            //else debugTextMesh.color = Color.yellow;
        }
    }                                        
	public bool buildable = true;                                       //If true - player can build on this hex
    public bool unlocked = false;                                       //

    bool _walkable = true;

	public GameManager gameManager;                                     //Link to gameManager  

	public SpriteRenderer spriteRenderer;                               //                             
    [Inspect]
	public Sprite[] spriteVariation;                                    //Different version of sprite for hex

	public Sprite spriteEnterPortal;                                    //
	public Sprite spriteEndPortal;                                      //

	public List<Creep> creepsInHex;                                     //All creeps in current hex
	public List<Creep> creepsGoingToHex;                                //All creeps walking towards this hex

    public HexUtilControl utilControl;

    TextMesh debugTextMesh;

    bool beingClicked;                                                  //If true - onmouseup will work

	public delegate void OnCreepEnterHex(EventInfo info);
	public event OnCreepEnterHex onCreepEnteringHex;

    public delegate void OnCreepExitHex(EventInfo info);
    public event OnCreepExitHex onCreepExitingHex;

    public delegate void OnTowerBuild(EventInfo info);
    public event OnTowerBuild onTowerEnterHex;

    public delegate void OnClick(EventInfo info);
    public event OnClick onClick;

    public delegate void OnCursorEnter(EventInfo info);
    public event OnCursorEnter onCursorEnter;

    public delegate void OnCursorExit(EventInfo info);
    public event OnCursorExit onCursorExit;

    private void Update()
    {
        
    }

    protected override void onMouseEnter()
    {
        base.onMouseEnter();

        //Ignore mouse enter if button pressed (panning camera)
        if (Input.GetMouseButton(0)) return;


        if (Inventory.Territories.Contains(territoryId))    //Territory is opened
        {
            if (gameManager.selectedHex == this)
                markAsSelected();
             else if (GameManager.buildMode)
                spriteRenderer.color = StaticColors.colorBuildModeHover;
             else
                spriteRenderer.color = StaticColors.colorNormalHover;

            if (TerritoryManager.currentlyHightlightedTerritory != -1)
            {
                TerritoryManager.unlightTerritory(TerritoryManager.currentlyHightlightedTerritory);
            }
        } else                                              //Territory is not yet unlocked
        {
            TerritoryManager.highlightTerritory(territoryId);
        }
        if (onCursorEnter != null)
        {
            EventInfo info = new EventInfo();
            info.hex = this;
            onCursorEnter(info);
        }
    }
    private new void OnMouseOver() {
        base.OnMouseOver();
    }

    protected override void onMouseExit()
    {
        base.onMouseExit();

        if (gameManager.selectedHex == this)
            markAsSelected();
        else if(GameManager.buildMode)
            spriteRenderer.color = StaticColors.colorBuildMode;
        else
            spriteRenderer.color = StaticColors.colorNormal;

        if (onCursorExit != null)
        {
            EventInfo info = new EventInfo();
            info.hex = this;
            onCursorExit(info);
        }
    }

    protected override void clickHandler()
    {
        base.clickHandler();

        if (onClick != null)
        {
            EventInfo info = new EventInfo();
            info.hex = this;
            info.tower = tower;
            onClick(info);
        }
	}

    public void markAsSelected()
    {
        if (GameManager.buildMode)
        {
            spriteRenderer.color = StaticColors.colorSelected;
        }
        else
        {
            spriteRenderer.color = StaticColors.colorSelectedBuildMode;
        }
    }

    public void markAsDeselected()
    {
        if (GameManager.buildMode)
        {
            spriteRenderer.color = StaticColors.colorBuildMode;
        }
        else
        {
            spriteRenderer.color = StaticColors.colorNormal;
        }
    }

    public void highlightTerritory()
    {
        utilControl.highlightTerritory();
    }

    public void unlightTerritort()
    {
        utilControl.unlightTerritory();
    }

    public void territoryReqMet()
    {
        spriteRenderer.sprite = utilControl.TerrReqMet;
    }

    public void territoryReqUnmet()
    {
        spriteRenderer.sprite = utilControl.TerrReqUnmet;
    }

    public void lockTerritory()
    {
        buildable = false;
        walkable = false;
    }

    public void unlockTerritory()
    {
        utilControl.territoryUnlock();
        spriteRenderer.sprite = spriteVariation[0];
        buildable = true;
        walkable = true;
    }

    private void Awake()
    {
        spriteRenderer = transform.Find("Model").GetComponent<SpriteRenderer>();
        debugTextMesh = GetComponentInChildren<TextMesh>();
    }

    public void initialize() {
        //Adding all neighbors to Dictionary
        
        Hex tempHex;
		tempHex = (x - 1 >= 0) ? gameManager.hexesTable[y][x-1].GetComponent<Hex>() : null;
		neighbors.Add ("Left", tempHex);
		if (tempHex) neighborList.Add (tempHex);
		if (shift) {
			tempHex =(y + 1 < gameManager.hexRows) ? gameManager.hexesTable[y+1][x].GetComponent<Hex>() : null;
			neighbors.Add ("LeftTop", tempHex);
			if (tempHex) neighborList.Add (tempHex);
			tempHex = (y > 0) ? gameManager.hexesTable[y-1][x].GetComponent<Hex>() : null;
			neighbors.Add ("LeftBottom", tempHex);
			if (tempHex) neighborList.Add (tempHex);
		} else {
			tempHex =((y + 1 < gameManager.hexRows) && (x > 0)) ? gameManager.hexesTable[y+1][x-1].GetComponent<Hex>() : null;
			neighbors.Add ("LeftTop", tempHex);
			if (tempHex) neighborList.Add (tempHex);
			tempHex = ((y > 0) && (x > 0)) ? gameManager.hexesTable[y-1][x-1].GetComponent<Hex>() : null;
			neighbors.Add ("LeftBottom", tempHex);
			if (tempHex) neighborList.Add (tempHex);
		}

		tempHex = (x + 1 < gameManager.hexRows) ? gameManager.hexesTable[y][x+1].GetComponent<Hex>() : null;
		neighbors.Add ("Right", tempHex);
		if (tempHex) neighborList.Add (tempHex);
		if (shift) {
			tempHex =			((y + 1 < gameManager.hexRows) && (x + 1 < gameManager.hexColumns)) ? gameManager.hexesTable[y+1][x+1].GetComponent<Hex>() : null;
			neighbors.Add ("RightTop", tempHex);
			if (tempHex) neighborList.Add (tempHex);
			tempHex =		((y > 0) && (x + 1 < gameManager.hexColumns)) ? gameManager.hexesTable[y-1][x+1].GetComponent<Hex>() : null;
			neighbors.Add ("RightBottom",tempHex);
			if (tempHex) neighborList.Add (tempHex);
		} else {
			tempHex =		(y + 1 < gameManager.hexRows) ? gameManager.hexesTable[y+1][x].GetComponent<Hex>() : null;
			neighbors.Add ("RightTop",tempHex);
			if (tempHex) neighborList.Add (tempHex);
			tempHex = 	(y > 0) ? gameManager.hexesTable[y-1][x].GetComponent<Hex>() : null;
			neighbors.Add ("RightBottom",tempHex);
			if (tempHex) neighborList.Add (tempHex);
		}

        
        for (var i = neighborList.Count - 1; i > -1; i--)
        {
            //Debug.Log("Checking hex " + neighborList[i].id);
            if (neighborList[i].territoryId == -1)
            {
                //Debug.Log("Removing hex...");
                neighborList.RemoveAt(i);
            }
        }

        //spriteRenderer.sprite = spriteVariation[Random.Range(0, spriteVariation.Length)];

        if (GameManager.buildMode)
        {
            goBuildMode();
        } else
        {
            goGameMode();
        }

        GameManager.onEnteringBuildMode += goBuildMode;
        GameManager.onEnteringGameMode += goGameMode;
    }

	public void updateDebugInfo() {
        //Showing coords. For testing 
        if (debugTextMesh)
        {
            debugTextMesh.text = "ID: " + id;
            if (shift)
            {
                debugTextMesh.color = Color.green;
            }
            //debugTextMesh.text += "\n" + destinationsNext [gameManager.currentDestinationCheck].Step.ToString ();
        }
    }

	public void clearDebugInfo() {
        //Showing coords. For testing 
        if (debugTextMesh)
        {
            debugTextMesh.text = "ID: " + id + "\n step: " + destinations[gameManager.endingHex].Step;
            /*if (shift)
            {
                debugTextMesh.color = Color.green;
            }*/
        }
	}

	public void beingBuildUpon(Tower tow) {
        tower = tow;
        walkable = false;
        buildable = false;
        if (onTowerEnterHex != null)
        {
            EventInfo info = new EventInfo();
            info.hex = this;
            info.tower = tow;
            onTowerEnterHex(info);
        }
        foreach (Creep creep in creepsGoingToHex) {
			creep.goBackToCenter ();
		}
	}

    public void removeTower()
    {
        tower = null;
        buildable = true;
        walkable = true;
    }

    public void creepEnteredHex(EventInfo info) {
		creepsInHex.Add (info.creep);
		if (creepsGoingToHex.Contains(info.creep)) {
			creepsGoingToHex.Remove(info.creep);
		}
        info.creep.onExitingHex += creepLeaveHex;
        info.creep.onDying += creepDiedInHex;
        info.creep.onCreepDestroy += creepLeaveHex;

        if (!gameManager.hexesWithCreeps.Contains (this)) {
			gameManager.hexesWithCreeps.Add (this);
		}

		if (onCreepEnteringHex != null) {
			onCreepEnteringHex (info);
		}
	}

	public void creepLeaveHex(EventInfo info) {
		creepsInHex.Remove (info.creep);
        if (info.creep)
        {
            info.creep.onExitingHex -= creepLeaveHex;
            info.creep.onDying -= creepLeaveHex;
            info.creep.onCreepDestroy -= creepLeaveHex;
        }
        

        if (creepsInHex.Count == 0) {
			gameManager.hexesWithCreeps.Remove (this);
		}

		if (onCreepExitingHex != null) {
			onCreepExitingHex (info);
		}
	}

    public void creepDiedInHex(EventInfo info)
    {
        /*Debug.Log("Creep ID " + 
            info.creep.id + " died in Hex " + 
            id + " Starting clearing process");*/
        creepsInHex.Remove(info.creep);
        if (info.creep)
        {
            info.creep.onExitingHex -= creepLeaveHex;
            info.creep.onDying -= creepLeaveHex;
        }

        if (creepsInHex.Count == 0)
        {
            gameManager.hexesWithCreeps.Remove(this);
        }
    }

    public void removeNeighbor(Hex hex)
    {
        if (neighborList.Contains(hex))
        {
            string target = "";
            neighborList.Remove(hex);
            foreach (KeyValuePair<string, Hex> pair in neighbors)
            {
                if (pair.Value == hex)
                    target = pair.Key;
            }
            neighbors[target] = null;
        }
    }

    private void OnDestroy()
    {
        foreach (Hex hex in neighborList)
        {
            hex.removeNeighbor(this);
        }
        onCreepEnteringHex = null;
        onCreepExitingHex = null;
        onTowerEnterHex = null;
        onClick = null;
        onCursorEnter = null;
        onCursorExit = null;
    }


    public List<Hex> getNeighborsInRange(int range = 1)
    {
        //Debug.Log("INI from- " + id);
        List<Hex> result = new List<Hex>();
        List<Hex> tempResult = new List<Hex>(neighborList);
        if (range <= 0)
        {
            return null;
        }

        if (range == 1) {
            result = tempResult;
            return result;
        } else
        {
            
            int i = range-1;
            List<Hex> tempTempResult = new List<Hex>();
            result.AddRange(tempResult);
            foreach (Hex hex in tempResult)
            {
                //Debug.Log("Call from - " + id + " Add range from " + hex.id);
                tempTempResult.AddRange(hex.getNeighborsInRange(i));
                //Debug.Log("Call from - " + id + "New Count for temptemp " + tempTempResult.Count);
            }
            result.AddRange(tempTempResult);
        }

        tempResult.Clear();

        foreach (Hex hex in result)
        {
            if (!tempResult.Contains(hex))
            {
                tempResult.Add(hex);
            }
        }

        return tempResult;
    }

    public List<Tower> getTowersInRange(int range)
    {
        List<Hex> hexes = getNeighborsInRange(range);
        List<Tower> result = new List<Tower>();

        foreach (Hex hex in hexes)
        {
            if (hex.tower != null)
            {
                result.Add(hex.tower);
            }
        }

        return result;
    }

    public List<Creep> getCreepsInRange(int range)
    {
        List<Hex> hexes = getNeighborsInRange(range);
        List<Creep> result = new List<Creep>();

        foreach (Hex hex in hexes)
        {
            if (hex.creepsInHex.Count > 0)
            {
                result.AddRange(hex.creepsInHex);
            }
        }

        return result;
    }


    public static List<Creep> getCreepsInHexes(List<Hex> hexes, Hex dest)
    {
        List<Creep> result = new List<Creep>();

        foreach (Hex hex in hexes)
        {
            result.AddRange(hex.creepsInHex);
        }

        //result.Sort(Creep.CompareCreepsByDistanceToDestination);
        //result = result.OrderBy(creep => creep.distanceToEnd).ToList<Creep>();


        return result;
    }


    /// <summary>
    /// Take list of hexes and destination, find closest hex Range in this list. Return -1 if list was empty or destination was unreachable from hexes;
    /// </summary> 
    public static int getClosestRangeToDestination(List<Hex> hexes, Hex dest)
    {
        if (hexes.Count > 0)
        {
            bool gotValue = false;
            int result = new int();
            foreach (Hex hex in hexes)
            {
                if (hex.destinations.ContainsKey(dest))
                {
                    if (hex.destinations[dest].Step < result)
                    {
                        result = hex.destinations[dest].Step;
                        gotValue = true;
                    }
                }
            }
            if (gotValue)
            {
                return result;
            }
        }
        return -1;
    }

    /// <summary>
    /// Take list of hexes and destination, find farthest hex Range in this list. Return -1 if list was empty or destination was unreachable from hexes;
    /// </summary> 
    public static int getFarthestRangeToDestination(List<Hex> hexes, Hex dest)
    {
        if (hexes.Count > 0)
        {
            bool gotValue = false;
            int result = new int();
            foreach (Hex hex in hexes)
            {
                if (hex.destinations.ContainsKey(dest))
                {
                    if (hex.destinations[dest].Step > result)
                    {
                        result = hex.destinations[dest].Step;
                        gotValue = true;
                    }
                }
            }
            if (gotValue)
            {
                return result;
            }
        }
        return -1;
    }

    public static List<Hex> getHexesInRangeToDestination(List<Hex> hexes, Hex dest, int range)
    {
        List<Hex> result = new List<Hex>();

        foreach (Hex hex in hexes)
        {
            if (hex.destinations.ContainsKey(dest))
            {
                if (hex.destinations[dest].Step == range)
                {
                    result.Add(hex);
                }
            }
        }

        return result;
    }

    //RANGECHECK

    public void cleareRangeCheck()
    {
        utilControl.clear();
    }

    public void showRangeCheckMarker()
    {
        utilControl.setRangeCheck();
    }

    void goBuildMode()
    {
        spriteRenderer.color = new Color(0.6f, 1, 1, 1.8f);
    }

    void goGameMode()
    {
        spriteRenderer.color = Color.white;
    }
}

public struct destinationNextStep {

	public Hex Destination;
	public int Step;
	public Hex NextStep;

	public destinationNextStep (Hex destination, int step, Hex nextStep) {
		Destination = destination;
		Step = step;
		NextStep = nextStep;
	}

	public destinationNextStep (destinationNextStep step) {
		Destination = step.Destination;
		Step = step.Step;
		NextStep = step.NextStep;
	}
}
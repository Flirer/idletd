﻿using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Stat {

    public delegate void onChange(EventInfo info);
    public event onChange OnChange;

    

    public enum TowerStat
    {
        damage,
        attackSpeed,
        reloadSpeed,
        ammoCount,
        critChance,
        critDamage
    }

    public static Stat getTowerStatByType(Tower tower, TowerStat statType)
    {
        Stat stat;
        switch (statType)
        {
            case Stat.TowerStat.ammoCount:
                stat = tower.maxAmmo;
                break;

            case Stat.TowerStat.attackSpeed:
                stat = tower.attackSpeed;
                break;

            case Stat.TowerStat.critChance:
                stat = tower.critChance;
                break;

            case Stat.TowerStat.critDamage:
                stat = tower.critDamage;
                break;

            case Stat.TowerStat.damage:
                stat = tower.damage;
                break;

            case Stat.TowerStat.reloadSpeed:
                stat = tower.reloadSpeed;
                break;

            default:
                Debug.Log("No stat to return");
                stat = null;
                break;
        }

        return stat;
    }

    public enum CreepStat
    {
        slowFactor,
        expBounty,
        goldBounty
    }

    public static Stat getCreepStatByType(Creep creep, CreepStat statType)
    {
        Stat stat;
        switch (statType)
        {
            case CreepStat.slowFactor:
                stat = creep.slowFactor;
                break;

            case CreepStat.expBounty:
                stat = creep.grantExp;
                break;

            case CreepStat.goldBounty:
                stat = creep.grantGold;
                break;

            default:
                stat = null;
                break;
        }

        return stat;
    }

    public enum bonusType
    {
        plain,
        percent,
        multiplier
    }

    [Inspect]
    double baseValue;

    [Inspect]
    double bonusValue = 0;

    [Inspect]
    double bonusPlainValue = 0;

    [Inspect]
    [Constructor("createMultiplier")]
    public List<StatMultiplier> multipliers;

    public StatMultiplier createMultiplier()
    {
        return new StatMultiplier(0);
    }

    [NonSerialized]
    [Inspect]
    List<Stat> proxyStats;

    public enum StatBonusType
    {
        plain,
        percent,
        multiplier
    }

    public Stat(double val)
    {
        baseValue = val;
        multipliers = new List<StatMultiplier>();
        proxyStats = new List<Stat>();
    }

    public double Value
    {
        get {
            double calcMultiplier = 1;

            foreach (StatMultiplier multiplier in multipliers)
            {
                calcMultiplier = calcMultiplier * multiplier.amount;
            }

            calcMultiplier *= getProxyMultiplier();

            double result = (baseValue + bonusPlainValue + getProxyPlainBonus()) * (1 + bonusValue + getProxyBonus());

            return (result * calcMultiplier);
        }
    }

    public double getProxyPlainBonus()
    {
        double result = 0;
        foreach (Stat s in proxyStats)
        {
            result += s.bonusPlainValue;
            result += s.getProxyPlainBonus();
        }
        return result;
    }

    public double getProxyBonus()
    {
        double result = 0;
        foreach (Stat s in proxyStats)
        {
            result += s.bonusValue;
            result += s.getProxyBonus();
        }
        return result;
    }

    public double getProxyMultiplier()
    {
        //Debug.Log("Lets gather some proxy multipliers, proxys: " + proxyStats.Count);
        double result = 1;
        if (proxyStats.Count > 0)
        {
            foreach (Stat s in proxyStats)
            {
                //Debug.Log(s.getStatMultipliers().Count);
                if (s.getStatMultipliers().Count > 0)
                {
                    foreach (StatMultiplier multi in s.getStatMultipliers())
                    {
                        result *= multi.amount;
                    }
                }

                result *= s.getProxyMultiplier();
            }
        }
        
        return result;
    }

    public void addBonus(double amount)
    {
        
        bonusValue += amount;
        hitOnChange();
    }

    public void removeBonus(double amount)
    {
        
        bonusValue -= amount;
        hitOnChange();
    }

    public double getStatBonus()
    {
        return bonusValue;
    }

    public void addPlainBonus(double amount)
    {
        
        bonusPlainValue += amount;
        hitOnChange();
    }

    public void removePlainBonus(double amount)
    {
        
        bonusPlainValue -= amount;
        hitOnChange();
    }

    public double getStatPlainBonus()
    {
        return bonusPlainValue;
    }

    public void addMultiplier(StatMultiplier multiplier)
    {
        
        multipliers.Add(multiplier);
        hitOnChange();
    }

    public void removeMultiplier(StatMultiplier multiplier)
    {
        
        multipliers.Remove(multiplier);
        hitOnChange();
    }

    public List<StatMultiplier> getStatMultipliers()
    {
        if (multipliers != null)
        {
            return multipliers;
        } else
        {
            return new List<StatMultiplier>();
        }
        
    }

    public double getBaseValue()
    {
        return baseValue;
    }

    public void updateBaseValue(double val)
    {
        
        baseValue = val;
        hitOnChange();
    }

    void hitOnChange()
    {
        if (OnChange != null)
        {
            EventInfo info = new EventInfo();
            info.stat = this;
            OnChange(info);
        }
    }

    public void addProxyStat(Stat stat_)
    {
        proxyStats.Add(stat_);
        hitOnChange();
    }

    public void removeProxyStat(Stat stat_)
    {
        proxyStats.Remove(stat_);
        hitOnChange();
    }

    public string toString()
    {
        string result = "";

        if (bonusValue != 0)
        {
            result += Math.Round(bonusValue, 1) + "%; ";
        }

        if (bonusPlainValue != 0)
        {
            result += Math.Round(bonusPlainValue, 1) + " to base Value; ";
        }

        if (getStatMultipliers().Count > 0)
        {
            double multi = 0;
            foreach (StatMultiplier m in getStatMultipliers())
            {
                multi += m.amount;
            }
            result += " Multiply by " + Math.Round(multi, 3) + " ; ";
        }

        if (result == "")
        {
            result = "Empty stat";
        }

        return result;
    }

    public void clearIncomingProxyStats()
    {
        proxyStats = new List<Stat>();
    }

    public static Stat operator +(Stat a, Stat b)
    {
        Stat stat = new Stat(0);
        stat.baseValue = a.baseValue + b.baseValue;
        stat.bonusValue = a.bonusValue + b.bonusValue;
        stat.bonusPlainValue = a.bonusPlainValue + b.bonusPlainValue;
        stat.multipliers.AddRange(a.getStatMultipliers());
        stat.multipliers.AddRange(b.getStatMultipliers());

        stat.multipliers = a.multipliers;

        return stat;
    }

    public void consume(Stat s)
    {
        baseValue += s.baseValue;
        bonusValue += s.bonusValue;
        bonusPlainValue += s.bonusPlainValue;
        multipliers.AddRange(s.getStatMultipliers());
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour {

    public Hex startingHex;
    public Hex endingHex;
	public GameManager gameManager;

    public int id;

    public int essenceReward = 1;
    public int essenceRewardFrequncy = 2;
    int essenceRewardCounter = 0;

    public int crystalReward = 1;
    public int crystalRewardFrequncy = 5;
    int crystalRewardCounter = 0;

    public int talantPointReward = 1;
    public int talantPointRewardFrequncy = 3;
    int talantPointRewardCounter = 0;

    public GameObject creepPrefab;
    public GameObject creepSwarmPrefab;
    public GameObject creepChampPrefab;

    public double expiriencePerWaveBase = 20;

    int creepId = 0;

	public static int currentWave = 1;

    int chunksPerWave = 3;
    int normalCreepsInChunk = 3;
    int swarmCreepsInChunk = 7;
    int championCreepsInChunk = 1;

    public bool creepReachExit = false;
    public bool waveWasBeaten = false;

    public double hpForWave;
    double expForWave;

    public List<Hex> path;

    WaveHolder waveholder;

    double percentOfgrowth = 1.1;
    double baseHp = 10;

	public void ini(Hex start, Hex end, int wave = 1) {
		startingHex = start;
		endingHex = end;
        currentWave = wave;
        path = new List<Hex>();

        foreach (int h in gameManager.waypoints)
        {
            path.Add(gameManager.hexes[h]);
            gameManager.hexes[h].spriteRenderer.sprite = gameManager.hexes[h].spriteEnterPortal;
            //gameManager.hexes[h].sprite.color = Color.yellow;
        }

        path.Add(gameManager.endingHex);

        GameManager.onEnteringGameMode += initiateSpawnWave;
        GameManager.onEnteringBuildMode += clearCurrentWave;
	}

	public Creep spawnCreep(GameObject creepPrefab) {
		GameObject creep = Instantiate (
			creepPrefab, 
			startingHex.transform.position, 
			Quaternion.identity
		) as GameObject;

        creep.name = "Creep_" + creepId;
        Creep creepComp = creep.GetComponent<Creep>();

        creepComp.ini(startingHex, path, hpForWave, expForWave);
        creepComp.gameManager = gameManager;
        creepComp.id = creepId;
        creepId++;

        creepComp.onClick += gameManager.selectCreep;
        creepComp.onCreepDestroy += gameManager.deselectDestroyedCreep;
        //gameManager.creepSpawn(creepComp);
        return creepComp;
    }

	public void initiateSpawnWave() {
        if (!creepReachExit && waveWasBeaten)
        {
            currentWave++;
            waveWasBeaten = false;
        }
        else
        {
            creepReachExit = false;
        }

        hpForWave = (baseHp * currentWave) * (Math.Pow(percentOfgrowth, currentWave));
        expForWave = expiriencePerWaveBase * currentWave / chunksPerWave * (1 + Talants.expirienceBonus.level*0.2);

        gameManager.userInterface.updateWaveInfo();
        
        Debug.Log("Spawn wave " + currentWave);
        StartCoroutine ("spawnWave");
    }


    public IEnumerator spawnWave () {
        yield return new WaitForSeconds(2);
        int i = 0;
        List<int> rolls = new List<int>();
        int sum = 0;
        for (int j = 0; j < chunksPerWave; j++)
        {
            rolls.Add(UnityEngine.Random.Range(0, 3));
            switch (rolls[rolls.Count-1])
            {
                case 0:
                    sum += 3;
                break;

                case 1:
                    sum += 7;
                break;
                    
                case 2:
                    sum += 1;
                break;
            }
        }
        waveholder = new WaveHolder(currentWave, new List<Creep>(), this, sum);

        while (i < chunksPerWave) {
            //int k = UnityEngine.Random.Range(0, 3);
            //Debug.Log(rolls[i]);
            switch(rolls[i])
            {
                case 0:
                    int j = 0;
                    while (j < 3)
                    {
                        yield return new WaitForSeconds(0.5f);
                        spawnNormalChunk();
                        j++;
                    }
                    yield return new WaitForSeconds(0.2f);
                    break;

                case 1:
                    int k = 0;
                    while (k < 7)
                    {
                        yield return new WaitForSeconds(0.3f);
                        spawnSwarmChunk();
                        k++;
                    }
                    yield return new WaitForSeconds(0.1f);
                    break;

                case 2:
                    int l = 0;
                    while (l < 1)
                    {
                        yield return new WaitForSeconds(0.8f);
                        spawnChampionChunk();
                        l++;
                    }
                    yield return new WaitForSeconds(0.3f);
                    break;

            }
            i++;
            /*Creep crp = spawnCreep(creepPrefab);
            waveholder.creeps.Add(crp);
            crp.OnDying += waveholder.killCreep;
			*/
            //yield return new WaitForSeconds (0.9f);
        }
    }

    public void spawnNormalChunk()
    {
        
            Creep crp = spawnCreep(creepPrefab);
            waveholder.creeps.Add(crp);
            crp.onDying += waveholder.killCreep;
            
    }

    public void spawnSwarmChunk()
    {
            Creep crp = spawnCreep(creepSwarmPrefab);
            waveholder.creeps.Add(crp);
            crp.onDying += waveholder.killCreep;
    }

    public void spawnChampionChunk()
    {
        Creep crp = spawnCreep(creepChampPrefab);
        waveholder.creeps.Add(crp);
        crp.onDying += waveholder.killCreep;
    }


    public void clearCurrentWave()
    {
        Debug.Log("Clearing waves");
        foreach (Creep crp in waveholder.creeps)
        {
            crp.terminate();
        }
        waveholder.terminate(waveholder);
        StopAllCoroutines();
    }

    public void giveRewardForWave(int level)
    {
        /*
        if (level % crystalRewardFrequncy == 0)
        {
            gameManager.giveCrystal(crystalReward);
        }

        if (level % essenceRewardFrequncy == 0)
        {
            gameManager.giveEssence(essenceReward);
        }

        if (level % talantPointRewardFrequncy == 0)
        {
            Talants.giveTalantPoint();
        }*/
    }
}

public class WaveHolder
{
    public int level;
    public List<Creep> creeps;
    public SpawnManager spawnManager;
    public int numberOfCreeps;

    public WaveHolder (int level, List<Creep> creeps, SpawnManager spawnManager, int numberOfCreeps)
    {
        this.level = level;
        this.creeps = creeps;
        this.spawnManager = spawnManager;
        this.numberOfCreeps = numberOfCreeps;
    }

    public void killCreep(EventInfo info)
    {
        
        creeps.Remove(info.creep);
        numberOfCreeps--;
        if (numberOfCreeps == 0)
        {
            //spawnManager.giveRewardForWave(level);
            Debug.Log("Wave " + level + " beaten");
            SaveLoad.waveBeaten(level);
            spawnManager.waveWasBeaten = true;
            spawnManager.initiateSpawnWave();
            terminate(this);
        }
    }

    public void terminate(WaveHolder wh)
    {
        wh = null;
    }
}

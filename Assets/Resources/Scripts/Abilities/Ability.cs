﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : MonoBehaviour {

    public enum AbilityActivationType
    {
        alwaysOn, onAttack, onCrit, onHit, onReload, onLevelUp, onUpgraid, creepDie
    }

    //L1
    public enum AbilityTargetType
    {
        tower, creep
    }

    //L1
    public enum AbilityNumberOfTargets
    {
        single, multiple
    }
    //L2
    public enum AbilitySingleTargets
    {
        self, weaponTarget, spellTarget, attackingTower, castingTower, closest, farthest
    }
    //L2
    public enum AbilityMultipleTargetsNumber
    {
        number, randomRange
    }
    //L2
    public enum AbilityMultipleTargetsArea
    {
        aroundHexInRange, allOnMap, 
    }
    //L3 - what kind of hex are we talking about from AbilityMultipleTargetsArea.aroundHexInRange
    public enum AbilityMultipleTargetsAreaType
    {
        ownHex, attackingTowerHex, attackedCreepHex, spellTargetHex
    }

    //Old shiiiet
    public enum AbilityTargetingType
    {
        self, weaponTarget, spellTarget, inArea
    }

    public enum AbilityAreaType
    {
        aroundSelf, aroundWeaponTarget, aroundSpellTarget
    }

    public enum AbilityTargetInArea
    {
        all, pickN, pickRandomN
    }


    public int id;
    public string abilName = "Undefined ability";
    public string description = "no description.";
    public Sprite icon;

    public int effectId;
    public string effectName;
    public string effectDescription;
    public Sprite effectIcon;

    int kills = 0;
    double damageDone = 0;

    public Tower tower;
    protected Creep creep;
    protected List<Creep> creeps;

    public delegate void OnAbilityRemove();
    public event OnAbilityRemove onAbilityRemove;

    public abstract void initialize(Tower t);
    public virtual string generateToolTip()
    {
        return "Undescribed Ability";
    }

    

    //STATISTICS GOES HERE

    public void incKillCounter()
    {
        kills++;
    }

    public void incDamageCounter(double amount)
    {
        damageDone += amount;
    }

    public void removeAbility()
    {
        if (onAbilityRemove != null) onAbilityRemove();
    }
}

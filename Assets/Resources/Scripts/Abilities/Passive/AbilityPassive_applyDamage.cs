﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class AbilityPassive_applyDamage : AbilityPassive
{
    [Inspect]
    public ef_damage.DamageEffectType damageType;

    [Inspect]
    public double damageAmount;

    [Inspect]
    public double originalDamageAmount = 0;

    [Inspect]
    public double originalDamageAmountLevelBonus = 0;

    [Inspect("displayTime")]
    public double timeBetweenTicks;

    [Inspect("displayChargeChargeAmount")]
    public int numberOfTicks;

    [Inspect("displayChargeType")]
    public Effect.effectChargesType chargeType;

    


    bool displayTime
    {
        get
        {
            if (damageType == ef_damage.DamageEffectType.end ||
                damageType == ef_damage.DamageEffectType.overTime ||
                damageType == ef_damage.DamageEffectType.overTimeCharges) return true;
            else return false;
        }
    }

    bool displayChargeType
    {
        get
        {
            if (damageType == ef_damage.DamageEffectType.onCharges ||
                damageType == ef_damage.DamageEffectType.overTimeCharges) return true;
            else return false;
        }
    }

    bool displayChargeChargeAmount
    {
        get
        {
            if (damageType == ef_damage.DamageEffectType.onCharges ||
                damageType == ef_damage.DamageEffectType.overTime) return true;
            else return false;
        }
    }


    public override void initialize(Tower tow)
    {
        tower = tow;
        switch (damageType)
        {
            case ef_damage.DamageEffectType.start:
                instantInitialize();
                break;

            case ef_damage.DamageEffectType.end:
                onEndInitialize();
                break;

            case ef_damage.DamageEffectType.overTime:
                dotInitialize();
                break;

            case ef_damage.DamageEffectType.onCharges:
                chargeInitialize();
                break;

            case ef_damage.DamageEffectType.overTimeCharges:
                timedChargeInitialize();
                break;
        }
    }

    void instantInitialize()
    {
        tower.onHit += castInstant;
    }

    void onEndInitialize()
    {
        tower.onHit += castOnEnd;
    }

    void dotInitialize()
    {
        tower.onHit += castDot;
    }

    void chargeInitialize()
    {
        tower.onHit += castCharges;
    } 

    void timedChargeInitialize()
    {

    }

    void castInstant(EventInfo info)
    {
        ef_damage effect = new ef_damage(info.creep, this);
        effect.setEffectType(sumUpDamage(info));
        info.creep.applyEffect(effect);
        effect.initialize();
        effect.OnEnd += clear;
    }

    void castOnEnd(EventInfo info)
    {
        ef_damage effect = new ef_damage(info.creep, this);
        effect.setEffectType(timeBetweenTicks, sumUpDamage(info));
        info.creep.applyEffect(effect);
        effect.initialize();
        effect.OnEnd += clear;
    }

    void castDot(EventInfo info)
    {
        ef_damage effect = new ef_damage(info.creep, this);
        effect.setEffectType(timeBetweenTicks, sumUpDamage(info), numberOfTicks);
        info.creep.applyEffect(effect);
        effect.initialize();
        effect.OnEnd += clear;
    }

    void castCharges(EventInfo info)
    {
        ef_damage effect = new ef_damage(info.creep, this);
        effect.tower = tower;
        effect.setEffectType(sumUpDamage(info), numberOfTicks, chargeType);
        info.creep.applyEffect(effect);
        effect.initialize();
        effect.OnEnd += clear;
    }

    double sumUpDamage(EventInfo info)
    {
        if (originalDamageAmount > 0)
        {
            if (originalDamageAmountLevelBonus != 0)
            {
                return damageAmount + (info.amount * (originalDamageAmount + (originalDamageAmountLevelBonus * info.tower.level.data)));
            }
            else
            {
                return damageAmount + (info.amount * (originalDamageAmount));
            }
        } else
        {
            return damageAmount;
        }
        
        
    }

    void clear(Effect eff)
    {
        eff.OnEnd -= clear;

        eff = null;
    }
}

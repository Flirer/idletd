﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInspector;

[AdvancedInspector]
public class AbilityPassive_auraForTowers : AbilityPassive
{

    [Inspect(0)]
    public string Name;

    [Inspect(1)]
    public string Description;

    [Inspect(2)]
    public string NameEffect;

    [Inspect(3)]
    public string DescriptionEffect;

    [Inspect(4)]
    public int _effectId;

    [Inspect(5)]
    public List<EffectStat> effectStats;

    [Inspect(6)]
    public int range = 1;

    List<Hex> hexes;

    public override void initialize(Tower tow)
    {
        abilName = Name;
        description = Description;
        effectName = NameEffect;
        effectDescription = DescriptionEffect;
        effectId = _effectId;

        tower = tow;

        tower.onMoveBegin += freeHexes;
        tower.onBeingRemoved += freeHexes;
        tower.onMoveEnd += getHexes;

        getHexes();
    }

    void getHexes(EventInfo info = null)
    {
        hexes = tower.locationHex.getNeighborsInRange(range);
        foreach (Hex hex in hexes)
        {
            initializeHex(hex);
        }
    }

    void freeHexes(EventInfo info)
    {
        foreach (Hex hex in hexes)
        {
            deinitializeHex(hex);
        }
        hexes = null;
        removeAbility();
    }

    void initializeHex(Hex hex)
    {
        hex.onTowerEnterHex += cast;
        if (hex.tower)
        {
            cast(hex.tower);
        }
    }

    void deinitializeHex(Hex hex)
    {
        hex.onTowerEnterHex -= cast;
    }

    void cast(EventInfo info)
    {
        cast(info.tower);
    }

    void cast(Tower tower)
    {

        foreach (EffectStat stat in effectStats)
        {
            //stat.statToChange = Stat.getCreepStatByType(_creep, stat.creepStatType);
            ef_statChange eff = new ef_statChange();
            eff.setTarget(tower);
            eff.setEffectType(effectStats);
            eff.setEndingType(this);

            eff.effectId = effectId;
            eff.Name = effectName;
            eff.Description = effectDescription;
            eff.icon = effectIcon;

            tower.applyEffect(eff);

            eff.initialize();
            eff.OnEnd += clear;
        }
    }

    void clear(Effect eff)
    {
        eff = null;
    }
}

﻿using AdvancedInspector;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class AbilityPassive_affectTowerStat: AbilityPassive {

    [SerializeField]
    [Inspect]
    public List<EffectStat> effectStats;

    [Inspect]
    AbilityActivationType activationType;

    //ENDING TYPES

    [Inspect] Effect.effectTeminationType endingType;
    //TIME
    [Inspect("timeEnding")]
    public float time;

    //CHARGES
    //[Inspect("chargeEnding")]
    public Effect.effectChargesType chargesType;
    [Inspect("chargeEnding")]
    public Effect.effectChargesTypeTower chargesTypeTower;
    [Inspect("chargeEnding")]
    public int chargesCount;

    bool timeEnding
    {
        get
        {
            if (endingType == Effect.effectTeminationType.time) return true;
            else return false;
        }
    }

    bool chargeEnding
    {
        get
        {
            if (endingType == Effect.effectTeminationType.charges) return true;
            else return false;
        }
    }

    public override void initialize(Tower tow)
    {
        tower = tow;

        switch (activationType)
        {
            case AbilityActivationType.creepDie:
                //TODO: Implent
                break;

            case AbilityActivationType.onAttack:
                tower.onAttack += cast;
                break;

            case AbilityActivationType.onCrit:
                tower.onCritHit += cast;
                break;

            case AbilityActivationType.onHit:
                tower.onHit += cast;
                break;

            case AbilityActivationType.onLevelUp:
                tower.onLevelUp += cast;
                break;

            case AbilityActivationType.onReload:
                tower.onReload += cast;
                break;

            case AbilityActivationType.onUpgraid:
                //TODO: Implent
                break;
        }

        foreach (EffectStat stat in this.effectStats)
        {
            stat.statToChange = Stat.getTowerStatByType(tower, stat.towerStatType);
        }
                
    }


    void cast(EventInfo info)
    {
        ef_statChange eff = new ef_statChange();
        eff.setEffectType(effectStats);
        eff.setTarget(tower);

        switch (endingType) {
            case Effect.effectTeminationType.time:
            eff.setEndingType(time);
                break;

            case Effect.effectTeminationType.charges:
                switch (chargesTypeTower)
                {
                    case Effect.effectChargesTypeTower.afterNAttacks:
                        chargesType = Effect.effectChargesType.afterNAttacks;
                        break;

                    case Effect.effectChargesTypeTower.afterNHit:
                        chargesType = Effect.effectChargesType.afterNHit;
                        break;

                    case Effect.effectChargesTypeTower.afterNCast:
                        chargesType = Effect.effectChargesType.afterNCast;
                        break;

                    case Effect.effectChargesTypeTower.afterNCreepsDie:
                        chargesType = Effect.effectChargesType.afterNCreepsDie;
                        break;

                    case Effect.effectChargesTypeTower.afterNCritAttack:
                        chargesType = Effect.effectChargesType.afterNCritAttack;
                        break;

                    case Effect.effectChargesTypeTower.afterNLevelUp:
                        chargesType = Effect.effectChargesType.afterNLevelUp;
                        break;

                    case Effect.effectChargesTypeTower.afterNReload:
                        chargesType = Effect.effectChargesType.afterNReload;
                        break;

                    case Effect.effectChargesTypeTower.afterNUpgraids:
                        chargesType = Effect.effectChargesType.afterNUpgraids;
                        break;
                }
            eff.setEndingType(chargesType, chargesCount);
                break;

        } 

        tower.applyEffect(eff);

        eff.effectId = effectId;
        eff.Name = effectName;
        eff.Description = effectDescription;
        eff.icon = effectIcon;

        eff.initialize();
        eff.OnEnd += clear;
    }

    void clear(Effect eff)
    {
        eff = null;
    }

    public override string generateToolTip()
    {
        return "<b>" + abilName + "</b>\n" + description;
    }

    public void deinitialize()
    {
        switch (activationType)
        {
            case AbilityActivationType.creepDie:
                //TODO: Implent
                break;

            case AbilityActivationType.onAttack:
                tower.onAttack -= cast;
                break;

            case AbilityActivationType.onCrit:
                tower.onCritHit -= cast;
                break;

            case AbilityActivationType.onHit:
                tower.onHit -= cast;
                break;

            case AbilityActivationType.onLevelUp:
                tower.onLevelUp -= cast;
                break;

            case AbilityActivationType.onReload:
                tower.onReload -= cast;
                break;

            case AbilityActivationType.onUpgraid:
                //TODO: Implent
                break;
        }
    }
}

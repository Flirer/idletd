﻿using AdvancedInspector;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class AbilityPassive_affectCreepStat : AbilityPassive {

    [Inspect(0)]
    public string Name;

    [Inspect(1)]
    public string Description;

    [Inspect(2)]
    public string NameEffect;

    [Inspect(3)]
    public string DescriptionEffect;

    [Inspect(4)]
    public int _effectId;

    [Inspect]
    public List<EffectStat> effectStats;

    [Inspect]
    public AbilityActivationType activationType;

    //ENDING TYPES
    [Inspect] Effect.effectTeminationType endingType;
    //TIME
    [Inspect("timeEnding")]
    public int time;

    bool timeEnding
    {
        get
        {
            if (endingType == Effect.effectTeminationType.time) return true;
            else return false;
        }
    }

    bool chargeEnding
    {
        get
        {
            if (endingType == Effect.effectTeminationType.charges) return true;
            else return false;
        }
    }

    //CHARGES
    //[Inspect("chargeEnding")]
    public Effect.effectChargesType chargesType;
    [Inspect("chargeEnding")]
    public Effect.effectChargesTypeCreep chargesTypeCreep;
    [Inspect("chargeEnding")]
    public int chargesCount;

    public override void initialize(Tower tow)
    {
        abilName = Name;
        description = Description;
        effectName = NameEffect;
        effectDescription = DescriptionEffect;
        effectId = _effectId;

        tower = tow;

        switch (activationType)
        {
            case AbilityActivationType.creepDie:
                //TODO: Implent
                break;

            case AbilityActivationType.onAttack:
                tower.onAttack += cast;
                break;

            case AbilityActivationType.onCrit:
                tower.onCritHit += cast;
                break;

            case AbilityActivationType.onHit:
                tower.onHit += cast;
                break;

            case AbilityActivationType.onLevelUp:
                tower.onLevelUp += cast;
                break;

            case AbilityActivationType.onReload:
                tower.onReload += cast;
                break;

            case AbilityActivationType.onUpgraid:
                //TODO: Implent
                break;
        }
    }


    void cast(EventInfo info)
    {
        creeps = info.creeps;
        Debug.Log("Casting ability... " + creeps.Count);
        foreach (Creep _creep in creeps)
        {
            foreach (EffectStat stat in effectStats)
            {
                //stat.statToChange = Stat.getCreepStatByType(_creep, stat.creepStatType);
                ef_statChange eff = new ef_statChange();
                eff.setTarget(_creep);
                eff.setEffectType(effectStats);
                

                switch (endingType)
                {
                    case Effect.effectTeminationType.time:
                        eff.setEndingType(time);
                        break;

                    case Effect.effectTeminationType.charges:
                        switch (chargesTypeCreep)
                        {
                            case Effect.effectChargesTypeCreep.afterNCreepsDie:
                                chargesType = Effect.effectChargesType.afterNCreepsDie;
                                break;

                            case Effect.effectChargesTypeCreep.onDeath:
                                chargesType = Effect.effectChargesType.onDeath;
                                break;

                            case Effect.effectChargesTypeCreep.afterTakingNHit:
                                chargesType = Effect.effectChargesType.afterTakingNHit;
                                break;

                            case Effect.effectChargesTypeCreep.afterTakingNCritHit:
                                chargesType = Effect.effectChargesType.afterTakingNCritHit;
                                break;

                            case Effect.effectChargesTypeCreep.afterNEnteringHex:
                                chargesType = Effect.effectChargesType.afterNEnteringHex;
                                break;
                        }
                        eff.setEndingType(chargesType, chargesCount);
                        break;

                }

                eff.effectId = effectId;
                eff.Name = effectName;
                eff.Description = effectDescription;
                eff.icon = effectIcon;

                _creep.applyEffect(eff);

                eff.initialize();
                eff.OnEnd += clear;
            }
        }
    }

    void clear(Effect eff)
    {
        eff = null;
    }

    public override string generateToolTip()
    {
        return "<b>" + abilName + "</b>\n" + description;
    }

        public void deinitialize()
    {
        switch (activationType)
        {
            case AbilityActivationType.creepDie:
                //TODO: Implent
                break;

            case AbilityActivationType.onAttack:
                tower.onAttack -= cast;
                break;

            case AbilityActivationType.onCrit:
                tower.onCritHit -= cast;
                break;

            case AbilityActivationType.onHit:
                tower.onHit -= cast;
                break;

            case AbilityActivationType.onLevelUp:
                tower.onLevelUp -= cast;
                break;

            case AbilityActivationType.onReload:
                tower.onReload -= cast;
                break;

            case AbilityActivationType.onUpgraid:
                //TODO: Implent
                break;
        }
    }
}

﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class GlobalMapObject : MonoBehaviour {

    [Inspect]
    public MapObjectFlagsHolder flagsHolder;

    [Inspect]
    [Restrict("getFlagsNames")]
    public string availableFlag;

    IList getFlagsNames()
    {
        List<string> collection = new List<string>();
        foreach (KeyValuePair<string, bool> flag in flagsHolder.globalMapFlags)
        {
            collection.Add(flag.Key);
        }
        return collection;
    }

	void Awake () {
        if (SaveLoad.firstLoadComplete)
        {
            ini();
        }
        else
        {
            SaveLoad.OnPlayerLoaded += ini;
        }
    }

    void ini()
    {
        Debug.Log(availableFlag);
        Debug.Log(MapObjectFlagsHolder.GlobalMapFlags);
        Debug.Log(MapObjectFlagsHolder.GlobalMapFlags[availableFlag] + "hooray");
        if (!MapObjectFlagsHolder.GlobalMapFlags[availableFlag])
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}

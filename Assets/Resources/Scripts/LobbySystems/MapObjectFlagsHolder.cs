﻿using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class MapObjectFlagsHolder : MonoBehaviour {

    [Serializable]
    public class MapObjectFlagsDictionary : UDictionary<string, bool> { }

    [Inspect]
    public MapObjectFlagsDictionary globalMapFlags = new MapObjectFlagsDictionary();

    public static MapObjectFlagsDictionary GlobalMapFlags;

    static GameObject MapFlagsGameObject;
    static MapObjectFlagsHolder that;

    private void Awake()
    {
        Debug.Log("Awake");
        if (MapFlagsGameObject == null)
        {
            Debug.Log("Creating static object");
            DontDestroyOnLoad(gameObject);
            MapFlagsGameObject = gameObject;
            that = this;
        }
        else if (MapFlagsGameObject != this)
        {
            Destroy(gameObject);
        }

    }

    void Start () {
        if (SaveLoad.firstLoadComplete)
        {
            ini();
        } else
        {
            SaveLoad.OnPlayerLoaded += ini;
        }
        
    }

    void ini ()
    {
        GlobalMapFlags = SaveLoad.playerData.mapFlags;
    }
	
	public static MapObjectFlagsDictionary getDefaultFlags()
    {
        //Debug.Log("Sending default flags");
        //Debug.Log(that);
        //Debug.Log(that.globalMapFlags);
        return that.globalMapFlags;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Talants: MonoBehaviour {

    public delegate void onTalantGain();
    public static event onTalantGain OnTalantGain;

    public delegate void onTalantsChange();
    public static event onTalantsChange OnTalantsChange;

    public enum talantType
    {
        supplyLimit,
        startingGold,
        startingCrystals,
        startingEssence,
        expirienceBonus
    }

    public static int freeTalantPoints = 0;

    //Supply limit

    public static Talant supplyLimit;

    //Starting gold 

    public static Talant startingGold;

    //Starting crystal

    public static Talant startingCrystal;

    //Starting essence

    public static Talant startingEssence;

    //Expirience bonus

    public static Talant expirienceBonus;

    public static GameObject TalantsGameObject;
    public void Awake()
    {

        if (TalantsGameObject == null)
        {
            DontDestroyOnLoad(gameObject);
            TalantsGameObject = gameObject;
        }
        else if (TalantsGameObject != this)
        {
            Destroy(gameObject);
        }

        if (SaveLoad.firstLoadComplete)
        {
            loadSavedTalants();
        }
        else
        {
            SaveLoad.OnPlayerLoaded += loadSavedTalants;
        }
    }

    public void loadSavedTalants()
    {
        supplyLimit = new Talant("Supply Limit", 50, "How much supply you will have at start of Arena");
        startingGold = new Talant("Starting Gold", 50, "How much gold you will have at start of Arena");
        startingCrystal = new Talant("Starting Crystals", 50, "How much crystals you will have at start of Arena");
        startingEssence = new Talant("Starting Essence", 50, "How much essence you will have at start of Arena");
        expirienceBonus = new Talant("Bonus Expirience", 50, "Expirience multiplier for your towers");

        /*freeTalantPoints = PlayerPrefs.GetInt("freeTalantPoints");

        supplyLimit.level = PlayerPrefs.GetInt("supplyLimitTalant");*/

        supplyLimit.level = SaveLoad.playerData.supplyLimit;
        startingGold.level = SaveLoad.playerData.startingGold;
        startingCrystal.level = SaveLoad.playerData.startingCrystals;
        startingEssence.level = SaveLoad.playerData.startingEssence;
        expirienceBonus.level = SaveLoad.playerData.expirienceBonus;

        freeTalantPoints = SaveLoad.playerData.freeTalantPoints;




        supplyLimit.OnTalantChange += SaveTalantPoints;
        startingGold.OnTalantChange += SaveTalantPoints;
        startingCrystal.OnTalantChange += SaveTalantPoints;
        startingEssence.OnTalantChange += SaveTalantPoints;
        expirienceBonus.OnTalantChange += SaveTalantPoints;
    }

    public void SaveTalantPoints()
    {
        if (OnTalantsChange != null)
        {
            OnTalantsChange();
        }

        SaveLoad.updateSaveData();
    }


    public static void giveTalantPoint()
    {
        freeTalantPoints++;

        /*PlayerPrefs.SetInt("freeTalantPoints", freeTalantPoints);
        PlayerPrefs.Save();*/
        SaveLoad.updateSaveData();

        Debug.Log("Added talant point");

        if (OnTalantGain != null)
        {
            OnTalantGain();
        }
    }
}

public class Talant
{
    public int level;
    public int levelMax;
    public string name;
    public string description;

    public delegate void onTalantChange();
    public event onTalantChange OnTalantChange;

    public Talant (string nam, int maxLvl, string desc)
    {
        level = 0;
        levelMax = maxLvl;
        name = nam;
        description = desc;
    }

    public void addPoint()
    {
        if (Talants.freeTalantPoints > 0 && level < levelMax)
        {
            level++;
            Talants.freeTalantPoints--;
            if (OnTalantChange != null)
            {
                OnTalantChange();
            }
        }
        
    }

    public void removePoint()
    {
        if (level > 0)
        {
            level--;
            Talants.freeTalantPoints++;
            if (OnTalantChange != null)
            {
                OnTalantChange();
            }
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInspector;
using System;

[AdvancedInspector]
public class RuneSchematic : MonoBehaviour  {

    [Inspect]
    public string runeName;

    [Inspect]
    public List<RuneStatScheme> statBonuses;

    [Help("checkNumberOfBonuses")]
    [Inspect]
    public int numberOfBonuses = 0;

    [Inspect]
    bool allowSameStatBonus = false;

    private HelpItem checkNumberOfBonuses()
    {
        if (statBonuses.Count >= numberOfBonuses)
            return new HelpItem(HelpType.Info, "");
        else
            return new HelpItem(HelpType.Error, "Not enough bonuses type!");
    }

    public Rune generate()
    {
        Rune rune = new Rune();
        rune.Name = runeName;


        for (int i = 0; i < numberOfBonuses; i++)
        {
            if (allowSameStatBonus)
            {
                rune.addStat(getRandomStatBonus().createRuneStat());
            } else
            {
                RuneStatScheme runeStatScheme = getRandomStatBonus();
                while (runeStatScheme.beingUsedInCerrentGenerating)
                {
                    runeStatScheme = getRandomStatBonus();
                }
                rune.addStat(runeStatScheme.createRuneStat());
            }
        }

        foreach (RuneStatScheme runeStatScheme in statBonuses)
        {
            runeStatScheme.beingUsedInCerrentGenerating = false;
        }

        return rune;
    }

    RuneStatScheme getRandomStatBonus()
    {
        return statBonuses[UnityEngine.Random.Range(0, statBonuses.Count)];
    }
}

[Serializable]
public class RuneStatScheme
{
    [Inspect] public Stat.TowerStat statType;
    [Inspect] public Stat.bonusType bonusType;
    [Inspect] public float minValue;
    [Inspect] public float MaxValue;

    public bool beingUsedInCerrentGenerating = false;

    public RuneStat createRuneStat()
    {
        RuneStat runeStat = new RuneStat();
        runeStat.statType = statType;
        switch (bonusType)
        {
            case Stat.bonusType.plain:
                runeStat.stat = addPlainBonus(UnityEngine.Random.Range(minValue, MaxValue));
                break;

            case Stat.bonusType.percent:
                runeStat.stat = addPercentBonus(UnityEngine.Random.Range(minValue, MaxValue));
                break;

            case Stat.bonusType.multiplier:
                runeStat.stat = addMultiplierBonus(UnityEngine.Random.Range(minValue, MaxValue));
                break;
        }
        beingUsedInCerrentGenerating = true;
        return runeStat;
    }

    Stat addPlainBonus(double value)
    {
        Stat stat = new Stat(0);
        stat.addPlainBonus(value);
        return stat;
    }

    Stat addPercentBonus(double value)
    {
        Stat stat = new Stat(0);
        stat.addBonus(value);
        return stat;
    }

    Stat addMultiplierBonus(float value)
    {
        Stat stat = new Stat(0);
        stat.addMultiplier(new StatMultiplier(value));
        return stat;
    }
}
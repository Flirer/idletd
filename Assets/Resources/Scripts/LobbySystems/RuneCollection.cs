﻿using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class RuneCollection : MonoBehaviour {

    //[Inspect]
    public static List<Rune> runes;

    public static List<GameObject> slotsForInsertionInTower;

    [Inspect]
    public RuneSchematic runeSchematic;

    public static RuneSchematic _runeSchematic;

    [Inspect]
    public GameObject _runeCollectionItemPrefab;

    public static GameObject runeCollectionItemPrefab;

    [Inspect]
    public GameObject _runeCollectionItemDragablePrefab;

    public static GameObject runeCollectionItemDragablePrefab;

    public static void buyNewRune()
    {
        //Debug.Log("Player credits: " + Lobby.getCredits());
        if (Inventory.Gold.amount >= 20)
        {
            SaveLoad.newRune(_runeSchematic.generate());
            Inventory.Gold.amount -= 20;
        }
    }

    public static void insertRune(/*Rune rune, int tower, int slot*/)
    {
        /*
        rune.towerPrefabId = tower;
        rune.towerSlotId = slot;
        SaveLoad.updateSaveData();
        */
        Rune rune = SlotForDragable.lastDropedItem.GetComponent<DragHandlerRune>().rune;
        rune.towerPrefabId = SlotForDragable.triggeredSlot.GetComponent<RuneCollectionSlot>().towerPrefabId;
        rune.towerSlotId = SlotForDragable.triggeredSlot.GetComponent<RuneCollectionSlot>().slotId;

        SaveLoad.updateSaveData();
    }

    public static void releaseRune()
    {
        Rune rune = SlotForDragable.lastPickedItem.GetComponent<DragHandlerRune>().rune;
        rune.towerPrefabId = -1;
        rune.towerSlotId = -1;
        SaveLoad.updateSaveData();
    }

	void Awake () {
        _runeSchematic = runeSchematic;
        runeCollectionItemPrefab = _runeCollectionItemPrefab;
        runeCollectionItemDragablePrefab = _runeCollectionItemDragablePrefab;
        //Debug.Log(_runeSchematic);
        SaveLoad.OnPlayerLoaded += initialize;
    }

    void initialize()
    {
        runes = SaveLoad.playerData.runes;
        //Debug.Log(runes.Count);
    }

    public static void renderRune(Transform container)
    {
        Debug.Log("Render rune in Collection");
        foreach (Transform child in container)
        {
            GameObject.Destroy(child.gameObject);
        }


        foreach (Rune rune in SaveLoad.playerData.runes)
        {
            GameObject rci = Instantiate(runeCollectionItemPrefab) as GameObject;
            rci.transform.SetParent(container, false);
            RuneCollectionItem runeCollectionItem = rci.GetComponent<RuneCollectionItem>();

            runeCollectionItem.rune = rune;
            runeCollectionItem.updateLables();

            runeCollectionItem.icon.SetActive(true);
            if (rune.towerPrefabId != -1)
            {
                runeCollectionItem.icon.SetActive(false);
            }
        }
    }

    public static void renderRunesForInsertion(Transform container)
    {
        Debug.Log("Render rune for insertion in Collection");
        foreach (Transform child in container)
        {
            GameObject.Destroy(child.gameObject);
        }


        foreach (Rune rune in SaveLoad.playerData.runes)
        {
            GameObject rci = Instantiate(runeCollectionItemDragablePrefab) as GameObject;
            rci.transform.SetParent(container, false);
            RuneCollectionItem runeCollectionItem = rci.GetComponent<RuneCollectionItem>();

            runeCollectionItem.rune = rune;
            runeCollectionItem.updateLables();

            rci.GetComponent<RuneCollectionItem>().icon.transform.parent.GetComponent<RuneCollectionSlot>().onlyTakeOriginal = true;
            rci.GetComponent<RuneCollectionItem>().icon.GetComponent<DragHandlerRune>().rune = rune;

            runeCollectionItem.icon.SetActive(true);
            /*if (rune.towerPrefabId != -1 && Lobby.towerForRuneInsertion != null)
            {
                if (rune.towerPrefabId == Lobby.towerForRuneInsertion.prefabId)
                {
                    runeCollectionItem.icon.transform.SetParent(
                        slotsForInsertionInTower[rune.towerSlotId].transform,
                        false);
                }
                else
                {
                    runeCollectionItem.icon.SetActive(false);
                }

            }*/
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticColors : MonoBehaviour {

    
    public static Color colorNormal = Color.white;
    public static Color colorBuildMode = new Color(0.6f, 1, 1, 1.8f);
    public static Color colorSelected = new Color(0.8f, 0.8f, 0.9f);

    public static Color colorNormalHover = new Color(0.9f, 0.9f, 0.9f);
    public static Color colorBuildModeHover = new Color(0.5f, 0.9f, 0.9f, 1.9f);
    public static Color colorSelectedBuildMode = new Color(0.8f, 0.8f, 0.9f);

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Resource {

    public double amount
    {
        get {
            return _amount.data;
        }

        set {
            _amount.data = value;
            if (_maxAmount.data > 0 && _amount.data > _maxAmount.data) _amount.data = _maxAmount.data;
            if (onChange != null) onChange();
        }
    }

    public double maxAmount
    {
        get
        {
            return _maxAmount.data;
        }

        set
        {
            _maxAmount.data = value;
            if (onChange != null) onChange();
        }
    }

    refData<double> _amount;
    refData<double> _maxAmount;
    bool canGoOverLimit = false;


    public delegate void OnChange();
    public event OnChange onChange;

    public Resource(refData<double> amount,
                    double maxAmount = -1,
                    bool canGoOverLimit = false)
    {
        _amount = amount;
        _maxAmount = new refData<double>(maxAmount);
        this.canGoOverLimit = canGoOverLimit;
    }
}


[Serializable]
public class TowerResearch
{
    public bool researched = false;

    public TowerResearch(bool b)
    {
        researched = b;
    }
}

﻿using AdvancedInspector;
using System;
using UnityEngine;

[Serializable]
public class StatMultiplier {

    [Inspect]
    public float amount;

    public StatMultiplier(float am)
    {
        setAmount(am);
    }

    public void setAmount(float am)
    {
        amount = am;
    }
}
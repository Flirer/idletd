﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexUtilControl : MonoBehaviour {

    Hex hex;

    public Sprite rangeCheckSprite;
    public Sprite noWalkSprite;
    public Sprite pathMarkerSprite;
    public Sprite buffMarkerSprite;
    public Sprite debuffMarkerSprite;
    public Sprite TerrReqMet;
    public Sprite TerrReqUnmet;

    public Transform left;
    public Transform topLeft;
    public Transform bottomLeft;
    public Transform right;
    public Transform topRight;
    public Transform bottomRight;

    SpriteRenderer spriteRenderer;

    private void Awake()
    {
        hex = transform.parent.GetComponent<Hex>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void setRangeCheck()
    {
        spriteRenderer.sprite = rangeCheckSprite;
    }

    public void clear()
    {
        //if (Inventory.Territories.Contains(hex.territoryId))
        spriteRenderer.sprite = null;

    }

    public void highlightTerritory()
    {
        unlightTerritory();

        if (hex.neighbors["Left"] == null ||
            hex.neighbors["Left"].territoryId != hex.territoryId)
        {
            left.gameObject.SetActive(true);
        }

        if (hex.neighbors["LeftTop"] == null ||
            hex.neighbors["LeftTop"].territoryId != hex.territoryId)
        {
            topLeft.gameObject.SetActive(true);
        }

        if (hex.neighbors["LeftBottom"] == null ||
            hex.neighbors["LeftBottom"].territoryId != hex.territoryId)
        {
            bottomLeft.gameObject.SetActive(true);
        }

        if (hex.neighbors["Right"] == null ||
            hex.neighbors["Right"].territoryId != hex.territoryId)
        {
            right.gameObject.SetActive(true);
        }

        if (hex.neighbors["RightTop"] == null ||
            hex.neighbors["RightTop"].territoryId != hex.territoryId)
        {
            topRight.gameObject.SetActive(true);
        }

        if (hex.neighbors["RightBottom"] == null ||
            hex.neighbors["RightBottom"].territoryId != hex.territoryId)
        {
            bottomRight.gameObject.SetActive(true);
        }
    }

    public void territoryUnlock()
    {
        spriteRenderer.sprite = null;
    }

    public void unlightTerritory()
    {
        left.gameObject.SetActive(false);
        topLeft.gameObject.SetActive(false);
        bottomLeft.gameObject.SetActive(false);
        right.gameObject.SetActive(false);
        topRight.gameObject.SetActive(false);
        bottomRight.gameObject.SetActive(false);
    }
}

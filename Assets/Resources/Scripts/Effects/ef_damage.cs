﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ef_damage : Effect
{
    double timeUntillNextTick;
    double tickLength;

    double damage;
    effectChargesType chargeType;

    public enum DamageEffectType {
        start,
        end,
        onCharges,
        overTime,
        overTimeCharges
    }

    public DamageEffectType damageEffectType;

    delegate void InitializeType();
    InitializeType initializeType;

    public ef_damage (Creep crp, Ability abil)
    {
        parentAbility = abil;
        creep = crp;
    }

    /// <summary>
    /// Simply deal damage as soon as meet target
    /// </summary>
    /// <param name="amount">Damage amount</param>
    public void setEffectType(double amount)
    {
        damageEffectType = DamageEffectType.start;
        damage = amount;
        initializeType = initializeDamageOnStart;
        //Ending in initialize
    }

    // <summary>
    /// Deal damage when effect ends
    /// </summary>
    /// <param name="time">Time until damage done</param>
    /// <param name="amount">Amount of damage</param>
    public void setEffectType(double time, double amount)
    {
        damageEffectType = DamageEffectType.end;
        timeLeft = time;
        damage = amount;
        initializeType = initializeDamageOnEnd;
        //Ending in fixed update
    }

    // <summary>
    /// Deal damage over time
    /// </summary>
    /// <param name="tickTime">Time between ticks</param>
    /// <param name="amount">Damage for every tick</param>
    /// <param name="tickCount">Number of ticks</param>
    public void setEffectType(double tickTime, double amount, int tickCount)
    {
        damageEffectType = DamageEffectType.overTime;
        charges = tickCount;
        damage = amount;
        tickLength = tickTime;
        initializeType = initializeDamageOverTime;
        //Ending in fixed update
    }

    // <summary>
    /// Deal damage with limited amount of charges
    /// </summary>
    /// <param name="amount">Damage</param>
    /// <param name="chargeCount">How many times damage will charge</param>
    /// <param name="type">Charge type</param>
    public void setEffectType(double amount, int chargeCount, effectChargesType type)
    {
        damageEffectType = DamageEffectType.onCharges;
        damage = amount;
        chargeType = type;
        initializeType = initializeDamageOnCharge;
        setEndingType(type, chargeCount);
    }

    // <summary>
    /// Deal damage with charges over limited amount of time
    /// </summary>
    /// <param name="amount"></param>
    public void setEffectType(double amount, int chargeCount, effectChargesType type, double time)
    {
        damageEffectType = DamageEffectType.onCharges;
        damage = amount;
        chargeType = type;
        tickLength = time;
        initializeType = initializeDamageTimedOnCharge;
        setEndingType(type, chargeCount, time);
    }

    void doDamage()
    {
        creep.takeDamage(parentAbility, damage);
    }

    protected override void spendCharges(EventInfo info)
    {
        doDamage();
        base.spendCharges(info);
    }


    public override void initialize()
    {
        creep.onDying += terminate_;
        
        initializeType();
    }

    void initializeDamageOnStart()
    {
        doDamage();
        terminate();
        update = chargesUpdate;
    }

    void initializeDamageOnEnd()
    {
        update = timedUpdate;
        
    }

    void initializeDamageOverTime()
    {
        timeUntillNextTick = tickLength;
        update = chargesUpdate;
    }

    void initializeDamageOnCharge()
    {
        update = chargesUpdate;
    }

    void initializeDamageTimedOnCharge()
    {
        update = timedUpdate;
    }




    public override void fixedUpdate()
    {

        switch (damageEffectType)
        {
            case DamageEffectType.overTime:

                if (timeUntillNextTick <= 0)
                {
                    doDamage();
                    timeUntillNextTick = tickLength;
                    charges--;
                    if (charges <= 0)
                    {
                        timedEnding();
                        return;
                    }
                }
                timeUntillNextTick -= Time.fixedDeltaTime;

                break;
            case DamageEffectType.end:
                if (timeLeft <= 0)
                {
                    doDamage();
                    timedEnding();
                    return;
                }
                timeUntillNextTick -= Time.fixedDeltaTime;
                break;
        }

        update();
    }

    void terminate_(EventInfo info)
    {
        terminate();
    }

    public override void terminate()
    {
        base.terminate();
        creep.removeEffect(this);
        creep.onDying -= terminate_;
    }
}

﻿using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ef_statChange : Effect {

    //STAT

    //Stat statToChange;
    //Stat.StatBonusType statBonusType;
    //double bonusAmount;
    //StatMultiplier multiplier;

    public List<EffectStat> effectStats;


    /*public void setEffectType(Stat stat, Stat.StatBonusType type, double amount)
    {
        statToChange = stat;
        statBonusType = type;
        bonusAmount = amount;

        if (statBonusType == Stat.StatBonusType.multiplier)
        {
            multiplier = new StatMultiplier((float)amount);
        }
    }*/

    public ef_statChange () {
            
    }

    public void setEffectType(List<EffectStat> effectStats)
    {
        //Debug.Log("Setting effect ID " + id + " for CREEP " + creep.id);
        this.effectStats = new List<EffectStat>();
        foreach (EffectStat stat in effectStats)
        {
            this.effectStats.Add(new EffectStat(stat));

            EffectStat _stat = this.effectStats[this.effectStats.Count - 1];

            if (targetType == EffectTarget.Creep)
                _stat.statToChange = Stat.getCreepStatByType(creep, _stat.creepStatType);
            else
                _stat.statToChange = Stat.getTowerStatByType(tower, _stat.towerStatType);

            if (_stat.statBonusType == Stat.StatBonusType.multiplier)
            {
                _stat.multiplier = new StatMultiplier((float)_stat.bonusAmount);
            }
        }
    }

    /// <summary>
    /// Kick in actuall stat bonus in game
    /// </summary>
    public override void initialize()
    {
        foreach (EffectStat stat in effectStats)
        {
            switch (stat.statBonusType)
            {
                case Stat.StatBonusType.plain:
                    stat.statToChange.addPlainBonus(stat.bonusAmount);
                    break;

                case Stat.StatBonusType.percent:
                    stat.statToChange.addBonus(stat.bonusAmount);
                    break;

                case Stat.StatBonusType.multiplier:
                    stat.statToChange.addMultiplier(stat.multiplier);
                    break;
            }
        }
    }


    public override void  fixedUpdate()
    {
        if (update != null)
            update();
    }


    void terminate_(Tower tow, Weapon w)
    {
        terminate();
    }

    public override void terminate()
    {
        foreach (EffectStat stat in effectStats)
        {
            //Debug.Log("- - STAT BEFOR: " + stat.statToChange.Value);
            switch (stat.statBonusType)
            {
                case Stat.StatBonusType.plain:
                    stat.statToChange.removePlainBonus(stat.bonusAmount);
                    break;

                case Stat.StatBonusType.percent:
                    stat.statToChange.removeBonus(stat.bonusAmount);
                    break;

                case Stat.StatBonusType.multiplier:
                    stat.statToChange.removeMultiplier(stat.multiplier);
                    break;
            }
            //Debug.Log("- - STAT AFTER: " + stat.statToChange.Value);
        }
        

        base.terminate();
    }
}

[AdvancedInspector]
[Serializable]
public class EffectStat
{

    [SerializeField]
    [Inspect]
    public Stat.TowerStat towerStatType;

    [SerializeField]
    [Inspect]
    public Stat.CreepStat creepStatType;

    [SerializeField]
    [Inspect]
    public Stat statToChange;

    [SerializeField]
    [Inspect]
    public Stat.StatBonusType statBonusType;

    [SerializeField]
    [Inspect]
    public double bonusAmount;

    [SerializeField]
    [HideInInspector]
    public StatMultiplier multiplier;

    public EffectStat ()
    {
        
    }

    public EffectStat(EffectStat stat)
    {
        towerStatType = stat.towerStatType;
        creepStatType = stat.creepStatType;
        statToChange = stat.statToChange;
        statBonusType = stat.statBonusType;
        bonusAmount = stat.bonusAmount;
        multiplier = stat.multiplier;
    }
    /*protected bool showStatType //ЗАКОНЧИТЬ С ОТОБРАЖЕНИЕМ В ЭФФЕКТАХ СТАТОВ, чтобы показывались либо статы на крипов, либо на башни
    {
        get
        {
            return true;
        }
    }*/
}
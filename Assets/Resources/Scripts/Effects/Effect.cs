﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Effect
{
    //For time based effects
    protected double timeLeft;
    //for charge based effects
    protected int charges;

    protected effectTeminationType endingType;
    protected effectChargesType endingTypeCharges;

    protected EffectTarget targetType;
    //TODO: Push this part to ability side?
    protected EffectTargetForTower targetTowerType;
    protected EffectTargetForCreep targetCreepType;

    [HideInInspector]
    public Tower tower;
    protected Creep creep;
    protected MapObject effectTarget;

    protected Ability parentAbility;

    public static int ID = 0;
    public int id = -1;
    public int effectId = -1;
    public string Name = "Effect";
    public string Description = "Undefined description";
    public Sprite icon;

    //TODO:Move here more stuff from effects
    public enum effectTeminationType
    {
        time, charges , manual
    }

    effectTeminationType terminationType;

    public enum effectChargesType
    {
        /*Tower part*/
        afterNReload,
        afterNAttacks,
        afterNHit,
        afterNCritAttack,
        afterNCast,
        afterNLevelUp,
        afterNUpgraids,
        /*Creep part*/
        afterTakingNHit,
        afterTakingNCritHit,
        afterNEnteringHex,
        afterNCreepsDie,
        onDeath
    }

    public enum effectChargesTypeCreep
    {
        afterTakingNHit,
        afterTakingNCritHit,
        afterNEnteringHex,
        afterNCreepsDie,
        onDeath
    }

    public enum effectChargesTypeTower
    {
        /*Tower part*/
        afterNReload,
        afterNAttacks,
        afterNHit,
        afterNCritAttack,
        afterNCast,
        afterNLevelUp,
        afterNUpgraids,
        afterNCreepsDie
    }

    effectChargesType terminationChargesType;

    //THIS GOES TO ABILITY
    public enum EffectTarget
    {
        Creep, Tower
    }

    EffectTarget effectTargetType;

    public enum EffectTargetForTower
    {
        allInRange, severalInRange, self
    }

    public enum EffectTargetForCreep
    {
        allInRange, severalInRange, self
    }

    public virtual void initialize() { }
    public virtual void fixedUpdate() { }

    public virtual void terminate() {
        if (targetType == EffectTarget.Creep)
        {
            creep.removeEffect(this);
        } else if (targetType == EffectTarget.Tower)
        {
            tower.removeEffect(this);
        }
    }

    public virtual string generateToolTip()
    {
        return "<b>" + Name + "</b>\n" + Description; 
    }

    public delegate void onEffectEnd(Effect effect);
    public event onEffectEnd OnEnd;

    protected delegate void Update();
    protected Update update;

    /*protected delegate void Terminate();
    protected Terminate terminate;*/

    //Effect type:

    public Effect ()
    {
        id = ID;
        ID++;
    }


    public enum EffectType
    {
        statChange,
        damage,
        dot,
        addWeapon,
        addAbility
    }


    public void setTarget(Creep crp)
    {
        targetType = EffectTarget.Creep;
        creep = crp;
    }

    public void setTarget(Tower twr)
    {
        targetType = EffectTarget.Tower;
        tower = twr;
    }


    //TODO: Hybrid ending system time + charges
    public void setEndingType(double lengthOfEffect)
    {
        endingType = effectTeminationType.time;
        timeLeft = lengthOfEffect;
        update = timedUpdate;
    }

    public void setEndingType(effectChargesType type, int amountOfCharges)
    {
        terminationChargesType = type;
        charges = amountOfCharges;
        update = chargesUpdate;
        switch (type)
        {
            //Tower cases
            case effectChargesType.afterNAttacks:
                tower.onAttack += spendCharges;
                break;

            case effectChargesType.afterNCritAttack:
                tower.onCritHit += spendCharges;
                break;

            case effectChargesType.afterNCast:
                //TODO: implent casts
                break;

            case effectChargesType.afterNLevelUp:
                tower.onLevelUp += spendCharges;
                break;

            case effectChargesType.afterNReload:
                tower.onReload += spendCharges;
                break;

            case effectChargesType.afterNUpgraids:
                //TODO: implent upgraids
                break;

            case effectChargesType.afterNCreepsDie:
                //TODO: implent creeps death detection
                break;

            case effectChargesType.afterTakingNCritHit:
                creep.onTakingCrit += spendCharges;
                break;

            case effectChargesType.afterNEnteringHex:
                creep.onExitingHex += spendCharges;
                break;

            case effectChargesType.afterNHit:
                creep.onTakingHit += spendCharges;
                break;

            case effectChargesType.onDeath:
                creep.onDying += spendCharges;
                break;
        }
    }

    public void setEndingType(effectChargesType type, int amountOfCharges, double time)
    {
        setEndingType(type, amountOfCharges);
    }

    public void setEndingType(Ability ability)
    {
        Debug.Log("setting ending type for aura");
        parentAbility = ability;

        update = locationCheckUpdate;

        if (targetType == EffectTarget.Creep)
        {
            creep.onExitingHex += terminateAura;
        }
        else
        {
            tower.onMoveBegin += terminateAura;
            tower.onBeingRemoved += terminateAura;
        }
        parentAbility.onAbilityRemove += terminateAura;
    }


    //Inner logic

    void terminateAura(EventInfo info)
    {
        terminateAura();
    }

    void terminateAura()
    {
        if (targetType == EffectTarget.Creep)
        {
            creep.onExitingHex -= terminateAura;
        } else
        {
            tower.onMoveBegin -= terminateAura;
            tower.onBeingRemoved -= terminateAura;
        }
        
        parentAbility.onAbilityRemove -= terminateAura;
        terminate();
    }

    protected void timedUpdate()
    {
        if (timeLeft <= 0)
        {
            timedEnding();
        }
        timeLeft -= Time.fixedDeltaTime;
    }

    protected void locationCheckUpdate()
    {

    }

    protected void chargesUpdate()
    {

    }

    protected virtual void spendCharges(EventInfo info)
    {
        Debug.Log("Spending charge");

        charges--;
        if (charges == 0)
        {
            chargeEnding();
        }
    }

    //Ending

    protected void timedEnding()
    {
        if (OnEnd != null)
        {
            OnEnd(this);
        }
        //TODO: Make sure this one use right version of terminate, not of base class
        terminate();
    }

    void chargeEnding() {
        switch (terminationChargesType)
        {
            //Tower cases
            case effectChargesType.afterNAttacks:
                tower.onAttack -= spendCharges;
                break;

            case effectChargesType.afterNCritAttack:
                tower.onCritHit -= spendCharges;
                break;

            case effectChargesType.afterNCast:
                //TODO: implent casts
                break;

            case effectChargesType.afterNLevelUp:
                tower.onLevelUp -= spendCharges;
                break;

            case effectChargesType.afterNReload:
                tower.onReload -= spendCharges;
                break;

            case effectChargesType.afterNUpgraids:
                //TODO: implent upgraids
                break;

            case effectChargesType.afterNCreepsDie:
                //TODO: implent creeps death detection
                break;

            case effectChargesType.afterTakingNCritHit:
                creep.onTakingCrit -= spendCharges;
                break;

            case effectChargesType.afterNEnteringHex:
                creep.onExitingHex -= spendCharges;
                break;

            case effectChargesType.afterNHit:
                creep.onTakingHit -= spendCharges;
                break;

            case effectChargesType.onDeath:
                creep.onDying -= spendCharges;
                break;
        }

        terminate();

        if (OnEnd != null)
        {
            OnEnd(this);
        }
    }
}

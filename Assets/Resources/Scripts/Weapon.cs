﻿using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class Weapon : MonoBehaviour {

    
	public Tower tower;

    [Inspect]
    [Group("Visuals", 1)]
    public GameObject projectile;
    public int id = 0;

    [Inspect(0)]
    public string Name = "Weapon";

    [Inspect]
    public SourceType sourceType;

    [Inspect]
    [Group("Visuals")]
    public Sprite icon;

    [Inspect("towerGotModel")]
    [Restrict("getSubModels")]
    [Group("Visuals")]
    public TowerSubModel model;

    [Inspect("towerGotModel")]
    [Restrict("getPoints")]
    [Group("Visuals")]
    public Transform projectileOriginate = null;

    bool towerGotModel
    {
        get
        {
            if (GetComponent<Tower>().model)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    bool weaponGotModel
    {
        get
        {
            if (model)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


    List<TowerSubModel> getSubModels()
    {
        List<TowerSubModel> result = new List<TowerSubModel>();
        result.Add(null);
        if (model && GetComponent<Tower>().model)
            result.AddRange(GetComponent<Tower>().model.subModels);

        return null;
    }

    List<Transform> getPoints()
    {
        if (model)
            return model.points;
        else
            return null;
    }




    public delegate void AttackAnimation();
    [Inspect("weaponGotModel")]
    [Group("Visuals")]
    public AttackAnimation attackAnimation = null;

    public delegate void BeginAttackAnimation();
    [Inspect("weaponGotModel")]
    [Group("Visuals")]
    public BeginAttackAnimation beginAttackAnimation = null;

    public delegate void EndAttackAnimation();
    [Inspect("weaponGotModel")]
    [Group("Visuals")]
    public EndAttackAnimation endAttackAnimation = null;

    public delegate void ReloadAnimation();
    [Inspect("weaponGotModel")]
    [Group("Visuals")]
    public ReloadAnimation reloadAnimation = null;


    [Inspect]
    [Group("Stats", 2)]
    public int range;

    [Inspect]
    [Group("Stats")]
    public float baseAttackSpeed = 1;

    [Inspect(InspectorLevel.Debug)]
    public Stat attackSpeed; 
	double attackColdown = 0;
    bool attackOnColdown = false;

    [Inspect]
    [Group("Stats")]
    public float baseReloadSpeed = 2;
    [Inspect(InspectorLevel.Debug)]
    public Stat reloadSpeed; //STAT
    double stockReloadState = 0;

    bool stockIsReloading = false;
    [Inspect]
    [Group("Stats")]
    public int ammoInStockMaxBase = 5; //STAT
    [Inspect(InspectorLevel.Debug)]
    public Stat ammoInStockMax;
    public int ammoInStock = 5;

    [Inspect]
    [Group("Stats")]
    public float baseDamage = 0;
    [Inspect(InspectorLevel.Debug)]
    public Stat damage; //STAT
    StatMultiplier damageLevelBonus;
    StatMultiplier damageUpgraidLevelBonus;
    [Inspect]
    [Group("Stats")]
    public float baseDamageExpirienceBonus = 50;
    [Group("Stats")]
    public float baseDamageUpgraidBonus = 50;

    [Inspect]
    [Group("Stats")]
    public float baseCritChance;
    [Inspect(InspectorLevel.Debug)]
    public Stat critChance;

    [Inspect]
    [Group("Stats")]
    public float baseCritDamage;
    [Inspect(InspectorLevel.Debug)]
    public Stat critDamage;

    [Inspect]
    [Group("Stats")]
    public float baseAoERadius;
    [Inspect(InspectorLevel.Debug)]
    public Stat AoERadius;

    [Inspect]
    [Group("Stats")]
    public float baseAoEDmg;
    [Inspect(InspectorLevel.Debug)]
    public Stat AoEDmg;

    [Inspect(InspectorLevel.Debug)]
    public List<Creep> reachableTargetCreeps;
	public Creep target;

    [Inspect(InspectorLevel.Debug)]
    public List<Hex> reachableHexes;
	Dictionary<Hex, int> hexesToCheck;
	Dictionary<Hex, int> hexesToCheckNext;

    public delegate void onAttack(EventInfo info);
    public event onAttack OnAttack;

    public delegate void onHit(EventInfo info);
    public event onHit OnHit;

    public delegate void onReload(EventInfo info);
    public event onReload OnReload;

    [Inspect]
    public bool primary;
    //STATISTICS:

    [Inspect(4)]
    [Group("Statistics:", 10)]
    int kills = 0;
    [Inspect(4)]
    [Group("Statistics:")]
    double damageDone = 0;

    public enum SourceType
    {
        none,
        ammo,
        heat
    }

    [Inspect]
    public LayerMask weaponmask;

    // Use this for initialization
    void Start () {
		target = null;
        reachableTargetCreeps = new List<Creep>();

    }

	public void initialize() {
        attackSpeed = new Stat(1);
        reloadSpeed = new Stat(baseReloadSpeed);
        damage = new Stat(baseDamage);
        critChance = new Stat(baseCritChance);
        critDamage = new Stat(baseCritDamage);
        ammoInStockMax = new Stat(ammoInStockMaxBase);
        AoERadius = new Stat(baseAoERadius);
        AoEDmg = new Stat(baseAoEDmg);

        damageLevelBonus = new StatMultiplier(1);
        damageUpgraidLevelBonus = new StatMultiplier(1);

        damage.addMultiplier(damageLevelBonus);
        damage.addMultiplier(damageUpgraidLevelBonus);

        tower = GetComponent<Tower>();
        tower.onLevelUp += updateExpLvlBonusDmg;
        tower.onUpgraidLevelUp += updateUpgraidLvlBonusDmg;

        updateRangeAndReach ();

        if (model != null)
        {
            if (model.attackAnimationAvailable)
            {
                model.iniAttackAnimation(this);
            }
        }
	}

	public void FixedUpdate() {
        if (attackOnColdown)
        {
            attackColdown -= Time.fixedDeltaTime;
            if (attackColdown <= 0)
            {
                attackColdown = 0;
                attackOnColdown = false;
            }
        }

		if ((target == null || target.state == Creep.creepState.dying) && reachableTargetCreeps.Count>0) {
            //Debug.Log("Tring to get new target");
			getTarget ();
		}

		if (target != null && target.state != Creep.creepState.dying && !attackOnColdown) {
            //Debug.Log("Attack animation lunch");
            attackColdown = baseAttackSpeed / (float)attackSpeed.Value;
            attackOnColdown = true;
            switch (sourceType)
            {
                case SourceType.ammo:
                    if (!stockIsReloading)
                    {
                        if (attackAnimation != null)
                        {
                            attackAnimation();
                        }
                        else
                        {
                            fire();
                        }
                    }
                    break;

                case SourceType.none:
                    if (attackAnimation != null)
                    {
                        attackAnimation();
                    }
                    else
                    {
                        fire();
                    }
                    break;
            }
            
		}

        if (stockIsReloading)
        {
            stockReloadState -= Time.fixedDeltaTime;
            if (stockReloadState <= 0)
            {
                stockReloadState = 0;
                stockIsReloading = false;
                
            }
        }
    }

    public void updateExpLvlBonusDmg(EventInfo info)
    {

        damageLevelBonus.setAmount(1 * Mathf.Pow(1 +  (baseDamageExpirienceBonus /100), tower.level.data));
        //damageLevelBonus.setAmount(Mathf.Pow(2, tower.level));
        //Debug.Log("Tower upgraid level up " + damageLevelBonus.amount);
        //Debug.Log("New tower level = " + tower.level);

    }

    public void updateUpgraidLvlBonusDmg(EventInfo info)
    {
        damageUpgraidLevelBonus.setAmount(1 * Mathf.Pow(1 + (baseDamageUpgraidBonus) / 100, tower.upgraidLevel.data));
        
    }

	public void updateRangeAndReach () {
        if (range <= 0) return;
        reachableHexes.Clear();
        List<Hex> hexes = tower.locationHex.getNeighborsInRange(range);
        foreach (Hex hex in hexes)
        {
            if (hex.walkable)
            {
                reachableHexes.Add(hex);
                hex.onTowerEnterHex += removeHexFromReachable;
                hex.onCreepEnteringHex += creepEnterReachableHex;
                hex.onCreepExitingHex += creepLeaveReachableHex;
            }
        }
    }

    void removeHexFromReachable(EventInfo info)
    {
        info.hex.onTowerEnterHex -= removeHexFromReachable;
        info.hex.onCreepEnteringHex -= creepEnterReachableHex;
        info.hex.onCreepExitingHex -= creepLeaveReachableHex;
        reachableHexes.Remove(info.hex);
    }

    void creepEnterReachableHex(EventInfo info)
    {
        if (reachableTargetCreeps.Contains(info.creep))
        {
            return;
        } else
        {
            reachableTargetCreeps.Add(info.creep);

            info.creep.onDying += creepDie;

        }
    }

    void creepLeaveReachableHex(EventInfo info)
    {
        //Debug.Log("Creep leave hex " + info.hex.id + " and entering new hex " + info.creep.location.id);
        if (info.creep) { 
            if (reachableHexes.Contains(info.creep.locationHex))
            {
                //Debug.Log("Creep " + info.creep.id + " still in range");
            }
            else
            {
                //Debug.Log("Creep " + info.creep.id + " left range");
                creepLeaveRange(info);
            }
        }
    }

    void creepLeaveRange(EventInfo info)
    {
        if (!reachableHexes.Contains(info.creep.locationHex) && reachableTargetCreeps.Contains(info.creep))
        {
            //Debug.Log("Removing creep " + info.creep.id + " from reachable targets");
            reachableTargetCreeps.Remove(info.creep);
            
            if (target == info.creep)
            {
                target = null;
            }
            
            info.creep.onDying -= creepDie;
        }
    }

    void creepDie(EventInfo info)
    {
        //Debug.Log("Weapon:Creep died");
        reachableTargetCreeps.Remove(info.creep);
        if (info.creep != null && target == info.creep)
        {
            target = null;
        }
    }

    bool getTarget() {
        //Debug.Log("Getting new target, creep count is " + reachableTargetCreeps.Count);

        float dist = 500;
        Creep targ = null;

        //foreach (Creep creep in reachableTargetCreeps)
        for (int i = reachableTargetCreeps.Count-1; i >= 0 ; i--)
        {
            if (reachableTargetCreeps[i] != null && reachableTargetCreeps[i].state != Creep.creepState.dying && reachableTargetCreeps[i].hpCurrent > 0)
            {
                if (reachableTargetCreeps[i].distanceToEnd < dist)
                {
                    dist = reachableTargetCreeps[i].distanceToEnd;
                    targ = reachableTargetCreeps[i];
                }
                //return true;
            } else
            {
                reachableTargetCreeps.RemoveAt(i);
            }
        }
        if (targ)
        {
            target = targ;
            target.onDying += creepDie;
        }
        //Debug.Log("Failed to get new target");
        return false;
	}


	public void fire() {

        if (target != null)
        {
            Projectile projScript;
            Vector3 projectileBornPosition;
            if (projectileOriginate != null)
            {
                projectileBornPosition = projectileOriginate.position;
            }
            else
            {
                projectileBornPosition = transform.position;
            }
            GameObject proj = Instantiate(
                projectile,
                projectileBornPosition,
                Quaternion.identity
            ) as GameObject;
            projScript = proj.GetComponent<Projectile>();
            projScript.ini(target.transform, 0.2f, this);

            projScript.OnProjectileLand += onHitingTarget;
            projScript.OnProjectileDestroy += projectileDestroy;
            /*attackColdown = baseAttackSpeed / (float)attackSpeed.Value;
            attackOnColdown = true;*/
            if (OnAttack != null)
            {
                EventInfo info = new EventInfo();
                info.creep = target;
                info.weapon = this;
                info.tower = tower;
                OnAttack(info);
            }

            if (sourceType == SourceType.ammo)
            {
                ammoHandle();
            }
        }
    }

    void ammoHandle()
    {
        ammoInStock--;
        //Debug.Log(ammoInStock);

        if (ammoInStock <= 0)
        {
            //Debug.Log("Ammo is less then 0");
            stockIsReloading = true;
            stockReloadState = reloadSpeed.Value;
            ammoInStock = (int)ammoInStockMax.Value;
            if (OnReload != null)
            {
                EventInfo info = new EventInfo();
                info.weapon = this;
                info.tower = tower;
                OnReload(info);
            }
        }
    }

    void onHitingTarget(EventInfo inf)
    {
        
        double damageDone = 0;
        List<Creep> creeps = new List<Creep>();

        if (AoERadius.Value == 0)
        {
            if (inf.creep.GetComponent<Creep>().state != Creep.creepState.dying)
            {
                damageDone = dealDamage(inf.creep);
                creeps.Add(inf.creep);
            }
        } else
        {
            
            damageDone += dealDamage(inf.creep);
            creeps.Add(inf.creep);

            Collider2D[] colliders = Physics2D.OverlapCircleAll(inf.projectile.transform.position, (float)AoERadius.Value, weaponmask.value);
            if (colliders.Length > 0)
            {
                foreach (Collider2D coll in colliders)
                {
                    if (coll.GetComponent<Creep>().state != Creep.creepState.dying && coll.GetComponent<Creep>() != inf.creep)
                    {
                        damageDone += dealDamage(coll.GetComponent<Creep>(), AoEDmg.Value);
                        creeps.Add(coll.GetComponent<Creep>());
                    }
                }
            }
        }


        if (OnHit != null)
        {
            EventInfo info = new EventInfo();
            info.creeps = creeps;
            info.weapon = this;
            info.tower = tower;
            info.amount = damageDone;
            OnHit(info);
        }
    }

    double dealDamage(Creep creep, double multiplier = 1)
    {
        double calcDamage = damage.Value;
        double tempCritChance = critChance.Value;
        double critMultiplier = 1;
        int chance = 0;
        while (tempCritChance > 75)
        {
            chance = UnityEngine.Random.Range(0, 3);
            if (chance == 3)
            {
                critMultiplier *= critDamage.Value;
            }
            tempCritChance -= 75;
        }

        if (tempCritChance > 0)
        {
            chance = UnityEngine.Random.Range(0, 99);
            if (chance < tempCritChance)
            {
                critMultiplier *= critDamage.Value;
            }
        }

        calcDamage *= critMultiplier;
        //if (critMultiplier > 1) Debug.Log("OMG WE HAVE A CRIT!");
        //Debug.Log(calcDamage * critMultiplier);
        creep.GetComponent<Creep>().takeDamage(this, calcDamage*multiplier);
        return calcDamage;
    }

    void projectileDestroy(Projectile proj)
    {
        proj.OnProjectileLand -= onHitingTarget;
        proj.OnProjectileDestroy -= projectileDestroy;
    }

    public void incKillCounter()
    {
        kills++;
    }

    public void incDamageCounter(double amount)
    {
        damageDone += amount;
    }

    public string generateToolTip()
    {
        string result;

        result = "<b>" + Name
            + "</b> \n <b>Damage:</b> " + Math.Round(damage.Value, 1)
            + " \n <b>Range:</b> " + range
            + " \n <b>Attack Speed:</b> " + Math.Round(attackSpeed.Value, 1)
            + " \n <b>Crit Chance:</b> " + Math.Round(critChance.Value, 1);

        if (sourceType == SourceType.ammo)
        {
            result += " \n <b>Ammo:</b> " + ammoInStock + "/" + Math.Round(ammoInStockMax.Value);
        } else
        {

        }

        result += " \n <b>Reload Speed:</b> " + reloadSpeed.Value
            + " \n \n <b>Kills:</b> " + kills
            + " \n <b>Damage Done:</b> " + Math.Round(damageDone, 1);
        return result;
    }
}

﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class Creep : MapObject {

    public GameManager gameManager;

    public SpriteRenderer sprite;
    Animator animator;

    public List<Hex> waypoints;
    public Hex destination;
    public Hex nextHex;

    public Hex prevLocation;

    public Vector3 destinationCords;
    public bool reachCenterOfHex = true; //becouse we spawn creeps in center of hex

    public Stat hpMax;
    public double hpCurrent = 100;

    public delegate void OnEnterHex(EventInfo info);
    public static event OnEnterHex onEnteringHex;

    public delegate void OnExitHex(EventInfo info);
    public event OnExitHex onExitingHex;

    public delegate void OnDying(EventInfo info);
    public event OnDying onDying;

    public delegate void OnTakingHit(EventInfo info);
    public event OnTakingHit onTakingHit;

    public delegate void OnTakingCrit(EventInfo info);
    public event OnTakingCrit onTakingCrit;

    public delegate void OnCreepDying(EventInfo info);
    public static event OnCreepDying onCreepDying;

    public delegate void OnCreepDestroy(EventInfo info);
    public event OnCreepDestroy onCreepDestroy;

    public delegate void OnClick(EventInfo info);
    public event OnClick onClick;



    bool needUpdateSelectedInfo = false;

    bool dead = false;

    public Stat grantExp;
    public Stat grantGold;   

    public enum _facing {
        left,
        leftTop,
        leftBottom,
        right,
        rightTop,
        rightBottom
    };

    _facing facing = _facing.right;

    public enum creepState
    {
        idle,
        walk,
        cast,
        finishCasting,
        dying
    };

    public creepState state = creepState.walk;

    public Stat moveSpeed; //STAT
    [Inspect]
    public Stat slowFactor;
    Vector3 actualSpeed;
    double moveSpeedAdjustment = 1;

    Messenger messenger;

    protected override void onMouseEnter()
    {
        base.onMouseEnter();
        if (gameManager.selectedCreep == this)
        {
            markAsSelected();
        }
        else
        {
            sprite.color = StaticColors.colorNormalHover;
        }
    }

    protected override void onMouseExit()
    {
        base.onMouseExit();
        if (Input.GetMouseButton(0)) return;
        if (gameManager.selectedCreep)
        {
            markAsSelected();
        }
        else
        {
            markAsDeselected();
        }
    }

    protected override void clickHandler()
    {
        base.clickHandler();

        if (onClick != null)
        {
            EventInfo inf = new EventInfo();
            inf.creep = this;
            onClick(inf);
        }
        //gameManager.selectCreep(this);
    }

    void markAsSelected ()
    {
        sprite.color = StaticColors.colorSelected;
    }

    void markAsDeselected()
    {
        sprite.color = StaticColors.colorNormal;
    }

    public float distanceToEnd
    {
        get
        {
            float result = 0;
            
            //Adding distance to next hex
            if (reachCenterOfHex)
            {
                float d1 = Vector3.Distance(locationHex.transform.position, nextHex.transform.position);
                float d2 = Vector3.Distance(transform.position, nextHex.transform.position);
                result += d2 / d1;
            } else
            {
                float d1 = Vector3.Distance(prevLocation.transform.position, nextHex.transform.position);
                float d2 = Vector3.Distance(transform.position, nextHex.transform.position);
                result += d2 / d1;
            }

            
            //Adding distance to finish path (if needed)
            if (nextHex != destination)
            {
                result += nextHex.destinations[destination].Step - 0.5f;
            }
            //Debug.Log("Second part of result, destination to path end: " + result);
            //Adding distances of paths left:
            if (waypoints.Count>1)
            {
                /*int i = 1;
                {
                    result += waypoints[i-1].destinations[waypoints[i]].Step;
                    Debug.Log("Third part of result, adding one of the waypoints: " + result);
                    i++;
                } while (i < waypoints.Count) ;*/
                for (int i = 0; i < waypoints.Count-1; i++)
                {
                    result += waypoints[i].destinations[waypoints[i + 1]].Step;
                    //Debug.Log("Third part of result, adding one of the waypoints: " + result);
                }
                //for (int i = 1; i < waypoints.Count-1)
            }
            

            return result;
        }
    }

    public void ini(Hex loc, Hex dest, double hp, double exp)
    {
        List<Hex> waypoints = new List<Hex>();
        waypoints.Add(dest);
        ini(loc, waypoints, hp, exp);
    }

    public void ini(Hex loc, List<Hex> waypoints, double hp, double exp)
    {
        animator = sprite.GetComponent<Animator>();

        //STATS
        hpMax = new Stat(hp);
        hpCurrent = hpMax.Value;
        grantExp = new Stat(exp);
        grantGold = new Stat(1);
        moveSpeed = new Stat(1);
        slowFactor = new Stat(77);
        slowFactor.OnChange += updateSlowFactor;


        locationHex = loc;

        if (waypoints.Count > 1)
        {
            this.waypoints = new List<Hex>(waypoints);
        }

        destination = waypoints[0];


        nextHex = loc.destinations[destination].NextStep;
        nextHex.creepsGoingToHex.Add(this);
        enteringHex(locationHex);
        updateCords();

        onEffectAdd += requestInfoUpdate;
        onEffectRemove += requestInfoUpdate;
    }
    
    public void moveTo(Hex hex, List<Hex> waypoints)
    {
        this.waypoints = new List<Hex>(waypoints);
        moveTo(hex, waypoints[0]);
    }

    public void moveTo(Hex hex, Hex dest)
    {
        leavingHex(locationHex);

        locationHex = hex;

        enteringHex(locationHex);

        transform.position = hex.transform.position;
        

        destination = dest;
        nextHex = locationHex.destinations[destination].NextStep;

        nextHex.creepsGoingToHex.Add(this);
        
        updateCords();
    }

    // Use this for initialization
    new void Start() {
        base.Start();
        messenger = GetComponent<Messenger>();
    }

    

    new void FixedUpdate() {
        base.FixedUpdate();

        switch (state) {
            case creepState.walk:
                walkRoutine();
                break;

            case creepState.idle:
                break;

            case creepState.cast:
                break;

            case creepState.finishCasting:
                break;

            case creepState.dying:
                break;
        }

        if (needUpdateSelectedInfo)
        {
            EventInfo inf = new EventInfo();
            inf.creep = this;
            updateInfoEventStarter(inf);
        }

        needUpdateSelectedInfo = false;

    }

    public void requestInfoUpdate()
    {
        needUpdateSelectedInfo = true;
    }


    void walkRoutine() {
        //Lets check if we reach our next destination
        if (Vector3.Distance(destinationCords, transform.position) <= 0.0005) {
            if (reachCenterOfHex) {
                //Now we at border
                reachCenterOfHex = false;
                //Switching location
                prevLocation = locationHex;
                locationHex = nextHex;
                leavingHex(prevLocation);
                enteringHex(locationHex);
            } else {
                //We get to the center
                reachCenterOfHex = true;

                if (nextHex == destination && locationHex == destination) {
                    //Here we reach location;
                    

                    if (waypoints.Count > 1) //We still have points to move too
                    {
                        waypoints.RemoveAt(0);
                        destination = waypoints[0];

                        nextHex = locationHex.destinations[destination].NextStep;
                        nextHex.creepsGoingToHex.Add(this);
                    } else //we reach last point
                    {
                        //state = creepState.idle;
                        gameManager.reachTheEnd(this);
                    }
                    

                } else {

                    nextHex = locationHex.destinations[destination].NextStep;
                    nextHex.creepsGoingToHex.Add(this);
                }
            }
            updateCords();

        }

        walk(actualSpeed);
    }

    void walk(Vector3 cords) {
        Vector3 incr = new Vector3();
        incr = cords * ((float)moveSpeed.Value * (float)moveSpeedAdjustment * 2);
        if (Vector3.Distance(destinationCords, transform.position) > incr.magnitude)
        {
            transform.position += incr;
        } else
        {
            transform.position = destinationCords;
        }
    }

    void updateCords() {
        //If we dont have cords:

        if (reachCenterOfHex) {
            destinationCords = locationHex.transform.position + (nextHex.transform.position - locationHex.transform.position) * 0.5f;
            //Debug.Log ("Here we are - begining where we expect - " + destinationCords);

        } else {
            //If we reach border of hex
            destinationCords = nextHex.transform.position;

        }

               
        //calculate new speed
        actualSpeed = (destinationCords - transform.position) * Time.fixedDeltaTime;


        if (locationHex.neighbors["RightTop"] == nextHex) {
            facing = _facing.rightTop;
        }
        if (locationHex.neighbors["Right"] == nextHex) {
            facing = _facing.right;
        }
        if (locationHex.neighbors["RightBottom"] == nextHex) {
            facing = _facing.rightBottom;
        }
        if (locationHex.neighbors["LeftTop"] == nextHex) {
            facing = _facing.leftTop;
        }
        if (locationHex.neighbors["Left"] == nextHex) {
            facing = _facing.left;
        }
        if (locationHex.neighbors["LeftBottom"] == nextHex) {
            facing = _facing.leftBottom;
        }
        changeFacing();
    }

    public void updateSlowFactor(EventInfo info)
    {
        if (slowFactor.Value > 0)
        {
            if (slowFactor.Value >= 100)
            {
                moveSpeedAdjustment = (100 / (2 * slowFactor.Value / 100)) /100;
            }
            else
            {
                moveSpeedAdjustment = (100 - (100 * slowFactor.Value / 200)) / 100;
            }
        } else
        {
            moveSpeedAdjustment = 1;
        }
        //calculate new speed
        //Debug.Log(moveSpeedAdjustment + " - " + slowFactor.Value);
    }

    public void goBackToCenter() {
        reachCenterOfHex = false;
        nextHex = locationHex;
        updateCords();
    }

    void changeFacing() {
        animator.speed = 0.3f;
        animator.ResetTrigger("Top");
        animator.ResetTrigger("Straight");
        animator.ResetTrigger("Bottom");
        switch (facing) {
            case _facing.leftTop:
                animator.SetTrigger("Top");
                sprite.flipX = true;
                break;

            case _facing.left:
                animator.SetTrigger("Straight");
                sprite.flipX = true;
                break;

            case _facing.leftBottom:
                animator.SetTrigger("Bottom");
                sprite.flipX = true;
                break;

            case _facing.rightTop:
                animator.SetTrigger("Top");
                sprite.flipX = false;
                break;

            case _facing.right:
                animator.SetTrigger("Straight");
                sprite.flipX = false;
                break;

            case _facing.rightBottom:
                animator.SetTrigger("Bottom");
                sprite.flipX = false;
                break;
        }
    }

    


    void enteringHex(Hex hex) {
        EventInfo info = new EventInfo();
        info.creep = this;
        info.hex = hex;
        hex.creepEnteredHex(info);

        if (onEnteringHex != null) {
            
            onEnteringHex(info);
        }
    }

    void leavingHex(Hex hex) {

        
        if (onExitingHex != null) {
            EventInfo info = new EventInfo();
            info.creep = this;
            info.hex = hex;
            onExitingHex(info);
        }
    }

    //public void takeDamage(Weapon source, double dmg)
    public void takeDamage(Weapon source, double dmg)
    {
        source.incDamageCounter(dmg);
        
        EventInfo info = new EventInfo();
        info.creep = this;
        info.weapon = source;
        info.tower = source.tower;
        info.amount = dmg;

        handleDamage(info, dmg);
        messenger.sendMessage(Color.red, System.Math.Round(dmg, 1).ToString());
    }

    public void takeDamage(Weapon source, double dmg, bool crit)
    {
        if (crit)
        {
            EventInfo info = new EventInfo();
            info.creep = this;
            info.weapon = source;
            info.tower = source.tower;
            info.amount = dmg;

            if (onTakingCrit != null)
            {
                onTakingCrit(info);
            }
        }

        takeDamage(source, dmg);
    }

        public void takeDamage(Ability source, double dmg)
    {
        source.incDamageCounter(dmg);
        
        EventInfo info = new EventInfo();
        info.creep = this;
        info.ability = source;
        info.tower = source.tower;
        info.amount = dmg;

        handleDamage(info, dmg);
        messenger.sendMessage(Color.blue, System.Math.Round(dmg, 1).ToString());
    }

    void handleDamage(EventInfo info, double dmg)
    {
        hpCurrent -= dmg;
        float exp;
        if (dmg > hpCurrent)
        {
            exp = Mathf.Abs((float)(hpCurrent / hpMax.Value * grantExp.Value));
        }
        else
        {
            exp = Mathf.Abs((float)(dmg / hpMax.Value * grantExp.Value));
        }

        info.tower.giveExp(exp);
        info.tower.incDamageCounter(dmg);
        info.hex = locationHex;
        if (hpCurrent <= 0 && !dead)
        {
            dead = true;
            die(info);
        }
    }

    /// <summary>
    ///  Handle creep death
    /// </summary>
    /// <param name="incInfo">Expect EventInfo with Ability OR Weapon info. If object will contain both will NOT work properly.</param>
    public void die(EventInfo incInfo) {
        //Debug.Log("Creep ID " + id + " died, located in hex " + incInfo.hex.id );
        state = creepState.dying;
        sprite.GetComponent<Animator>().SetTrigger("Dying");
        startDecay();
        messenger.sendMessage(Color.yellow, Mathf.RoundToInt((float)grantGold.Value).ToString());
        Inventory.Gold.amount += (Mathf.RoundToInt((float)grantGold.Value));
        if (nextHex.creepsGoingToHex.Contains(this))
        {
            nextHex.creepsGoingToHex.Remove(this);
        }

        incInfo.tower.incKillCounter();
        if (incInfo.weapon) incInfo.weapon.incKillCounter();
        if (incInfo.ability) incInfo.ability.incKillCounter();

        EventInfo info = new EventInfo();
        info.hex = locationHex;
        info.creep = this;

        if (onDying != null)
        {
            onDying(info);
        }

        if (onCreepDying != null)
        {
            onCreepDying(info);
        }
    }

    public void startDecay()
    {
        StartCoroutine("decay");
    }

    IEnumerator decay()
    {

        yield return new WaitForSeconds(3);
        terminate();
    }

    public void terminate()
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if (onCreepDestroy != null) {
            EventInfo info = new EventInfo();
            info.creep = this;

            onCreepDestroy(info);
        }
        
        onDying = null;
        onEnteringHex = null;
        onExitingHex = null;
        onTakingHit = null;
        onTakingCrit = null;
        onCreepDestroy = null;
        onClick = null;
        onEffectAdd -= requestInfoUpdate;
        onEffectRemove -= requestInfoUpdate;
    }

    public static int CompareCreepsByDistanceToDestination(Creep creep1, Creep creep2)
    {
        if (creep1.distanceToEnd < creep2.distanceToEnd)
        {
            return 1;
        } else if (creep1.distanceToEnd == creep2.distanceToEnd)
        {
            return 0;
        }
        {
            return -1;
        }
    }
        
}

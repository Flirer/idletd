﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class towerSelectUi : MonoBehaviour {

    public Button moveBtn;
    public Button lvlUpBtn;
    public Button morphBtn;
    public Button delBtn;

    public Tower tower;

    // Use this for initialization
    void Start () {
        moveBtn = transform.Find("MoveBtn").GetComponent<Button>();
        lvlUpBtn = transform.Find("UpgraidBtn").GetComponent<Button>();
        morphBtn = transform.Find("MorphBtn").GetComponent<Button>();
        delBtn = transform.Find("DeleteBtn").GetComponent<Button>();

        tower = transform.parent.GetComponent<Tower>();

        delBtn.onClick.AddListener(GameManager.Instance.removeSelectedTower);
        moveBtn.onClick.AddListener(GameManager.Instance.beginMoveTower);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

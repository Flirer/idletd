﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class TowerSubModel : MonoBehaviour {

    public delegate void Animation_attackEvent();
    public Animation_attackEvent attackEvent;

    [Inspect]
    [Group("Animations Available")]
    public bool attackAnimationAvailable = false;
    [Inspect]
    [Group("Animations Available")]
    public bool countiniuseAttackAnimationAvailable = false;
    [Inspect]
    [Group("Animations Available")]
    public bool reloadAnimationAvailable = false;

    [Inspect]
    public List<Transform> points;

    Animator animator;

	// Use this for initialization
	void Awake () {
        animator = GetComponent<Animator>();
	}

    //This function triggered by Tower to lunch animation
    public void attackAnimationStart()
    {
        //Debug.Log("Attack in submodel");
        animator.SetTrigger("Attack");
    }

    //This event is triggered FROM animation
    public void attackAnimationEvent()
    {
        //Debug.Log("Attack event lunching");
        if (attackEvent != null)
        {
            attackEvent();
        }
    }

    public void reloadAnimationStart()
    {
        animator.SetTrigger("Reload");
    }

    public void beginAttackAnimationStart()
    {
        animator.SetTrigger("BeginAttack");
    }

    public void endAttackAnimationStart()
    {
        animator.SetTrigger("EndAttack");
    }

    public void iniAttackAnimation(Weapon weapon)
    {
        weapon.attackAnimation += attackAnimationStart;
        attackEvent = weapon.fire;
    }
}

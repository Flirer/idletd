﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInspector;
using System;

[AdvancedInspector]
[Serializable]
public class Rune {

    //TODO: Implent add weapon;
    //TODO: IMplent add ability;


    [NonSerialized]
    public Tower tower;

    public string Name;

    /// <summary>
    /// If o tower, in wich this rune inserted
    /// </summary>
    [Inspect]
    public int towerPrefabId = -1;

    /// <summary>
    /// Id of slot in tower
    /// </summary>
    /// [Inspect]
    public int towerSlotId = -1;

    [Inspect]
    public List<RuneStat> bonusStats;

    public Rune ()
    {
        bonusStats = new List<RuneStat>();
    }

    public void initialize(Tower t)
    {
        Debug.Log("Activating rune for " + t.name);
        tower = t;
        foreach (RuneStat runeStat in bonusStats)
        {
            activateStat(runeStat);
        }
    }

    public void activateStat(RuneStat stat)
    {
        stat.stat.clearIncomingProxyStats();
        Stat s = Stat.getTowerStatByType(tower, stat.statType);
        s.consume(stat.stat);
    }

    public void addStat (RuneStat runeStat)
    {
        bonusStats.Add(runeStat);
    }
}

[Serializable]
public class RuneStat
{
    public Stat.TowerStat statType;
    public Stat stat;
}
﻿using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class Tower : MapObject {

    GameObject selectUi;

    [Group("Visuals", Expandable = true)]
    [Inspect]
    public TowerModel model;

    [Inspect(1)]
    public GameObject placementDecoy;

    [Inspect(1)]
    public string towerName;
    [Inspect(1)]
    public string towerDescription;
    [Inspect(1)]
    public Sprite icon;

    [Inspect(1)]
    public List<Tower> morph;

    [Inspect]
    public int researchParts = 1;

    //public Weapon weapon;
    [Inspect(3)]
    public List<Weapon> weapons;

    [Inspect(1)]
    public refData<int> level;
    [Inspect(InspectorLevel.Debug)]
    public refData<double> expirience;
    [Inspect(InspectorLevel.Debug)]
    public double expirienceToLevelUp = 0;
    [Inspect(InspectorLevel.Debug)]
    public double baseExperience = 20;
    [Inspect(InspectorLevel.Debug)]
    public refData<int> upgraidLevel;
    [Inspect(1)]
    public int buildMax;
    public refData<int> builded;

    [Group("Runes", Expandable = true)]
    [Inspect(5)]
    public int runeSlotsCount;
    [Group("Runes")]
    [Inspect(5)]
    public Rune[] runes;

    public delegate void OnLevelUp(EventInfo info);
    public event OnLevelUp onLevelUp;

    public delegate void OnUpgraidLevelUp(EventInfo info);
    public event OnUpgraidLevelUp onUpgraidLevelUp;

    public delegate void OnAttack(EventInfo info);
    public event OnAttack onAttack;

    public delegate void OnHit(EventInfo info);
    public event OnHit onHit;

    public delegate void OnCritHit(EventInfo info);
    public event OnCritHit onCritHit;

    public delegate void OnKill(EventInfo info);
    public event OnKill onKill;

    public delegate void OnReload(EventInfo info);
    public event OnReload onReload;

    public delegate void OnWeaponAdd(EventInfo info);
    public event OnWeaponAdd onWeaponAdd;

    public delegate void OnWeaponRemove(EventInfo info);
    public event OnWeaponRemove onWeaponRemove;

    public delegate void OnCursorEnter(EventInfo info);
    public event OnCursorEnter onCursorEnter;

    public delegate void OnCursorExit(EventInfo info);
    public event OnCursorExit onCursorExit;

    public delegate void OnClick(EventInfo info);
    public event OnClick onClick;

    public delegate void OnMoveBegin(EventInfo info);
    public event OnMoveBegin onMoveBegin;

    public delegate void OnMove(EventInfo info);
    public event OnMove onMove;

    public delegate void OnMoveEnd(EventInfo info);
    public event OnMoveEnd onMoveEnd;

    public delegate void OnBeingRemoved(EventInfo info);
    public event OnBeingRemoved onBeingRemoved;

    public delegate void OnBeingSelected();
    public event OnBeingSelected onBeingSelected;

    public delegate void OnBeingDeselected();
    public event OnBeingDeselected onBeingDeselected;

    //RESOURCE


    [Group("Resource Cost:", Expandable = true)]
    [Inspect(2)]
    public int goldCost = 0;

    [Group("Resource Cost:")]
    [Inspect(2)]
    public double researchCost = 0;

    [Group("Resource Cost:")]
    [Inspect(2)]
    public int SupplyCost;

    [Group("Resource Cost:")]
    [Inspect(2)]
    public int SupplyGive;


    [Group("Stats", 5, Expandable = true)]
    [Inspect]
    public Stat damage;
    [Group("Stats")]
    [Inspect]
    public Stat attackSpeed;
    [Group("Stats")]
    [Inspect]
    public Stat reloadSpeed;
    [Group("Stats")]
    [Inspect]
    public Stat critChance;
    [Group("Stats")]
    [Inspect]
    public Stat critDamage;
    [Group("Stats")]
    [Inspect]
    public Stat maxAmmo;

    //STATISTICS:

    [Inspect(4)]
    [Group("Statistics:", 1, Expandable = true)]
    public int kills = 0;
    [Inspect(4)]
    [Group("Statistics:")]
    public double damageDone = 0;

    bool needUpdateSelectedInfo = false;

    //VISUALS
    

    // Use this for initialization
    new void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	new void FixedUpdate() {
        base.FixedUpdate();

        if (needUpdateSelectedInfo)
        {
            EventInfo inf = new EventInfo();
            inf.tower = this;
            updateInfoEventStarter(inf);
        }

        needUpdateSelectedInfo = false;
    }

	public void build (Hex loc) {
        locationHex = loc;
        if (model != null)
        {
            if (model.buildAnimation)
            {
                model.buildCompleteEvent += ini;
                model.onRemoveAnimationFinish += removeTower;
            } else
            {
                ini();
            }
        } else
        {
            ini();
        }

        //Sorting layer
        int layer = 1000 - (int)locationHex.transform.position.y;

        if (model)
        {
            if (model.GetComponent<SpriteRenderer>() != null)
            {
                model.GetComponent<SpriteRenderer>().sortingOrder += layer;
            }
            foreach (Transform child in model.transform)
            {
                if (child.GetComponent<SpriteRenderer>() != null)
                {
                    child.GetComponent<SpriteRenderer>().sortingOrder += layer;
                }
                foreach (Transform _child in child)
                {
                    if (_child.GetComponent<SpriteRenderer>() != null)
                    {
                        _child.GetComponent<SpriteRenderer>().sortingOrder += layer;
                    }
                }
            }
        }
    }

    public void upgraid()
    {
        Inventory.Gold.amount -= goldCost * (upgraidLevel.data + 1) * (Math.Pow(1.5, upgraidLevel.data));
        upgraidLevel.data++;
        if (onUpgraidLevelUp != null)
        {
            EventInfo info = new EventInfo();
            info.tower = this;
            onUpgraidLevelUp(info);
        }
        
    }

    private void ini()
    {
        if (!SaveLoad.playerData.towerStatisticData.ContainsKey(prefabId))
            SaveLoad.playerData.towerStatisticData.Add(prefabId, new TowerStaticData());

        selectUi = transform.Find("SelectUi").gameObject;


        level = SaveLoad.playerData.towerStatisticData[prefabId].level;
        expirience = SaveLoad.playerData.towerStatisticData[prefabId].expirience;
        upgraidLevel = SaveLoad.playerData.towerStatisticData[prefabId].upgraidLevel;
        builded = SaveLoad.playerData.towerStatisticData[prefabId].numberBuilded;
        expirienceToLevelUp = 0;

        updateNextLevelExperienceReq();

        transform.position = locationHex.transform.position;
        
        locationHex.beingBuildUpon(this);

        damage = new Stat(0);
        attackSpeed = new Stat(0);
        reloadSpeed = new Stat(0);
        critChance = new Stat(0);
        critDamage = new Stat(0);
        maxAmmo = new Stat(0);

        damage.OnChange += requestInfoUpdate;
        attackSpeed.OnChange += requestInfoUpdate;
        reloadSpeed.OnChange += requestInfoUpdate;
        critChance.OnChange += requestInfoUpdate;
        critDamage.OnChange += requestInfoUpdate;
        maxAmmo.OnChange += requestInfoUpdate;

        foreach (Weapon wep in weapons)
        {
            wep.initialize();
            if (wep.primary)
            {
                wep.OnAttack += onAttackIni;
                wep.OnHit += onHitIni;
                wep.OnReload += onReloadIni;
            }

            wep.damage.addProxyStat(damage);
            wep.attackSpeed.addProxyStat(attackSpeed);
            wep.reloadSpeed.addProxyStat(reloadSpeed);
            wep.critChance.addProxyStat(critChance);
            wep.critDamage.addProxyStat(critDamage);
            wep.ammoInStockMax.addProxyStat(maxAmmo);

        }

        runes = new Rune[runeSlotsCount];

        foreach (Rune rune in SaveLoad.playerData.runes)
        {
            if (rune.towerPrefabId == prefabId)
            {
                runes[rune.towerSlotId] = rune;
            }
        }

        foreach (Rune rune in runes)
        {
            if (rune != null)
            {
                rune.initialize(this);
            }
        }

        foreach (Ability abil in abilities)
        {
            abil.initialize(this);
        }

        onEffectAdd += requestInfoUpdate;
        onEffectRemove += requestInfoUpdate;
    }

    void onAttackIni(EventInfo info)
    {
        if (onAttack != null) onAttack(info);
    }

    void onHitIni(EventInfo info)
    {
        if (onHit != null) onHit(info);
    }

    void onReloadIni(EventInfo info)
    {
        if (onReload != null) onReload(info);
    }



    public void giveExp(double exp)
    {
        expirience.data += exp;
        if (expirience.data >= expirienceToLevelUp)
        {
            if (Double.IsInfinity(expirience.data) || Double.IsNaN(expirience.data))
            {
                return;
            }
            while (expirience.data >= expirienceToLevelUp)
            {
                level.data++;
                expirience.data -= expirienceToLevelUp;
                updateNextLevelExperienceReq();
            }
            if (onLevelUp != null)
            {
                EventInfo inf = new EventInfo();
                inf.tower = this;
                onLevelUp(inf);
            }
        }
        
        EventInfo info = new EventInfo();
        requestInfoUpdate(info);
    }

    void requestInfoUpdate(EventInfo inf)
    {
        requestInfoUpdate();
    }

    void requestInfoUpdate()
    {
        needUpdateSelectedInfo = true;
    }

    public void updateNextLevelExperienceReq()
    {
        expirienceToLevelUp = baseExperience * (level.data*100000);
    }


    public int getRange()
    {
        //TODO: Return more complex range(coverage) then just number
        int result = 0;
        foreach (Weapon wep in weapons)
        {
            if (wep.range > result)
            {
                result = wep.range;
            }
        }
        return result;
    }

    public List<Weapon> getWeapons()
    {
        return weapons;
    }

    //TODO: Finish adding and removing weapons
    public void addWeapon()
    {

    }

    public void removeWeapon()
    {

    }

    //STATISTICS GOES HERE

    public void incKillCounter()
    {
        kills++;
    }

    public void incDamageCounter(double amount)
    {
        damageDone += amount;
    }

    protected override void onMouseEnter()
    {
        base.onMouseEnter();
        if (Input.GetMouseButton(0)) return;
        locationHex.markAsSelected();
        if (onCursorEnter != null)
        {
            EventInfo info = new EventInfo();
            info.tower = this;
            onCursorEnter(info);
        }
    }


    protected override void onMouseExit()
    {
        base.onMouseExit();
        locationHex.markAsDeselected();

        if (onCursorExit != null)
        {
            EventInfo info = new EventInfo();
            info.tower = this;
            onCursorExit(info);
        }
    }

    protected override void clickHandler()
    {
        base.clickHandler();

        if (onClick != null)
        {
            EventInfo info = new EventInfo();
            info.hex = locationHex;
            info.tower = this;
            onClick(info);
        }
        //gameManager.selectTower(this);
    }

    public void selectHandler()
    {
        selectUi.SetActive(true);
        if (onBeingSelected != null) onBeingSelected();
    }

    public void deselectHandler()
    {
        selectUi.SetActive(false);
        if (onBeingDeselected != null) onBeingDeselected();
    }

    void markAsSelected()
    {
        locationHex.markAsSelected();
    }

    void markAsDeselected()
    {
        locationHex.markAsDeselected();
    }

    public string generateSelectInfoTip()
    {
        string result;

        result = "<b>" + towerName + "</b> \n <b>Level:</b> " + level.data
            + " \n <b>Upgraid:</b> " + upgraidLevel.data + " \n <b>Expirience:</b> "
            + expirience.data + "/" + expirienceToLevelUp + " \n <b>Damage Done</b> "
            + damageDone + " \n <b>Kills</b> " + kills;

        return result;
    }

    public string generateToolTip()
    {
        string result;

        result = "<b>" + towerName + "</b> \n"
            + towerDescription + "\n" 
            + " Supply Cost: " + SupplyCost;

        return result;
    }

    public string generateResearchToolTip()
    {
        string result;

        result = "<b>" + towerName + "</b> \n"
            + towerDescription + "\n"
            + " Research cost: " + researchCost;

        return result;
    }

    private void OnDestroy()
    {
        onLevelUp = null;
        onUpgraidLevelUp = null;
        onAttack = null;
        onHit = null;
        onCritHit = null;
        onKill = null;
        onReload = null;
        onWeaponAdd = null;
        onWeaponRemove = null;
        onCursorEnter = null;
        onCursorExit = null;
        onClick = null;
        onMove = null;
        onBeingSelected = null;
        onBeingDeselected = null;
    }

    public void removeTowerIni()
    {
        model.removeTowerAnimationLunch();
        EventInfo info = new EventInfo();
        info.tower = this;
        if (onBeingRemoved != null) onBeingRemoved(info);
    }

    void removeTower()
    {
        Destroy(gameObject);
    }

    public void initializeMove()
    {
        EventInfo info = new EventInfo();
        info.tower = this;
        if (onMoveBegin != null) onMoveBegin(info);
    }

    public void whileMove()
    {
        EventInfo info = new EventInfo();
        info.tower = this;
        if (onMove != null) onMove(info);
    }

    public void endMove()
    {
        EventInfo info = new EventInfo();
        info.tower = this;
        if (onMoveEnd != null) onMoveEnd(info);
    }

}
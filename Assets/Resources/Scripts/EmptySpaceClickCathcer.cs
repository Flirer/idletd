﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EmptySpaceClickCathcer : MonoBehaviour, IPointerClickHandler {
    public void OnPointerClick(PointerEventData eventData)
    {
        GameManager.clickOnEmptySpace();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    Camera cam;
    Transform camTransform;
    Vector3 lastCursorPosition;
    Vector3 cursorPosition;

    Vector3 newCameraPosition;
    public float speed = 6;
    public float distanceSpeed = 10;
    float distanceForMaxSpeed = 25;
    float currentDistance;

    float topBorder = 0;
    float bottomBorder = 0;
    float leftBorder = 0;
    float rightBorder = 0;

    List<float> zoomLevel = new List<float>();
    int currentZoomLevel = 2;

    bool aplicationFocused = true;

    PixelPerfectCamera pixelPerfectCamera;
	// Use this for initialization
	void Start () {
        pixelPerfectCamera = GetComponent<PixelPerfectCamera>();
        cam = GetComponent<Camera>();
        camTransform = cam.transform;
        newCameraPosition = camTransform.position;

        zoomLevel.Add(0.8f);
        zoomLevel.Add(1.2f);
        zoomLevel.Add(2.3f);
        zoomLevel.Add(2.5f);

        updateBorders();

        TerritoryManager.onTerritoryChange += updateBorders;
    }

    private void Update()
    {
        cursorPosition = Input.mousePosition;
        if (Input.GetMouseButton(0) && aplicationFocused) {

            if (lastCursorPosition != null)
            {
                newCameraPosition += Vector3.Normalize(lastCursorPosition - cursorPosition)* distanceSpeed;
                if (newCameraPosition.y > topBorder) newCameraPosition.y = topBorder;
                if (newCameraPosition.y < bottomBorder) newCameraPosition.y = bottomBorder;
                if (newCameraPosition.x < leftBorder) newCameraPosition.x = leftBorder;
                if (newCameraPosition.x > rightBorder) newCameraPosition.x = rightBorder;
            }
        }

        if (!aplicationFocused) aplicationFocused = true;

        lastCursorPosition = cursorPosition;
        if (camTransform.position != newCameraPosition)
        {
            currentDistance = Vector3.Distance(camTransform.position, newCameraPosition);
            float distanceFactor; 
            if (currentDistance >= distanceForMaxSpeed)
            {
                distanceFactor = 1;
            } else
            {
                distanceFactor = currentDistance / distanceForMaxSpeed;
            }
            camTransform.position = Vector3.Lerp(camTransform.position, newCameraPosition, distanceFactor*Time.deltaTime*speed* zoomLevel[currentZoomLevel]);
        }
        if (Input.mouseScrollDelta.y < 0)
        {

            if (currentZoomLevel < zoomLevel.Count-1)
            {
                currentZoomLevel++;
                pixelPerfectCamera.targetCameraHalfHeight = zoomLevel[currentZoomLevel];
            }

            pixelPerfectCamera.adjustCameraFOV();
        } else 
        if (Input.mouseScrollDelta.y > 0)
        {
            if (currentZoomLevel > 0)
            {
                currentZoomLevel--;
                pixelPerfectCamera.targetCameraHalfHeight = zoomLevel[currentZoomLevel];
            }
            pixelPerfectCamera.adjustCameraFOV();
        }
    }

    void updateBorders()
    {
        bool first = true;

        foreach (KeyValuePair <int, Hex> pair in GameManager.Instance.hexes)
        {
            if (first)
            {
                topBorder = pair.Value.transform.position.y;
                bottomBorder = pair.Value.transform.position.y;
                leftBorder = pair.Value.transform.position.x;
                rightBorder = pair.Value.transform.position.x;

                first = false;
            } else
            {
                if (topBorder < pair.Value.transform.position.y) topBorder = pair.Value.transform.position.y;
                if (bottomBorder > pair.Value.transform.position.y) bottomBorder = pair.Value.transform.position.y;
                if (leftBorder > pair.Value.transform.position.x) leftBorder = pair.Value.transform.position.x;
                if (rightBorder < pair.Value.transform.position.x) rightBorder = pair.Value.transform.position.x;
            }
        }
    }

    void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus)
        aplicationFocused = false;
    }

}

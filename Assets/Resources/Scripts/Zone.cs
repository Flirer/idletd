﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Zone {

    public List<TowerSavedLocationData> towers;
    public int LastWaveBeaten;
    public double income;

    //WAYPOINTS

	public Zone()
    {
        towers = new List<TowerSavedLocationData>();
    }

    public void removeTower(Tower tower)
    {
        foreach (TowerSavedLocationData data in towers)
        {
            if (data.tower == tower)
            {
                towers.Remove(data);
                break;
            }
        }
    }
}

[System.Serializable]
public class TowerSavedLocationData
{
    public int prefabId;
    public int hex;

    [System.NonSerialized]
    public Tower tower;

    public TowerSavedLocationData(Tower tower)
    {

        initialize(tower);
        updateData();
    }

    public void initialize(Tower tower)
    {
        this.tower = tower;
        this.tower.onUpgraidLevelUp += updateData;
        this.tower.onLevelUp += updateData;
    }

    public void updateData(EventInfo info = null)
    {
        prefabId = tower.prefabId;
        hex = tower.locationHex.id;

        SaveLoad.reqSave();
    }
}

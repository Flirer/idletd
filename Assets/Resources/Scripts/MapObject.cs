﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

[AdvancedInspector]
public class MapObject : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {

    public delegate void OnUpdatedUiInfo(EventInfo info);
    public event OnUpdatedUiInfo onInfoUpdate;

    public delegate void OnEffectAdd();
    public event OnEffectAdd onEffectAdd;

    public delegate void OnEffectRemove();
    public event OnEffectRemove onEffectRemove;

    public delegate void OnCreate(MapObject mapObject);
    public static event OnCreate onCreate;

    public Hex locationHex;
    public int id;

    public int prefabId;

    bool beingClickedOn = false;
    bool mouseOver = false;

    //Effects aplied
    [SerializeField]
    [Inspect(3)]
    public List<Effect> effects;
    [SerializeField]
    List<Effect> effectsToRemove;

    //Abilities
    [SerializeField]
    [Inspect(3)]
    public List<Ability> abilities;

    virtual public void Start()
    {
        effects = new List<Effect>();
        effectsToRemove = new List<Effect>();

        if (onCreate != null) onCreate(this);
    }

    virtual public void FixedUpdate()
    {
        foreach (Effect eff in effects)
        {
            eff.fixedUpdate();
        }

        foreach (Effect eff in effectsToRemove)
        {
            Debug.Log("- - Removing effect from " + id);
            effects.Remove(eff);
        }

        if (onEffectRemove != null && effectsToRemove.Count > 0)
        {
            onEffectRemove();
        }

        effectsToRemove.Clear();
    }

    protected virtual void updateInfoEventStarter(EventInfo info)
    {
        if (onInfoUpdate != null)
        {
            onInfoUpdate(info);
        }
    }

    public virtual void applyEffect(Effect effect)
    {
        effects.Add(effect);
        if (onEffectAdd != null)
        {
            onEffectAdd();
        }
    }

    public virtual void removeEffect(Effect effect)
    {
        effectsToRemove.Add((Effect)effect);
    }

    IEnumerator clearMouseClick()
    {
        yield return new WaitForSeconds(1);
        beingClickedOn = false;
    }

    protected virtual void onMouseDown()
    {
        if (beingClickedOn) StopCoroutine(clearMouseClick());
        StartCoroutine(clearMouseClick());

        beingClickedOn = true;
    }

    protected virtual void onMouseUp()
    {
        if (beingClickedOn && mouseOver) clickHandler();
    }

    protected virtual void onMouseEnter()
    {
        mouseOver = true;
    }

    protected void OnMouseOver()
    {
        mouseOver = true;
    }

    protected virtual void onMouseExit()
    {
        mouseOver = false;
    }

    private void OnDestroy()
    {
        onCreate = null;
        onEffectAdd = null;
        onEffectRemove = null;
        onInfoUpdate = null;
    }

    /// <summary>
    /// ANy new map object need to define what it expects on clicks
    /// </summary>
    protected virtual void clickHandler()
    {

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        onMouseDown();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onMouseUp();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        onMouseEnter();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        onMouseExit();
    }
}

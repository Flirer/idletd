﻿using UnityEngine;

[System.Serializable]
public class Reward {
    public enum Type {Gold, TowerResearch, Structure}

    public Type type;
    public int id;
    public int id2;
    public double amount;

    public void grant()
    {
        if (type == Type.Gold)
        {
            Inventory.Gold.amount += amount;
        } else if (type == Type.TowerResearch)
        {
            Inventory.researchTower(id);
        } else if (type == Type.Structure)
        {
            GameManager.CreateTower(
                TowerCollection.TowerPrefabs[id], 
                GameManager.Instance.hexes[id2]);
        }
    }
}

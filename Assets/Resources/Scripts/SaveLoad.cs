﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using System;
using System.Runtime.Serialization;

public class SaveLoad : MonoBehaviour {

    public delegate void onPlayerLoaded();
    public static event onPlayerLoaded OnPlayerLoaded;

    public static PlayerData playerData;

    public static SaveLoad Instance;

    public static bool firstLoadComplete = false;
    public static bool newGame = false;

    public static bool requestSave = false;

    public bool resetExistingData = false;


    public void Awake()
    {
        firstLoadComplete = false;
        Instance = this;

        GetComponent<TowerCollection>().initialization();
        Load();
    }

    private void FixedUpdate()
    {
        if (requestSave)
        {
            Save();
            requestSave = false;
        }
    }


    public static void waveBeaten(int wave)
    {
        //If currently saved max beaten wave is less then what we recive
        if (GameManager.currentZone.LastWaveBeaten < wave)
        {
            GameManager.currentZone.LastWaveBeaten = wave;
        }
        reqSave();
    }

    public static void newRune(Rune rune)
    {
        playerData.runes.Add(rune);
        reqSave();
    }

    public static void updateSaveData()
    {

        playerData.freeTalantPoints = Talants.freeTalantPoints;

        playerData.supplyLimit = Talants.supplyLimit.level;
        playerData.startingGold = Talants.startingGold.level;
        playerData.startingCrystals = Talants.startingCrystal.level;
        playerData.startingEssence = Talants.startingEssence.level;
        playerData.expirienceBonus = Talants.expirienceBonus.level;

        playerData.mapFlags = MapObjectFlagsHolder.GlobalMapFlags;

        reqSave();
    }

    public static void setTowerToSlot(int slot, int towerId)
    {
        playerData.actionBarSlots[slot] = towerId;
        reqSave();
    }

    public static void reqSave()
    {
        if (!requestSave) requestSave = true;
    }

    static void Save()
    {
        IFormatter formatter = new BinaryFormatter();
        byte[] bytes;
        using (MemoryStream stream = new MemoryStream())
        {
            formatter.Serialize(stream, playerData);
            bytes = stream.ToArray();
        }

        string encodedText = Convert.ToBase64String(bytes);

        PlayerPrefs.SetString("PlayerData", encodedText);
        PlayerPrefs.Save();
        //Debug.Log("Game saved");
    }

    public void Load()
    {
        string encodedText = "";

        if (PlayerPrefs.HasKey("PlayerData"))
        {
            encodedText = PlayerPrefs.GetString("PlayerData");
        }

        if (encodedText != "" && !resetExistingData)
        {
            
            byte[] bytes = Convert.FromBase64String(encodedText);

            IFormatter formatter = new BinaryFormatter();

            MemoryStream stream = new MemoryStream(bytes);

            playerData = (PlayerData)formatter.Deserialize(stream);

            playerData.inventory.initialize();
        }
        else
        {
            playerData = new PlayerData();
            reqSave();
            resetExistingData = false;
            newGame = true;

            playerData.inventory.initialize();
            playerData.inventory.initializeResource();
            Inventory.researchTower(4);
            Inventory.unlockTerritory(0);
        }

        

        if (!firstLoadComplete)
        {
            if (OnPlayerLoaded != null)
            {
                OnPlayerLoaded();
            }
            firstLoadComplete = true;
        }
    }

    /* FILE BASED SAVING SYSTEM
    static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerData.banan");

        bf.Serialize(file, playerData);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerData.banan") && !resetExistingData)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerData.banan", FileMode.Open);

            playerData = (PlayerData)bf.Deserialize(file);
            file.Close();

            //Call only once
            
        } else
        {
            playerData = new PlayerData();
            Save();
            resetExistingData = false;
        }
        Debug.Log("First load complete");
        Debug.Log(playerData.mapFlags);
        if (!firstLoadComplete)
        {
            if (OnPlayerLoaded != null)
            {
                OnPlayerLoaded();
            }
            firstLoadComplete = true;
        }
    }*/
}

[Serializable]
public class PlayerData
{
    public refData<double> credits;
    public refData<double> knowledge;
    public refData<double> supply;

    public int[] actionBarSlots;

    public int freeTalantPoints;

    public int supplyLimit;
    public int startingGold;
    public int startingCrystals;
    public int startingEssence;
    public int expirienceBonus;

    public MapObjectFlagsHolder.MapObjectFlagsDictionary mapFlags;

    public List<Rune> runes;

    public List<Zone> zones;

    public List<bool> territories;

    public Inventory inventory;

    public Dictionary<int, TowerStaticData> towerStatisticData;

    public PlayerData ()
    {
        credits = new refData<double>(16);
        knowledge = new refData<double>(0);
        supply = new refData<double>(8);


        freeTalantPoints = 50;

        supplyLimit = 0;
        startingGold = 0;
        startingCrystals = 0;
        startingEssence = 0;
        expirienceBonus = 0;

        actionBarSlots = new int[10];
        for (int i = 0; i < 10; i++)
        {
            actionBarSlots[i] = -1;
        }

        runes = new List<Rune>();

        zones = new List<Zone>();

        territories = new List<bool>();

        inventory = new Inventory();

        inventory.towers = new List<int>();
        inventory.territories = new List<int>();
        inventory.towerResearchParts = new Dictionary<int, int>();

        towerStatisticData = new Dictionary<int, TowerStaticData>();
    }
}


//This is a way, to create reference point for int, float, or double value.
[Serializable]
public class refData<T> {
    public T data;

    public refData(T data) {
        this.data = data;
    }
}

[Serializable]
public class TowerStaticData
{
    public refData<int> level;
    public refData<int> upgraidLevel;
    public refData<double> expirience;
    public refData<int> numberBuilded;

    public TowerStaticData() {

        level = new refData<int>(0);
        upgraidLevel = new refData<int>(0);
        expirience = new refData<double>(0);
        numberBuilded = new refData<int>(0);
    }
}